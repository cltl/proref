'''
Created on 9 Oct 2017

@author: cumeo
'''
import codecs
from peng_roth import ec_pb_chain_path, ec_fn_chain_path
from time import time
from cort.core.corpora import Corpus
from data import parse_frames
from _collections import defaultdict
from peng_roth.semlm import extract_ec_chain
import preprocess_conll
import os
import config

num_docs_in_dev = 10000

ec_pb_chain_path_train = 'preprocessed-data/srlCorefChains.train.txt'
ec_pb_chain_path_dev = 'preprocessed-data/srlCorefChains.dev.txt'
ec_pb_chain_oxlm_train = 'preprocessed-data/ec_pb_chain_as_sentences.train.txt'
ec_pb_chain_oxlm_dev = 'preprocessed-data/ec_pb_chain_as_sentences.dev.txt'

ec_fn_chain_path_train = 'preprocessed-data/srlCorefChains_group.train.txt'
ec_fn_chain_path_dev = 'preprocessed-data/srlCorefChains_group.dev.txt'
ec_fn_chain_oxlm_train = 'preprocessed-data/ec_fn_chain_as_sentences.train.txt'
ec_fn_chain_oxlm_dev = 'preprocessed-data/ec_fn_chain_as_sentences.dev.txt'

def split_ec_chains_train_dev(inp_path, train_path, dev_path):
    with codecs.open(inp_path, 'r', 'utf-8') as f_in:
        # first documents go to dev set 
        with codecs.open(dev_path, 'w', 'utf-8') as f_out:
            num_doc = 0
            line = f_in.readline()
            while line != '' and num_doc < num_docs_in_dev:
                f_out.write(line)
                line = f_in.readline()
                if line.startswith('Doc:'):
                    num_doc += 1
        # the rest go to training set
        with codecs.open(train_path, 'w', 'utf-8') as f_out:
            while line != '':
                f_out.write(line)
                line = f_in.readline()

def ec_chains_to_oxlm_format(in_path, out_path):
    with codecs.open(in_path, 'r', 'utf-8') as f_in, \
            codecs.open(out_path, 'w', 'utf-8') as f_out:
        for i, line in enumerate(f_in):
            if not line.startswith('Doc:'):
                ner_label, chain = line.rstrip().split('\t')
                frames = chain.split(',')
                f_out.write(' '.join(frames))
                f_out.write('\n')
            if (i+1) % 100000 == 0:
                print('%d ...' %(i+1))
        
class ConllECChainsPaths(object):
    def __init__(self, base_dir):
        self.train_gold_path = os.path.join(base_dir, 'conll_ec_train.gold.txt')
        self.dev_gold_path = os.path.join(base_dir, 'conll_ec_dev.gold.txt')
        self.test_gold_path = os.path.join(base_dir, 'conll_ec_test.gold.txt')
        self.train_auto_srl_path = os.path.join(base_dir, 'conll_ec_train.auto_srl.txt')
        self.dev_auto_srl_path = os.path.join(base_dir, 'conll_ec_dev.auto_srl.txt')
        self.test_auto_srl_path = os.path.join(base_dir, 'conll_ec_test.auto_srl.txt')

def extract_ec_chains_for_training():
    config.setup_experiment()
    paths = ConllECChainsPaths(config.out_dir)
    for inp_path, out_path in ((preprocess_conll.paths.train_gold_path, paths.train_gold_path),
                               (preprocess_conll.paths.dev_gold_path, paths.dev_gold_path),
                               (preprocess_conll.paths.test_gold_path, paths.test_gold_path),
                               (preprocess_conll.paths.train_auto_srl_path, paths.train_auto_srl_path),
                               (preprocess_conll.paths.dev_auto_srl_path, paths.dev_auto_srl_path),
                               (preprocess_conll.paths.test_auto_srl_path, paths.test_auto_srl_path)):
#     for inp_path, out_path in (('preprocessed-data/sample.conll', # for debugging 
#                                 os.path.join(config.out_dir, 'conll_sample.txt')),): 
        start_sec = time()
        print('Reading data from %s...' %inp_path)
        with codecs.open(inp_path, 'r', 'utf-8') as f:
            corpus = Corpus.from_file('conll-2012-train', f)
            parse_frames(corpus)
        elapsed_min = (time() - start_sec) / 60
        print('Reading data from %s... Done (%.1f min).' %(inp_path, elapsed_min))
        with codecs.open(out_path, 'w', 'utf-8') as f:
            for doc in corpus:
                id2chain = defaultdict(list)
                for m in doc.annotated_mentions:
                    id2chain[m.attributes['annotated_set_id']].append(m)
                ec_chains = []
                for men_chain in id2chain.values():
                    ec_chain = extract_ec_chain(men_chain, map_to_framenet=True)
                    if ec_chain:
                        ec_chains.append(ec_chain)
                        f.write(' '.join(ec_chain))
                        f.write(os.linesep)

if __name__ == '__main__':
#     split_ec_chains_train_dev(ec_pb_chain_path, ec_pb_chain_path_train, ec_pb_chain_path_dev)
#     ec_chains_to_oxlm_format(ec_pb_chain_path_train, ec_pb_chain_oxlm_train)
#     ec_chains_to_oxlm_format(ec_pb_chain_path_dev, ec_pb_chain_oxlm_dev)
#     
#     split_ec_chains_train_dev(ec_fn_chain_path, ec_fn_chain_path_train, ec_fn_chain_path_dev)
#     ec_chains_to_oxlm_format(ec_fn_chain_path_train, ec_fn_chain_oxlm_train)
#     ec_chains_to_oxlm_format(ec_fn_chain_path_dev, ec_fn_chain_oxlm_dev)
#     
    extract_ec_chains_for_training()