'''
Created on 9 Oct 2017

@author: cumeo
'''
import codecs
from peng_roth.preprocess import ec_pb_chain_oxlm_train, ec_fn_chain_oxlm_train
from collections import Counter
import numpy as np
from peng_roth import preprocess
import re
import sys
import random
import config
import os

def measure_full_vocabulary():
    num_seq = 0
    num_tok = 0
    frequencies = Counter()
    with codecs.open(ec_pb_chain_oxlm_train, 'r', 'utf-8') as f:
        for line in f:
            for word in line.strip().split():
                frequencies[word] += 1
                num_tok += 1
            num_seq += 1
            if num_seq % 100000 == 0:
                print('%d ...' %num_seq)
    group_highs = np.array([5, 10, 20, 25, 30, 50, 100, 1000])
    group_counts = [0] * (len(group_highs)+1)
    for word, freq in frequencies.items():
        group_counts[np.searchsorted(group_highs, freq+1)] += 1
    print('Number of unique words in different frequencies ranges:')
    print('\t[0, %d): %d' %(group_highs[0], group_counts[0]))
    for i in range(1, len(group_highs)):
        print('\t[%d, %d): %d' %(group_highs[i-1], group_highs[i], group_counts[i]))
    print('\t>=%d: %d' %(group_highs[-1], group_counts[-1]))
#     with codecs.open('output/ec_frequencies.txt', 'w', 'utf-8') as f:
#         for word, freq in sorted(frequencies.items(), key=lambda i: i[1]):
#             f.write('%s\t%d\n' %(word, freq))

    # Reproduce Table 2 in Peng and Roth (2016) 
    cutoff = 25
    frames = [f for f in frequencies if frequencies[f] >= cutoff]
    fc = [f for f in frames if '#' in f]
    high_frequency_fc = sorted(fc, key=lambda f: frequencies[f], reverse=True)
    print('High frequency frame compounds: %s' %high_frequency_fc[:100])
    print('Num unique frames: %d' %len(frames))
    print('Num unique frame compounds: %d' %len(fc))
    print('Num sequences: %d' %num_seq)
    print('Num tokens: %d' %num_tok)
    print('Num tokens per sequence: %.1f' %(num_tok/num_seq))

def read_vocab(chain_file):
    sys.stdout.write('Reading file %s... ' %chain_file)
    vocab = Counter()
    with codecs.open(chain_file, 'r', 'utf-8') as f:
        for line in f:
            for word in re.split(' |,', line.strip()):
                vocab[word] += 1
    sys.stdout.write('Done.\n')
    return vocab

def write_most_frequent(words, counter, out_path):
    subvocab = Counter(dict((w, counter[w]) for w in words))
    with open(out_path, 'w') as f:
        for w, c in subvocab.most_common():
            f.write('%s\t%d\n' %(w,c))

def _compare_ec_chain_vocab_pair(name1, path1, name2, path2):
    sys2_vocab = read_vocab(path2)
    sys2_count = sum(sys2_vocab.values())
    sys1_vocab = read_vocab(path1)
    sys1_count = sum(sys1_vocab.values())
    shared = set(sys1_vocab).intersection(sys2_vocab)
    shared_count1 = sum(sys1_vocab[w] for w in shared)
    shared_count2 = sum(sys2_vocab[w] for w in shared)
    print('='*80)
    print('Comparing the vocabulary of %s and %s' %(name1, name2))
    print('\t%s = %s' %(name1, path1))
    print('\t%s = %s' %(name2, path2))
    print('='*80)
    print("Shared EC-words: %d (%.1f%% of sys1, %.1f%% of sys2)" 
          %(len(shared), len(shared)*100/len(sys1_vocab), len(shared)*100/len(sys2_vocab)))
    print("\tCount in sys1 data: %d (%.1f%%)" 
          %(shared_count1, shared_count1*100/sys1_count))
    print("\tCount in sys2 data: %d (%.1f%%)" 
          %(shared_count2, shared_count2*100/sys2_count))
    print('\tSamples: %s' %random.sample(shared, 5))
    sys1_private = set(sys1_vocab).difference(sys2_vocab)
    print("EC-words in sys1 but not in sys2: %d (%.1f%% of sys1)" 
          %(len(sys1_private), len(sys1_private)*100/len(sys1_vocab)))
    print('\tSamples: %s' %random.sample(sys1_private, 5))
    sys1_private_path = os.path.join(config.out_dir, '%s-but-not-%s.txt' %(name1, name2))
    write_most_frequent(sys1_private, sys1_vocab, sys1_private_path)
    print('\tWritten to %s' %sys1_private_path)
    sys2_private = set(sys2_vocab).difference(sys1_vocab)
    print("EC-words in sys2 but not in sys1: %d (%.1f%% of sys2)" 
          %(len(sys2_private), 
            len(sys2_private)*100/len(sys2_vocab)))

def compare_ec_chain_vocabs():
    my_paths = preprocess.ConllECChainsPaths('output/2017-11-27-working')
    _compare_ec_chain_vocab_pair("conll_train", my_paths.train_gold_path, "peng", ec_fn_chain_oxlm_train)
    _compare_ec_chain_vocab_pair("conll_dev", my_paths.dev_gold_path, "conll_train", my_paths.train_gold_path)

if __name__ == '__main__':
    config.setup_experiment()
#     measure_full_vocabulary()
    compare_ec_chain_vocabs()
    