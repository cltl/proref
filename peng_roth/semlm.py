from lxml import etree
from collections import defaultdict
import os

def read_semlink():
    # "we do a mapping to FrameNet frames via VerbNet senses (Schuler, 2005), 
    # thus achieving a higher level of abstraction"
    predicate_vn2fn = defaultdict(set)
    with open(os.path.join('data', 'semlink1.1', 'vn-fn', 'VNclass-FNframeMappings.xml')) as f:
        tree = etree.parse(f)
        for vncls in tree.iterfind('vncls'):
            fnframe = vncls.get('fnframe')
            if fnframe not in ['DS', 'NA']:
                predicate_vn2fn[(vncls.get('class'), vncls.get('vnmember'))].add(fnframe)

    predicate_pb2vn = defaultdict(set)
    with open(os.path.join('data', 'semlink1.1', 'vn-pb', 'type_map.xml')) as f:
        tree = etree.parse(f)
        for predicate in tree.iterfind('predicate'):
            lemma = predicate.get('lemma')
            for argmap in predicate.iterfind('argmap'):
                pb = argmap.get('pb-roleset')
                vncls = argmap.get('vn-class')
                predicate_pb2vn[pb].add((vncls, lemma))
                
    pb2fn = {}
    for pb in predicate_pb2vn:
        fn_frames = set()
        for vn in predicate_pb2vn[pb]:
            fn_frames.update(predicate_vn2fn[vn])
        if len(fn_frames) == 1:
            pb2fn[pb] = list(fn_frames)[0]
    return pb2fn
    
pb2fn = read_semlink()

def _format_frame_element(fe, doc, map_to_framenet):
    pred = fe.frame.predicate_id
    if map_to_framenet:
        # "we do a mapping to FrameNet frames via VerbNet senses 
        # (Schuler, 2005), thus achieving a higher level of abstraction. 
        # The mapping file4 defines deterministic mappings. However, 
        # the mapping is not complete and there are remaining PropBank 
        # frames."
        fn_frame = pb2fn.get(pred)
        if fn_frame:
            pred = fn_frame
    appendix = ''
    # "if a preposition immediately follows a predicate, we append 
    # the preposition to the predicate e.g. “take over”"
    # actually, in "take over", "over" is a particle (RP)
    p = fe.frame.predicate_offset+1
    if p < len(doc.pos) and doc.pos[p] in ('RP', 'IN'):
        appendix += '[%s]' %(doc.tokens[p])
    # "if we encounter the semantic role label AM-PRD which 
    # indicates a secondary predicate, we also append this 
    # secondary predicate to the main predicate e.g. “be happy”"
    # I found "wake.02[up happy]!A0" in the data Peng sent me
    prd = fe.frame.get('ARGM-PRD')
    if prd is not None:
        # avoid cases like "Cause_to_fragment[through][stirring up anti-Japanese 
        # spirit throughout the nation and influencing the situation of the 
        # anti-fascist war of the people worldwide]"
        if len(prd.tokens) <= 2:
            appendix += '[%s]' %' '.join(prd.tokens)
    # "if we see the semantic role label AM-NEG which 
    # indicates negation, we ap- pend “not” to the predicate 
    # e.g. “not like”"
    if 'ARGM-NEG' in fe.frame.roles():
        appendix += '(not)'
    role = fe.role.replace('ARG', 'A')
    return '%s%s!%s' %(pred, appendix, role)
    

def extract_ec_chain(men_chain, map_to_framenet=False):
    '''Extract a sequence of frame-semantic tags as described in Peng & Roth (2016)
    '''
    ec_chain = []
    doc = men_chain[0].document
    for m in men_chain:
        # "If a mention head is inside an argument, we regard it as a match."
        head_index = m.span.begin + m.attributes['head_index']
        related_frames = doc.frame_elements[head_index]
        related_frames.sort(key=lambda fe: fe.frame.predicate_offset)
        i = 0
        while i < len(related_frames)-1:
            # "We apply the rule that if the gap between two predicates is less 
            # than two tokens, we treat them as a unified semantic frame defined 
            # by the conjunction of the two (augmented) semantic frames, e.g. 
            # “eat.01-drink.01” and “decide.01-buy.01”"
            if related_frames[i+1].frame.predicate_offset - related_frames[i].frame.predicate_offset < 2:
                related_frames[i:i+2] = (related_frames[i], related_frames[i+1])
            i += 1 # no need to join more than 2 frames (?)
        for fe in related_frames:
            if isinstance(fe, tuple):
                compound_str = '#'.join(_format_frame_element(sub_fe, 
                                        doc, map_to_framenet) for sub_fe in fe)
                ec_chain.append(compound_str)
            else:
                ec_chain.append(_format_frame_element(fe, doc, map_to_framenet))
    if len(ec_chain) >= 2:
#         assert all(elem.strip() != '' for elem in ec_chain)
        return ec_chain
    else:
        return None
    
