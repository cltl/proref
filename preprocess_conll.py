import os
import config
import shutil
import codecs
import re

# Please modify this manually when you have a new version
class __Paths(object):
    def __init__(self, base_dir):
        self.train_gold_path = os.path.join(base_dir, 'train.gold_conll')
        self.dev_gold_path = os.path.join(base_dir, 'dev.gold_conll')
        self.test_gold_path = os.path.join(base_dir, 'test.gold_conll')
        self.train_auto_srl_path = os.path.join(base_dir, 'train.auto_srl_conll')
        self.dev_auto_srl_path = os.path.join(base_dir, 'dev.auto_srl_conll')
        self.test_auto_srl_path = os.path.join(base_dir, 'test.auto_srl_conll')

paths = __Paths('preprocessed-data/2017-11-13-f8c181f')

orig_train_dir = 'data/conll-2012/conll-2012/v4/data/train/data/english/annotations'
orig_dev_dir = 'data/conll-2012/conll-2012/v4/data/development/data/english/annotations'
orig_test_dir = 'data/conll-2012/conll-2012/v9/data/test/data/english/annotations'
orig_test_key_dir = 'data/conll-2012/conll-2012-test-key/v4/data/test/data/english/annotations'

def concat_conll(inp_dir, out_path, suffix):
    with codecs.open(out_path, 'w', 'utf-8') as out_f:
        for root, _, fnames in os.walk(inp_dir):
            for fname in fnames:
                if fname.endswith(suffix):
                    inp_path = os.path.join(root, fname)
                    with codecs.open(inp_path, 'r', 'utf-8') as inp_f:
                        shutil.copyfileobj(inp_f, out_f)

def inject_auto_srl(inp_dir, out_path, auto_dir=None, map_auto_version=None):
    doc_count = 0
    with codecs.open(out_path, 'w', 'utf-8') as f_out:
        for root, _, fnames in os.walk(inp_dir):
            for fname in fnames:
                if fname.endswith('_gold_conll'):
                    fname2 = fname.replace('_gold', '_auto')
                    path1 = os.path.join(root, fname)
                    path2 = os.path.join(root, fname2)
                    if auto_dir is not None:
                        path2 = path2.replace(inp_dir, auto_dir)
                    if map_auto_version is not None:
                        path2 = re.sub(r'v\d+_auto', 'v%d_auto' %map_auto_version, path2)
                    assert os.path.exists(path2)
                    with codecs.open(path1, 'r', 'utf-8') as f1, \
                            codecs.open(path2, 'r', 'utf-8') as f2:
                        for line1, line2 in zip(f1, f2):
                            if line1.startswith('#'):
                                assert line1 == line2 # same headers and footers
                                f_out.write(line1)
                            else:
                                if line1.strip() != '':
                                    fields1 = re.split(r'\s+', line1.strip())
                                    fields2 = re.split(r'\s+', line2.strip())
                                    assert fields1[3] == fields2[3] # same tokens
                                    fields1[11:-1] = fields2[11:-1]
                                    f_out.write('\t'.join(fields1))
                                f_out.write('\n')
                    doc_count += 1
                    if doc_count % 100 == 0:
                        print('Document #%d ...' %doc_count)

def create_train_dev_test():
    concat_conll(orig_train_dir, paths.train_gold_path, '_gold_conll')
    concat_conll(orig_dev_dir, paths.dev_gold_path, '_gold_conll')
    concat_conll(orig_test_key_dir, paths.test_gold_path, '_gold_conll')
    inject_auto_srl(orig_train_dir, paths.train_auto_srl_path)
    inject_auto_srl(orig_dev_dir, paths.dev_auto_srl_path)
    inject_auto_srl(orig_test_key_dir, paths.test_auto_srl_path, 
                    auto_dir=orig_test_dir, map_auto_version=9)

if __name__ == '__main__':
    config.setup_experiment()
    if not os.path.exists(config.preprocess_dir): os.makedirs(config.preprocess_dir)
    paths = __Paths(config.preprocess_dir)
    create_train_dev_test()
