from datetime import date
import os
import subprocess
import sys

preprocess_dir = None
out_dir = None
revision = None

def get_revision():
    # git diff --staged is faster than git diff (of everything) so let's do it first
    changed = (subprocess.check_output('git diff --staged', shell=True) != b'' or 
               subprocess.check_output('git diff', shell=True) != b'')
    if changed:
        return date.today().strftime('%Y-%m-%d') + '-' + 'working'
    else:
        return (subprocess.check_output('git show -s --format=%ci', shell=True)[:10] +
                subprocess.check_output('git show -s --format=-%h', shell=True).strip()).decode()
        

def setup_experiment():
    global out_dir, preprocess_dir, revision
    revision = get_revision()
    out_dir = os.path.join('output', revision)
    preprocess_dir = os.path.join('preprocessed-data', revision)
    if not os.path.exists(out_dir): os.makedirs(out_dir)
    
    # Unbuffer output
    # Due to this issue it can't be unbuffered: http://bugs.python.org/issue17404
    # But setting buffer to 1 is practically the same
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'wt', 1)
    log_path = os.path.join(out_dir, os.path.basename(sys.argv[0]) + '.out')
    tee = subprocess.Popen(["tee", "-a", log_path], stdin=subprocess.PIPE)
    os.dup2(tee.stdin.fileno(), sys.stdout.fileno())
    os.dup2(tee.stdin.fileno(), sys.stderr.fileno())
    sys.stderr.write('Output dir: %s\n' %out_dir)
    sys.stderr.write('Log is written to: %s\n' %log_path)

if __name__ == '__main__':
    if sys.argv[1:] == ['-rev']:
        print(get_revision())