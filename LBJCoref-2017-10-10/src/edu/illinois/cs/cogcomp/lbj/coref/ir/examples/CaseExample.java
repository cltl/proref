/*
 * Created on Jan 19, 2007 By Eric Bengtson
 *
 */

// vim:fileformat=unix

package edu.illinois.cs.cogcomp.lbj.coref.ir.examples;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.illinois.cs.cogcomp.lbj.coref.util.io.myIO;

import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;


public class CaseExample extends Example {

    public static Map<String,String> m_wordList = null;
    public Doc m_doc;
    public int m_wordN;
    public int m_numWords;

    public CaseExample(Doc doc, int wordN) {
	m_doc = doc;
	m_numWords = doc.getWords().size();
	m_wordN = wordN;
	// TODO: Dependency on location of files:
	if (m_wordList == null)
	    m_wordList = loadWordList("EricWordList.sorted");
    }

    /* getWord returns a caseless word. */
    public String getWord() {
	return m_doc.getWord(m_wordN).toLowerCase();
    }
    public String getWord(int offset) {
	int targetN = m_wordN + offset;
	if (targetN < 0 || targetN >= m_numWords)
	    return "";
	else
	    return m_doc.getWord(targetN).toLowerCase();
    }

    /* getWord returns a caseless word. */
    public String getPOS() {
	return m_doc.getPOS(m_wordN);
    }
    public String getPOS(int offset) {
	int targetN = m_wordN + offset;
	if (targetN < 0 || targetN >= m_numWords)
	    return "";
	else
	    return m_doc.getPOS(targetN);
    }

    /** Return getWordCase(), or "newWord" if not in word list. */
    public String getDictCase() {
	return getDictCase(this.getWord());
    }
    /* Assumes word is lowercased! */
    public String getDictCase(String word) {
	if (m_wordList.containsKey(word)) {
	    String casedWord = m_wordList.get(word);
	    return getWordCase(casedWord);
	} else {
	    return "newWord";
	}
    }

    /** Returns "allLower", "firstCap", "allCaps", "multiCase", "digit", "punc", or "other"
     * In case of a single-character uppercase word, returns "firstCap"
     * Zero length words are "other".
     * Words beginning with a digit are "digit".  This implies that words
       containing internal digits can still be "allLower" or "allUpper".
     * Words beginning with punctuation are "punc", and word-internal punc is
       not considered "punc".
     * Words beginning with whitespace are "other".
     */
    public String getWordCase() {
	return getWordCase(m_doc.getWord(m_wordN));
    }
    public String getWordCase(String word) {
	if (word.length() == 0)
	    return "other";

	if ( Character.isDigit(word.charAt(0)) )
	    return "digit";

	if ( Character.isWhitespace(word.charAt(0)) )
	    return "other";

	if ( !Character.isLetterOrDigit(word.charAt(0)) )
	    return "punc";

	if ( word.equals(word.toLowerCase()) )
	    return "allLower";

	/* A one-letter word is first-cap rather than uppercase */
	if ( word.equals(word.toUpperCase()) && word.length() > 1 )
	    return "allCaps";

	if ( Character.isUpperCase(word.charAt(0)) ) {
	    if (word.length() == 1)
		return "firstCap";
	    else if (word.substring(1).equals(word.substring(1).toLowerCase()))
		return "firstCap";
	}

	return "multiCase"; //Must be mixed case if gets here.
    }

    public Map<String,String> loadWordList(String filename) {
	Map<String, String> wordsList = new HashMap<String, String>();
	URL uFile = ClassLoader.getSystemResource(filename);
	if (uFile == null) {
	    System.err.println("File not found.");
	    return wordsList;
	}
	String fqFilename = uFile.getFile();
	List<String> sLines = (new myIO()).readLines(fqFilename);
	for (String word : sLines) {
	    wordsList.put(word.toLowerCase(), word);
	}
	return wordsList;
    }
}
