package edu.illinois.cs.cogcomp.lbj.coref.ir;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;


/**
  * Represents a chunk of text in the context of a document.
  * Contains start and end characters and a mechanism for determining
  * the start and end word numbers, and the words, contained in the chunk.
  * Capable of being sorted.
 */
public class Chunk implements Serializable, Comparable<Chunk> {
  /**
    * This ID should change if the serialization changes.
   */
  private static final long serialVersionUID = 50L;
  private Doc m_doc;
  private int m_start; /** In count characters **/
  private int m_end; /** In count characters **/
  private String m_text; //Warning: Doesn't affect hashCode or equality.

  /**
    * Constructs a chunk given a range of characters, some text,
    * and a document for context.
    * @param d The document containing the chunk.
    * (Only a reference is kept; the document is not copied).
    * @param start The position of the character that starts this Chunk.
    * @param end The position of the character that ends this Chunk.
    * @param text A convenience access to the text between start and end.
    * This text should match the text in the character range in the document.
    *  WARNING: text is not used for comparison or hashing.
   */
  public Chunk(Doc d, int start, int end, String text) {
    if (m_start < 0)
      System.err.println("Bad start location for chunk.");
    if (m_end < 0)
      System.err.println("Bad end location for chunk.");

    m_doc = d;
    m_start = start;
    m_end = end;
    m_text = text;
  }

  /**
    * Gets a string representation of the chunk
    * @return A string representation of the chunk.
   */
  public String toString() {
    return m_start+","+m_end+": "+m_text;
  }
  /**
    * Gets the start character number of the chunk.
    * @return The position of the character that starts this Chunk.
   */
  public int getStart() {
    if (m_start < 0)
      System.err.println("Warning: Chunk start < 0");
    return m_start;
  }

  /**
    * Gets the end character number of the chunk.
    * @return The position of the character that ends this Chunk.
   */
  public int getEnd() {
    if (m_end < 0)
      System.err.println("Warning: Chunk end < 0");
    return m_end;
  }

  /**
    * Gets the word number of the first word of the chunk.
    * This is the position in the document of the first word of the chunk.
    * @return the word number of the first word in the Chunk.
   */
  public int getStartWN() {
    return m_doc.getWordNum(getStart());
  }

  /**
    * Gets the word number of the last word of the chunk.
    * This is the position in the document of the last word of the chunk.
    * @return the word number of the last word in the Chunk.
   */
  public int getEndWN() {
    return m_doc.getWordNum(getEnd());
  }

  /**
    * Gets the words of the chunk.
    * @return An unmodifiable view of the sublist (of the Doc's getWords())
    * of words contained in this Chunk.
   */
  public List<String> getWords() {
    return Collections.unmodifiableList(
        m_doc.getWords().subList(getStartWN(), getEndWN() + 1));
  }

  /**
    * Gets the text of the chunk.
    * @return The text; the text is for convenience only and does not
    * affect equality or hashCode() value.
   */
  public String getText() {
    return m_text;
  }

  /**
    * Gets a cleaned text that has newlines replaced with spaces.
   */
  public String getCleanText() {
    return m_text.replaceAll(" ?\n", " ");
  }

  /**
    * Determines whether this chunk is equal to a specified object.
    * Chunks are equal if they occupy the same character positions.
    * equals and hashcode methods inspired by article hosted on
    * Technofundo, called "Euals and Hash Code", by Manish Hatwalne
    * Available as of Feb 27 2007 at URL: 
    * http://www.geocities.com/technofundo/tech/java/equalhash.html
    * @param o Any object.
    * @return Whether this is equal to the specified object.
   */
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || (o.getClass() != this.getClass()) )
      return false;
    Chunk c = (Chunk) o; //Same class, so must be valid.
    if (   c.getStart() == this.m_start
           && c.getEnd()   == this.m_end
           //NOTE: This was here, but makes it hard to compare mentions
           //from different instances of the same doc,
           //since identity of Doc is hard to get right:
           //&& c.m_doc.equals(this.m_doc)
           //NOTE: texts aren't compared!
       )
      return true;
    else
      return false;
  }

  /**
    * Gets the hash code of this chunk.
    * The hash code of a chunk is determined entirely by its
    * start and end character positions.
    * equals and hashcode methods inspired by article hosted on
    * Technofundo, called "Euals and Hash Code", by Manish Hatwalne
    * Available as of Feb 27 2007 at URL: 
    * http://www.geocities.com/technofundo/tech/java/equalhash.html
   */
  public int hashCode() {
    int value = 217; // 7 * 31;
    value += this.m_start;
    value *= 31;
    value += this.m_end;
    //NOTE: m_text isn't taken into account.
    return value;
  }

  /**
    * Compare the chunk in a way that Sorts ascending,
    * first by start positions, or if starts are equal, by end positions.
    * @param c Another chunk.
   */
  public int compareTo(Chunk c) {
    if (getStart() < c.getStart()) return -1;
    if (getStart() > c.getStart()) return 1;
    if (getEnd() < c.getEnd()) return -1;
    if (getEnd() > c.getEnd()) return 1;
    return 0;
  }
}

