/**
 * 
 */
package edu.illinois.cs.cogcomp.lbj.coref.util.io;

/**
 * @author Eric
 *
 */
public class Debug {
    public Debug(String msg) {
	System.err.println(msg);
    }

    public static void p(String msg) {
	System.err.println(msg);
    }
}
