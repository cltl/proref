package edu.illinois.cs.cogcomp.lbj.coref.util.collections;

import java.util.Collection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class ListUtils {
    public static <T> List<T> flatten(Collection<? extends Collection<T>> nest)
    {
	List<T> result = new ArrayList<T>();
	for (Collection<T> list : nest) {
	    for (T element : list) {
		result.add(element);
	    }
	}
	return result;
    }
    
    /**
     * @param <T>
     * @param list
     * @return An ArrayList containing the elements from the list.
     */
    public static <T> List<T> getNoDuplicates(List<T> list) {
	return new ArrayList<T>(new HashSet<T>(list));
    }
    
    /**
     * @param l list
     * @param numer numerator
     * @param denom denominator
     * @return Add num/denom elements at random to first element of result
     *         and (1-num)/denom elements at random to first element of
     *         result.
     */
    
    public static <T> List<T>[] splitRand(List<T> l, int numer, int denom,
	int seed) {
	Random r = new Random(seed); /* TODO: generate seed */
	List<T> a = new ArrayList<T>();
	List<T> b = new ArrayList<T>();
	for (T o : l) {
	    if (r.nextInt(denom) < numer)
		a.add(o);
	    else
		b.add(o);
	}
	@SuppressWarnings("unchecked")
	List<T>[] pair = (List<T>[]) new List[2];
	pair[0] = a; pair[1] = b;
	return pair; //Safe
    }
}
