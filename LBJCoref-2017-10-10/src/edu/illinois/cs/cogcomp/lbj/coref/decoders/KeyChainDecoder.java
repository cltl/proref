package edu.illinois.cs.cogcomp.lbj.coref.decoders;

import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;
import edu.illinois.cs.cogcomp.lbj.coref.ir.solutions.ChainSolution;

/**
 * Decodes a document into a {@code ChainSolution}.
 * @param CT The type of element in the {@code ChainSolution}.
 * @author Eric Bengtson
 */
abstract public class KeyChainDecoder<CT>
 extends DecoderWithOptions<ChainSolution<CT>>
 implements KeyDecoder<ChainSolution<CT>> {
    
    /**
     * Decodes a document into a {@code ChainSolution}.
     * @param doc The document to decode.
     * @return A ChainSolution.
     */
    abstract public ChainSolution<CT> decode(Doc doc);
    
}
