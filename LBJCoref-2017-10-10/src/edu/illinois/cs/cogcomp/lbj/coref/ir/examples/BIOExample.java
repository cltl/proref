package edu.illinois.cs.cogcomp.lbj.coref.ir.examples;

import java.util.HashSet;
import java.util.Set;

import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;

/**
 * An example of a word and information about where in a chunk
 * it is located: beginning, inside, or outside a chunk.
 * @author Eric Bengtson
 */
public class BIOExample extends WordExample {

    /**
     * The previous word's example.
     */
    public BIOExample m_prevEx = null;

    /**
     * Whether this word 'b'egins, is 'i'nside, or is 'o'utside a (head) chunk.
     */
    public char m_bio;
    
    /**
     * Whether this word 'b'egins, is 'i'nside, or is 'o'utside an extent chunk.
     */
    public char m_bioE;
    
    /**
     * Whether a word 's'tarts or 'e'nds an extent,
     * for use with bracket classifiers.
     */
    public char m_bracketE; //'s'tarts or 'e'nds a mentions
    
    /**
     * A predicted label for the BIO status of the head.
     */
    public String m_predLabel = null;
    
    /**
     * A predicted label for the BIO status of the extent.
     */
    public String m_predELabel = null;
    
    /**
     * The words of the previous entities.
     */
    public static Set<String> prevEntWords = new HashSet<String>();

    /**
     * Constructs a BIO example for a given word with respect to its
     * presence in a head and in an extent.
     * @param doc The document containing the example word.
     * @param wordN The word number of the example word.
     * @param bioH The BIO status of the word with respect to a head.
     * (unknown: used for evaluation where ground truth is not known)
     * @param bioE The BIO status of the word with respect to an extent.
     * (unknown: used for evaluation where ground truth is not known).
     * @param prev The BIO example for the previous word, if any.
     */
    public BIOExample(Doc doc, int wordN, char bioH, char bioE, BIOExample prev) {
	super(doc, wordN);
	m_bio = bioH;
	m_bioE = bioE;
	m_prevEx = prev;
	m_predLabel = null;
    }

    /**
     * Constructs a BIO example for a given word with respect to its
     * presence in a head.
     * @param doc The document containing the example word.
     * @param wordN The word number of the example word.
     * @param bioH The BIO status of the word with respect to a head.
     * May be 'b', 'i', 'o', or 'u'
     * (unknown: used for evaluation where ground truth is not known)
     * (unknown: used for evaluation where ground truth is not known).
     * @param prev The BIO example for the previous word, if any.
     */
    public BIOExample(Doc doc, int wordN, char bioH,  BIOExample prev) {
	this(doc, wordN, bioH, 'u', prev);
    }



}
