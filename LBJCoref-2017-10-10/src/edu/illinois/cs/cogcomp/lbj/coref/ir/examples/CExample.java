package edu.illinois.cs.cogcomp.lbj.coref.ir.examples;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import edu.illinois.cs.cogcomp.lbj.coref.ir.Mention;
import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;


/**
 * A coreference example (CExample) is a pair of referrers, where each
 * referrer is either an entity mention or a set of entity mentions,
 * and the document containing the mentions.
 * A CExample is the input to classifiers that determine coreference.
 * @author Eric Bengtson
 */
public class CExample extends Example implements Serializable {
    private static final long serialVersionUID = 58L;

    public Doc m_doc = null;

    private Mention m_m1 = null;
    private Mention m_m2 = null;
    //Should contain m1/m2 as long as m_m1/m_m2 has been set:
    private Set<Mention> m_c1 = new HashSet<Mention>();
    private Set<Mention> m_c2 = new HashSet<Mention>();

    private boolean m_positive;

    /**
     * Construct a coreference example referring to mentions
     * {@code m1} and {@code m2} in the context of {@code doc}.
     * If mentions lack (true) EntityID, use setPositive()
     * or do not rely on areCoreferent().
     * @param doc The document containing the mentions.
     * @param m1 The first mention, which generally occurs
     * before {@code m2} in the document.
     * @param m2 The second mention, which is the anaphor, and generally
     * occurs after {@code m1}.
     */
    public CExample(Doc doc, Mention m1, Mention m2) {
	m_doc = doc;
	m_m1 = m1; m_m2 = m2;
	m_c1.add(m_m1);
	m_c2.add(m_m2);

	if (m1.getEntityID().equals(m2.getEntityID()))
	    m_positive = true;
	else
	    m_positive = false;
    }

    
    /**
     * Construct a coreference example referring to a cluster of mentions
     * {@code c1} and a mention {@code m2}
     * in the context of a document {@code doc}.
     * Used to determine whether {@code m2} refers to the entity
     * represented by the mentions in {@code c1}.
     * Cluster {@code c1} and {@code m2}
     * are defined to be coreferential with 
     * only if all mentions in {@code c1}
     * have the same entity ID as {@code m2}.
     * In case c1 is empty, c1 and m2 will be considered coreferent.
     * If mentions lack (true) EntityID, use setPositive()
     * or do not rely on areCoreferent().
     * @param doc The document containing the mentions.
     * @param c1 The set of mentions thought to refer to an entity (copied defensively).
     * c1 must be non-empty
     * @param m2 The second mention, which is the anaphor.
     * @throws IllegalArgumentException if {@code c1} is empty.
     */
    public CExample(Doc doc, Set<Mention> c1, Mention m2) {
	//Should m1 be set to an element of c1? 
	m_c1 = new HashSet<Mention>(c1);
	if (m_c1.size() == 0)
	    throw new IllegalArgumentException("First cluster empty."); 

	m_m2 = m2;
	m_c2.add(m_m2);
	m_doc = doc;

	m_positive = true;
	for (Mention m1 : c1) {
	    if (!m1.getEntityID().equals(m2.getEntityID())) {
		m_positive = false;
		break;
	    }
	}
    }
    
    
    /**
     * Construct a coreference example referring to a set of mentions
     * {@code c1} and a mention {@code m2}
     * in the context of a document {@code doc}.
     * Used to determine whether {@code m2} refers to the entity
     * represented by the mentions in {@code c1}.
     * @param doc The document containing the mentions.
     * @param c1 A set of mentions (copied defensively).
     * @param m2 The second mention, which is the anaphor.
     * @param positiveExample Specify whether this example should be considered
     * a positive one.
     * @throws IllegalArgumentException if {@code c1} is empty.
     */
    public CExample(Doc doc, Set<Mention> c1, Mention m2,
		    boolean positiveExample)
    {
	//Should m1 be set to an element of c1?
	m_c1 = new HashSet<Mention>(c1);
	if (m_c1.size() == 0)
	    throw new IllegalArgumentException("First cluster empty.");
	
	
	m_m2 = m2;
	m_c2.add(m_m2);
	m_doc = doc;

	m_positive = positiveExample;
    }
    
    
    /**
     * Construct a coreference example referring to a set of mentions
     * {@code c1} and a set of mentions {@code c2}
     * in the context of a document {@code doc}.
     * Used to determine whether mentions in {@code c2}
     * refer to the same entity as the
     * the mentions in {@code c1}.
     * (The exact criteria for coreference are left undefined --
     * whether this example is coreferential is specified
     * by {@code setPositive()}.)
     * @param doc The document containing the mentions.
     * @param c1 A set of mentions (copied defensively).
     * @param c2 A second set of mentions (copied defensively).
     * @throws IllegalArgumentException if {@code c1} or {@code c2} is empty.
     */
    public CExample(Doc doc, Set<Mention> c1, Set<Mention> c2) {
	m_doc = doc;
	m_c1 = new HashSet<Mention>(c1);
	m_c2 = new HashSet<Mention>(c2);
	if (m_c1.size() == 0)
	    throw new IllegalArgumentException("First cluster empty.");
	if (m_c2.size() == 0)
	    throw new IllegalArgumentException("Second cluster empty.");
	m_positive = false; //FIXME.  Change or document.
    }
    
    
    /**
     * Construct a coreference example referring to a set of mentions
     * {@code c1} and a set of mentions {@code c2}
     * in the context of a document {@code doc}.
     * Used to determine whether mentions in {@code c2}
     * refer to the same entity as the
     * the mentions in {@code c1}.
     * (The exact criteria for coreference are left undefined  --
     * whether this example is coreferential is specified
     * by {@code positiveExample}.)
     * @param doc The document containing the mentions.
     * @param c1 A set of mentions (copied defensively).
     * @param c2 A second set of mentions (copied defensively).
     * @param positiveExample Specify whether this example should be considered
     * a positive one.
     * @throws IllegalArgumentException if {@code c1} or {@code c2} is empty.
     */
    public CExample(Doc doc, Set<Mention> c1, Set<Mention> c2,
		    boolean positiveExample) {
	m_doc = doc;

	m_c1 = new HashSet<Mention>(c1);
	m_c2 = new HashSet<Mention>(c2);
	if (m_c1.size() == 0)
	    throw new IllegalArgumentException("First cluster empty.");
	if (m_c2.size() == 0)
	    throw new IllegalArgumentException("Second cluster empty.");
	m_positive = positiveExample;
    }
    
    
    /**
     * Construct a coreference example referring to a set of mentions
     * {@code c1} and a set of mentions {@code c2}
     * in the context of a document {@code doc}.
     * Used to determine whether mentions in {@code c2}
     * refer to the same entity as the
     * the mentions in {@code c1}.
     * (The exact criteria for coreference are left undefined  --
     * whether this example is coreferential is specified
     * by {@code setPositive()}.)
     * @param doc The document containing the mentions.
     * @param c1 A set of mentions (copied defensively).
     * @param c2 A second set of mentions (copied defensively).
     * @param m1 The canonical mention of {@code c1}.
     * @param m2 The canonical mention of {@code c2}.
     * @throws IllegalArgumentException if {@code c1} or {@code c2} is empty.
     */
    public CExample(Doc doc, Set<Mention> c1, Set<Mention> c2,
		    Mention m1, Mention m2)
    {
	this(doc, m1, m2);
	m_c1 = new HashSet<Mention>(c1);
	m_c2 = new HashSet<Mention>(c2);
	if (m_c1.size() == 0)
	    throw new IllegalArgumentException("First cluster empty.");
	if (m_c2.size() == 0)
	    throw new IllegalArgumentException("Second cluster empty.");
    }

    /**
     * Construct a coreference example referring to a set of mentions
     * {@code c1} and a set of mentions {@code c2}
     * in the context of a document {@code doc}.
     * Used to determine whether mentions in {@code c2}
     * refer to the same entity as the
     * the mentions in {@code c1}.
     * (The exact criteria for coreference are left undefined  --
     * whether this example is coreferential is specified
     * by {@code positiveExample}.)
     * @param doc The document containing the mentions.
     * @param c1 A set of mentions.
     * @param c2 A second set of mentions.
     * @param m1 The canonical mention of {@code c1}.
     * @param m2 The canonical mention of {@code c2}.
     * @param positiveExample Specify whether this example should be considered
     * a positive one.
     * @throws IllegalArgumentException if {@code c1} or {@code c2} is empty.
     */
    public CExample(Doc doc, Set<Mention> c1, Set<Mention> c2,
		    Mention m1, Mention m2,
		    boolean positiveExample)
    {
	this(doc, c1, c2, m1, m2);
	m_positive = positiveExample;
    }


    //Getter and Setter methods:

    /** 
     * Assumes true knowledge has been given, either using a constructor,
     * the settings of the EntityID labels, or using the setPositive() method.
     * @return For the mention pair case, returns true if the pair are
     * truly coreferential, and false otherwise;  For the cluster case, should
     * return true if all pairs in the cross product of the clusters are
     * truly coreferential, false if such pairs are not coreferential, and
     * the result is user-defined in any other case
     * (Note: The default definition of areCoreferent() is subject to change).
     * (See the comment on the constructors or the setPositive() method.)
     */
    public boolean areCoreferent() {
	//TODO: Extend for cluster case or test-data case.

	if (getM1() != null && getM2() != null)
	    return getM1().getEntityID().equals(getM2().getEntityID());
	else {
	    System.err.println("Lacking mention in the CExample");
	    return false;
	}
    }

    /**
     * Gets the first mention, if available.
     * @return The first mention (which may be an arbitrary mention
     * from the Set {@code c1} if specified,
     * or {@code null} if no first mention is available.  
     */
    public Mention getM1() {
	if (m_m1 == null && m_c1 != null && m_c1.size() > 0)
	    return m_c1.iterator().next();
	return m_m1;
    }

    /**
     * Gets the second mention, if available.
     * @return The second mention (which may be an arbitrary mention
     * from the Set {@code c2} if specified,
     * or {@code null} if no second mention is available.  
     */
    public Mention getM2() {
	if (m_m2 == null && m_c2 != null && m_c2.size() > 0)
	    return m_c2.iterator().next();
	return m_m2;
    }

    /**
     * Gets the specified mention. 
     * @param n if 1, return m1, if 2, return m2.
     * @throws IllegalArgumentException if {@code n} is not 1 or 2.
     */
    public Mention getM(int n) {
	if (n == 1) return getM1();
	else if (n == 2) return getM2();
	else throw new IllegalArgumentException("Invalid n");
    }

    /**
     * Gets the first set of mentions, if available.
     * @return An unmodifiable view of the first set of mentions,
     * or a set containing {@code m1} if no cluster has been specified.
     */
    public Set<Mention> getC1() {
	return Collections.unmodifiableSet(m_c1);
    }

    /**
     * Gets the second set of mentions, if available.
     * @return An unmodifiable view of the second set of mentions.
     * or a set containing {@code m2} if no cluster has been specified.
     */
    public Set<Mention> getC2() {
	return Collections.unmodifiableSet(m_c2);
    }

    /**
     * Gets the document that provides context for the mentions in the example.
     * @return the Doc containing the mentions.
     */
    public Doc getDoc() {
	return m_doc;
    }

    /** This method may be used along with a constructor that does not take
     * the coreference status as input to override the automatically determined
     * result of the {@code areCoreferent()} method.
     * In the cluster case, it is recommended that {@code isPos}
     * be set to true if all pairs in the cross-product are coreferential,
     * and false if all such pairs are non-coreferential;
     * in case some pairs are coreferential,
     * the user may choose an intended interpretation.
     * (Note: The default definition of {@code areCoreferent()}
     * is subject to change).
     * @param isPos Specifies whether
     * this example should be considered positive.
     */
    public void setPositive(boolean isPos) {
	m_positive = isPos;
    }

    /** 
     * Determines whether this is a positive example.
     * @return whether this is a positive example, according to the
     * entity IDs, constructor, or setPositive method.
     */
    public boolean isPositive() {
	return m_positive;
    }
    
    // IO Methods:

    /**
     * Computes a string representation of this example.
     * Presently, this string is of the form {@code &gt;m1;<br>m2&lt;}
     * or if c1 or c2 are present, m1 or m2 is replaced with a comma separated
     * list of mentions.
     */
    public String toString() {
	String s = "<";
	if (m_m1 != null)
	    s += m_m1.getText();
	else {
	    for (Mention m : m_c1) {
		s += m.getText() + ",";
	    }
	}
	s += ";\n";
	if (m_m2 != null)
	    s += m_m2.getText();
	else {
	    for (Mention m : m_c2) {
		s += m.getText() + ",";
	    }
	}
	s += ">";
	return s;
    }
}
