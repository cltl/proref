package edu.illinois.cs.cogcomp.lbj.coref.decoders;

import java.util.ArrayList;
import java.util.List;

import edu.illinois.cs.cogcomp.lbj.coref.filters.MFilter;
import edu.illinois.cs.cogcomp.lbj.coref.ir.Entity;
import edu.illinois.cs.cogcomp.lbj.coref.ir.Mention;
import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;
import edu.illinois.cs.cogcomp.lbj.coref.ir.solutions.ChainSolution;


/**
 * A decoder that extracts the true entities of a document and encodes
 * them in a {@code ChainSolution}.
 * Provides facility for filtering some of those mentions.
 * @author Eric Bengtson
 */
public class CorefKeyDecoder extends KeyChainDecoder<Mention> {
    
    /* Members */
    
    /** The first filter, which may be null if no filtering is desired. */
    private MFilter m_mFilter = null;
    
    /** This filter should be m_mFilter if only one filter needed. */
    private MFilter m_m2Filter = null;
    
    /** Whether a pair of mentions must both be accepted to link them. */
    private boolean m_bBothMustAccept = true;
    
    /** Whether all predicted mentions should be added to the solution. */
    private boolean m_allMents = true;

    
    
    /* Constructors */
    
    /**
     * Construct a decoder that does not filter but adds all predicted
     * mentions to the resulting solution.
     */
    public CorefKeyDecoder() {
    }

    //Idea: Implement case where only one mention needs to pass filter.

    /** 
     * Constructor that accepts a single mention filter
     * and requires that a pair of mentions both be accepted
     * for the pair to be linked in the solution.
     * @param filter The mention filter.
     */
    public CorefKeyDecoder(MFilter filter) {
	m_mFilter = filter;
	m_m2Filter = filter;
    }

    /**
     * Constructor that accepts a single mention filter
     * @param filter A mention filter.
     * @param bothMustAccept If true, a pair of mentions in a entity must both
     * be accepted by the filter for the pair to be linked,
     * otherwise, any pair where at least one mention is accepted
     * by the filter will be linked.
     */
    public CorefKeyDecoder(MFilter filter, boolean bothMustAccept) {
	this(filter);
	m_bBothMustAccept = bothMustAccept;
    }

    /**
     * Constructor that accepts a single mention filter
     * @param filter A mention filter.
     * @param bothMustAccept If true, a pair of mentions in a entity must both
     * be accepted by the filter for the pair to be linked,
     * otherwise, any pair where at least one mention is accepted
     * by the filter will be linked.
     * @param allMents Whether to add all predicted mentions to the solution.
     */
    public CorefKeyDecoder(MFilter filter, boolean bothMustAccept,
			   boolean allMents) {
	this(filter, bothMustAccept);
	m_allMents = allMents;
    }

    /**
     * Constructs a decoder that accepts two filters.
     * Since an entity represents an unordered set of mentions,
     * the order of the filters is irrelevant.
     * A pair of mentions must be accepted by both filters (in some order)
     * for them to be linked.
     * If a single mention is accepted by both filters, it will
     * be included in the solution.
     * @param m1Filter One filter, which should not be {@code null}.
     * @param m2Filter Another filter, which should not be {@code null}.
     * @param allMents Whether to add all predicted mentions to the solution.
     */
    public CorefKeyDecoder(MFilter m1Filter, MFilter m2Filter, boolean allMents)
    {
	m_mFilter = m1Filter;
	m_m2Filter = m2Filter;
	m_allMents = allMents;
    }

    /**
     * Constructs a decoder that accepts two filters.
     * Since an entity represents an unordered set of mentions,
     * the order of the filters is irrelevant.
     * A pair of mentions must be accepted by both filters (in some order)
     * for them to be linked.
     * If a single mention is accepted by both filters, it will
     * be included in the solution.
     * All predicted mentions will be added to the solution.
     * @param m1Filter One filter, which should not be {@code null}.
     * @param m2Filter Another filter, which should not be {@code null}.
     */
    public CorefKeyDecoder(MFilter m1Filter, MFilter m2Filter) {
	m_mFilter = m1Filter;
	m_m2Filter = m2Filter;
    }

    
    
    /* Decode method */
    
    /**
     * Extract the entities from a document and encode the parts
     * that are accepted by any optional filters into a {@code ChainSolution}
     * representing those entities.
     * @param doc The document whose entities to extract.
     * @return A {@code ChainSolution} representing entities
     * as chains of mentions.
     */
    public ChainSolution<Mention> decode(Doc doc) {
	ChainSolution<Mention> sol = new ChainSolution<Mention>();

	if (m_allMents)
	    for (Mention m : doc.getPredMentions())
		sol.recordExistence(m);

	List<Entity> entities = doc.getTrueEntities();
	for (Entity ent : entities) { //Zero filters:
	    if (m_mFilter == null) {
		zeroFilterEntity(sol, ent);
	    }
	    //Faster, because just add all mentions that match filter:
	    else if (m_mFilter == m_m2Filter && m_bBothMustAccept) {
		oneFilterEntity(sol, ent);
	    }
	    else { //Both filters:
		twoFilterEntity(sol, ent);
	    }
	}

	if (m_allMents)
	    for (Mention m : doc.getTrueMentions()) 
		sol.recordExistence(m);
	
	//TODO: Check logic and consider changing.
	if (!m_allMents) {
	    for (Mention m1 : doc.getTrueMentions()) {
		for (Mention m2 : doc.getTrueMentions()) {
		    
		    if (m_mFilter == null || m_m2Filter == null
		     || doAccept(m1,m2)) 
		    {
			sol.recordExistence(m1);
			sol.recordExistence(m2);
		    }
		    
		}
	    }
	}
	
	return sol;
    }

    
    
    /* Filter Utilities */ 
    
    /** 
     * Adds the mentions in the provided entity as a chain in the solution.
     * Does not apply any filters.
     * Modifies the solution in place.
     * @param sol The solution, to be modified in place.
     * @param ent The entity whose mentions should be placed in a chain.
     */
    protected void zeroFilterEntity(ChainSolution<Mention> sol,
     Entity ent) {
	List<Mention> eMents = new ArrayList<Mention>();
	for ( Mention m : ent.getMentions() ) {
	    eMents.add(m);
	}
	if (eMents.size() > 0)
	    sol.addChain( eMents );
    }

    /** 
     * Adds the mentions of the provided entity
     * that are accepted by the (first) filter as a chain in the solution.
     * Modifies the solution in place.
     * @param sol The solution, to be modified in place.
     * @param ent The entity whose mentions should be placed in a chain.
     */
    protected void oneFilterEntity(ChainSolution<Mention> sol, Entity ent)
    {
	List<Mention> eMents = new ArrayList<Mention>();
	for ( Mention m : ent.getMentions() ) {
	    //TODO: Why restrict to non-existent mentions?
	    if (m_mFilter.accept(m) && !sol.contains(m))
		eMents.add(m);
	}
	if (eMents.size() > 0)
	    sol.addChain(eMents);
    }

    /** 
     * Links every pair of mentions of the provided entity
     * that are accepted by the filter(s).
     * Modifies the solution in place.
     * @param sol The solution, to be modified in place.
     * @param ent The entity whose mentions should be linked.
     */
    protected void twoFilterEntity(ChainSolution<Mention> sol, Entity ent)
    {
	for ( Mention m : ent.getMentions() ) {
	    for ( Mention m2 : ent.getMentions() ) {
		//TODO: Why restrict to non-existent mentions?
		if (doAccept(m,m2) && !sol.contains(m)) {
		    sol.recordEquivalence(m,m2);
		}
	    }
	}
    }

    /**
     * Determine whether filter(s) accept the mentions.
     * @param m1 One mention.
     * @param m2 Another mention.
     * @return Whether the filter(s) accept the mentions,
     * in order.
     */
    private boolean doAccept(Mention m1, Mention m2) {
	if (m_bBothMustAccept) {
	    if (m_mFilter.accept(m1) && m_m2Filter.accept(m2)) {
		return true;
	    }
	} else {
	    if (m_mFilter.accept(m1) || m_m2Filter.accept(m2)) {
		return true;
	    }
	}
	return false;
    }

}
