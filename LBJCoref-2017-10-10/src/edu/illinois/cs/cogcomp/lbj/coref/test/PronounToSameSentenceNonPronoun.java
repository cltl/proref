package edu.illinois.cs.cogcomp.lbj.coref.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;
import edu.illinois.cs.cogcomp.lbj.coref.ir.Mention;


public class PronounToSameSentenceNonPronoun
  implements ConstraintStatistics.Constraint
{
  private static String[] pronouns =
    { "he", "her", "him", "his", "it", "its", "she", "their", "them", "they",
      "who", "whose" };
  private static String[] peoplePronouns =
    { "he", "she", "him", "her", "his", "who", "whose" };
  private static String[] nonPeoplePronouns = { "it", "its" };
  private static String[] pluralPronouns = { "they", "them", "their" };
  private static String[] singularPronouns =
    { "he", "her", "him", "his", "it", "its", "she", "who" };
  static {
    Arrays.sort(pronouns);
    Arrays.sort(peoplePronouns);
    Arrays.sort(nonPeoplePronouns);
    Arrays.sort(pluralPronouns);
    Arrays.sort(singularPronouns);
  }


  public static Mention findAppropriate(Doc d, int mIndex) {
    Mention m = d.getTrueMention(mIndex);
    int sentenceIndex = m.getSentNum();
    String text = m.getExtent().getText().toLowerCase();
    boolean requiresPerson = Arrays.binarySearch(peoplePronouns, text) >= 0;
    boolean requiresNonPerson =
      Arrays.binarySearch(nonPeoplePronouns, text) >= 0;
    boolean requiresPlural = Arrays.binarySearch(pluralPronouns, text) >= 0;
    boolean requiresSingular =
      Arrays.binarySearch(singularPronouns, text) >= 0;

    if (--mIndex >= 0) {
      m = d.getTrueMention(mIndex);
      int wordIndex = m.getHeadFirstWordNum();

      while (mIndex >= 0 && sentenceIndex == m.getSentNum()) {
        if (!m.getType().equals("PRO") && !m.getType().equals("PRE")
            && (!requiresNonPerson || !m.getEntityType().equals("PER"))
            && (!requiresPerson || m.getEntityType().equals("PER"))
            && (!requiresPlural
                || (d.getPOS(wordIndex).equals("NNPS")
                    || d.getPOS(wordIndex).equals("NNS")))
            && (!requiresSingular
                || !(d.getPOS(wordIndex).equals("NNPS")
                     || d.getPOS(wordIndex).equals("NNS"))))
          return m;

        if (--mIndex < 0) break;
        m = d.getTrueMention(mIndex);
        wordIndex = m.getHeadFirstWordNum();
      }
    }

    return null;
  }


  public List<List<Mention[]>> constrain(Doc d) {
    List<List<Mention[]>> result = new ArrayList<List<Mention[]>>();
    List<Mention[]> forceCoref = new LinkedList<Mention[]>();
    result.add(forceCoref);
    result.add(new LinkedList<Mention[]>());

    int N = d.getNumMentions();
    for (int i = 0; i < N; ++i) {
      Mention m = d.getTrueMention(i);
      String text = m.getExtent().getText().toLowerCase();
      if (Arrays.binarySearch(pronouns, text) < 0) continue;

      Mention m2 = findAppropriate(d, i);
      if (m2 != null) forceCoref.add(new Mention[]{ m, m2 });
    }

    return result;
  }
}

