package edu.illinois.cs.cogcomp.lbj.coref.ir.relations;

import java.io.Serializable;

/**
 * Represents a relation between entities, appearing in a document.
 * @author Eric Bengtson
 */
public class Relation implements Serializable {

    private static final long serialVersionUID = -7619242474215509032L;
    private String m_id;
    private String m_type;
    private String m_subType;
    private RelationEntityArgument m_a1; /* entity argument 1 */
    private RelationEntityArgument m_a2; /* (possibly null) entity argument 2 */
    private RelationMention m_mention;

    /**
     * Constructs a relation.
     * @param id The relation ID.
     * @param type The relation type.
     * @param subType The relation subtype.
     * @param entArg1 The first argument of the relation, representing an entity.
     * @param entArg2 The second argument of the relation, representing an entity.
     * @param m The relation mention.
     */
    public Relation(String id, String type, String subType, RelationEntityArgument entArg1,
	    RelationEntityArgument entArg2, RelationMention m) {
	m_id = id;
	m_type = type;
	m_subType = subType;
	m_a1 = entArg1;
	m_a2 = entArg2;
	m_mention = m;
    }

    /**
     * Constructs a string representation of this relation.
     * @return A string representation of the relation.
     */
    public String toString() {
	return "ID:"+m_id+", TYPE: "+m_type+", SUBTYPE:"+m_subType
	+", EARGS:("+m_a1+", "+m_a2+")"+", MENTION:"+m_mention+"\n";

    }

    /**
     * Gets the serialization version UID.
     * @return The serialization version UID.
     */
    public static long getSerialVersionUID() {
	return serialVersionUID;
    }

    /**
     * Gets the relation ID.
     * @return The relation ID.
     */
    public String getID() {
	return m_id;
    }

    /**
     * Gets the relation type.
     * @return The relation type.
     */
    public String getType() {
	return m_type;
    }

    /**
     * Gets the relation subtype.
     * @return The relation subtype.
     */
    public String getSubtype() {
	return m_subType;
    }

    /**
     * Gets the relation mention.
     * @return The relation mention.
     */
    public RelationMention getMention() {
	return m_mention;
    }

    /**
     * Gets the first entity argument.
     * @return The first entity argument.
     */
    public RelationEntityArgument getA1() {
	return m_a1;
    }

    /**
     * Gets the second relation argument.
     * @return The second relation argument, which may be null.
     */
    public RelationEntityArgument getA2() {
	return m_a2;
    }
}
