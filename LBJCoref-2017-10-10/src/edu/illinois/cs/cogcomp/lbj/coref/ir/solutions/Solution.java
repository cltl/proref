package edu.illinois.cs.cogcomp.lbj.coref.ir.solutions;


/** 
 * Interface for the output from one or more classifiers
 * applied to a (possibly) structured input.
 * The contents and methods vary depending on the structure of the output.
 * @author Eric Bengtson
 */
public interface Solution {
}
