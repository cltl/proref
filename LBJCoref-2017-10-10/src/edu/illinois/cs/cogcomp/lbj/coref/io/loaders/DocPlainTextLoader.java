package edu.illinois.cs.cogcomp.lbj.coref.io.loaders;

import LBJ2.classify.Classifier;
import edu.illinois.cs.cogcomp.lbj.coref.decoders.MentionDecoder;
import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;
import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.DocPlainText;


/**
 * Loads documents from the filenames listed in the specified file.
 * <p>
 * To load documents from files,
 * construct this providing the filename of a file containing
 * a list of plain-text-document filenames (one per line)
 * and then call {@link #loadDocs()}.
 * (Note: Each filename should be specified relative to
 * a location in the classpath). 
 * </p>
 * @author Eric Bengtson
 */
public class DocPlainTextLoader extends DocLoader {

    /** 
     * Constructs a loader that loads plain text files,
     * and will detect and type mentions automatically.
     * Words will be split by an automatic word splitting algorithm.
     * Sentence boundaries, quotations, and part-of-speech tags will
     * also automatically be discovered.
     * The file contains a list of filenames, one per line.
     * @param fileListFN The name of the corpus file, relative
     * to the a location in the classpath,
     * containing a list of plain text document filenames, one per line.
     * Each document filename should be specified relative to the classpath.
     * @param mentionDetector The mention detector extracts mentions from
     * a document, by predicting the head and extent boundaries of all mentions.
     * @param mTyper Determines the mention types of each mention.
     * Takes {@code Mention} objects as input and returns the type as a string,
     * "NAM", "NOM", "PRE", or "PRO".
     */
    public DocPlainTextLoader(String fileListFN,
	    MentionDecoder mentionDetector, Classifier mTyper) {
	super(fileListFN, mentionDetector, mTyper); 
    }
    
    /**
     * Constructs and returns a document
     * from the specified plain text file.
     * @param filename The name of a plain text file,
     * relative to a location in the classpath.
     * @return A document representing the specified plain text file.
     */
    @Override
    protected Doc createDoc(String filename) {
	return new DocPlainText(filename);
    }
}
