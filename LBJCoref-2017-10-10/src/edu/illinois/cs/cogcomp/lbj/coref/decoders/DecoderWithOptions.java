package edu.illinois.cs.cogcomp.lbj.coref.decoders;

import java.util.HashMap;
import java.util.Map;

import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;
import edu.illinois.cs.cogcomp.lbj.coref.ir.solutions.Solution;

/**
 * Abstract class for decoding some set of predictions
 * into an arbitrary solution type.
 * @param <ST> The solution type.
 * @author Eric Bengtson
 */
abstract public class DecoderWithOptions<ST extends Solution>
 implements SolutionDecoder<ST> {

    protected Map<String,String> m_options = new HashMap<String,String>();
    protected boolean m_train;

    /**
     * Construct a decoder.
     */
    public DecoderWithOptions() {
	super();
    }

    
    /**
     * Decode a document into a solution.
     * @param doc A document to process.
     * @return A solution representing the result of the decoding.
     */
    abstract public ST decode(Doc doc);
    
    

    /**
     * Gets the string value of some option.
     * Override to provide default values, but be sure to call super.
     * @param option The name of the option to get.
     * @return the String value for the option {@code p},
     * or {@code null} if not defined and no default specified.
     */
    public String getOption(String option) {
        return m_options.get(option);
    }

    /**
     * Gets the value of the option interpreted as a boolean.
     * Override to provide default values, but be sure to call {@code super}.
     * @param option The name of the option.
     * @return the boolean value for {@code option}
     * as defined by {@code Boolean.parseBoolean},
     * or {@code false} if not defined and no default specified.
     */
    public boolean getBooleanOption(String option) {
        if (m_options.containsKey(option)) {
            return Boolean.parseBoolean(m_options.get(option));
        } else {
            return false;
        }
    }

    /**
     * Gets the value of the option interpreted as a real number.
     * @param option The name of the option.
     * @param defaultVal The default value.
     * @return the real value for {@code >option}
     * as defined by {@code Double.parseDouble},
     * or {@code defaultVal} if not defined.
     */
    public double getRealOption(String option, double defaultVal) {
        if (m_options.containsKey(option)) {
            return Double.parseDouble(m_options.get(option));
        } else {
            return defaultVal;
        }
    }

    /** 
     * Sets the value of some option.
     * Also processes the option.
     * If overridden to handle more options, but be sure to call
     * {@code super.setOption(String, String)}.
     * If {@code super.setOption(String, String)} is called,
     * no need to override {@code setOption(String, boolean)}.
     * However, to add more options,
     * consider overriding {@code processOption} instead.
     * @param option The name of the option.
     * @param value The desired value.
     */
    public void setOption(String option, String value) {
        m_options.put(option, value);
        processOption(option, value);
    }

    /**
     * Sets the option to a boolean value.
     * If option is already a string "true" or "false",
     * call setOption(String, String) instead.
     * No need to override this method.
     * Override {@code setOption(String, String)} instead.
     * @param option The name of the option.
     * @param value The desired value.
     */
    public void setOption(String option, boolean value) {
        setOption(option, Boolean.toString(value));
    }

    /**
     * Sets the option to a real value.
     * If option is already a string,
     * call {@code setOption(String, String)} instead.
     * No need to override this method.
     * Override {@code setOption(String, String)} instead.
     * @param option The name of the option.
     * @param value The desired value.
     */
    public void setOption(String option, double value) {
        setOption(option, Double.toString(value));
    }

    /** 
     * Specify whether the algorithm is training during decode.  
     * @param train Whether training.
     */
    public void setTrain(boolean train) {
        m_train = train;
    }
    
    /**
     * Handles option changes when they happen (when setOption is called).
     * Override to create subclass specific options,
     * but be sure to call {@code super.processOptions()}.
     * @param option The name of the option, which is generally
     * all lowercase.
     * @param value The value, which may be the string representation
     * of a boolean or real value
     * (In a format supported by by Boolean.parseBoolean or Double.parseDouble)
     * or any arbitrary string. 
     */
    public void processOption(String option, String value) {
        if (option.equals("train"))
            setTrain(Boolean.parseBoolean(value));
    }

}