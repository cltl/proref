package edu.illinois.cs.cogcomp.lbj.coref.ir.relations;

import java.io.Serializable;

/**
 * Represents a relation entity argument.
 * @author Eric Bengtson
 */
public class RelationEntityArgument implements Serializable {

	private static final long serialVersionUID = 3163261790875690331L;
	public String m_id;
	public int m_argNum;

	/**
	 * Constructs a relation entity argument.
	 * @param id The ID.
	 * @param argNum The argument.
	 */
	public RelationEntityArgument(String id, int argNum) {
		m_id = id;
		m_argNum = argNum;
	}

	/**
	 * Gets a string representation of this relation entity argument.
	 * @return A string representation of this relation entity argument.
	 */
	public String toString() {
		return "<"+m_id+", "+m_argNum+">";
	}

	/**
	 * Gets the ID.
	 * @return The ID.
	 */
	public String getID() {
		return m_id;
	}
}
