package edu.illinois.cs.cogcomp.lbj.coref;

import java.util.List;
import java.util.ArrayList;
import java.io.*;

import LBJ2.parse.LineByLine;
import edu.illinois.cs.cogcomp.lbj.coref.decoders.BestLinkDecoder;
import edu.illinois.cs.cogcomp.lbj.coref.decoders.MentionDecoder;
import edu.illinois.cs.cogcomp.lbj.coref.decoders.ExtendHeadsDecoder;
import edu.illinois.cs.cogcomp.lbj.coref.decoders.BIODecoder;
import edu.illinois.cs.cogcomp.lbj.coref.io.loaders.DocLoader;
import edu.illinois.cs.cogcomp.lbj.coref.io.loaders.DocFromTextLoader;
import edu.illinois.cs.cogcomp.lbj.coref.ir.Mention;
import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;
import edu.illinois.cs.cogcomp.lbj.coref.ir.solutions.ChainSolution;
import edu.illinois.cs.cogcomp.lbj.coref.learned.Emnlp8;
import edu.illinois.cs.cogcomp.lbj.coref.learned.MDExtendHeads;
import edu.illinois.cs.cogcomp.lbj.coref.learned.MentionDetectorMyBIOHead;
import edu.illinois.cs.cogcomp.lbj.coref.learned.MTypePredictor;


/**
  * Use this program to add coreference annotation to a plain text input file.
  * Note that it requires at least 1 GB of memory to run correctly.
  *
  * <h4>Usage</h4>
  * <blockquote><pre>
  *   java -Xmx2g edu.illinois.cs.cogcomp.lbj.coref.CorefPlainText &lt;text file&gt;
  * </pre></blockquote>
  *
  * <h4>Input</h4>
  * <p> <code>&lt;text file&gt;</code> is the absolute or relative path and
  * name of the input file, which should contain naturally written,
  * unannotated, plain English text.
  *
  * <h4>Output</h4>
  * <p> The output is the same plain text that appeared in the input file
  * after being annotated by the coreference classifier.  Annotations mark
  * sequences of words as <i>mentions</i> of an <i>entity</i>.  A single
  * asterisk character (<code>*</code>) is prepended to the first word of the
  * mention.  An asterisk followed by an underscore (<code>_</code>) and an
  * integer is appended to the last word of the mention.  Mentions which the
  * classifier believes refer to the same entity will be annotated with the
  * same integer.  Mentions can be nested, but they never overlap otherwise.
 **/
public class CorefPlainText
{
  public static void main(String[] args)
  {
    String plainTextFile = null;

    try
    {
      plainTextFile = args[0];
      if (args.length > 1) throw new Exception();
    }
    catch (Exception e)
    {
      System.err.println(
  "usage: java -Xmx2g edu.illinois.cs.cogcomp.lbj.coref.CorefPlainText "
+ "<text file>\n");
      System.exit(1);
    }

    BufferedReader in = null;

    try { in = new BufferedReader(new FileReader(plainTextFile)); }
    catch (Exception e)
    {
      System.err.println("Can't open '" + plainTextFile + "' for input:");
      e.printStackTrace();
      System.exit(1);
    }

    String fullText = "";
    for (String line = readLine(in, plainTextFile); line != null;
         line = readLine(in, plainTextFile))
      fullText += line + "\n";

    // Detecting the mentions in the plain text:
		MentionDecoder mdDec =
      new ExtendHeadsDecoder(new MDExtendHeads(),
                             new BIODecoder(new MentionDetectorMyBIOHead()));
		MTypePredictor mTyper = new MTypePredictor();
		DocLoader loader = new DocFromTextLoader(mdDec, mTyper); // From a string
    Doc doc = loader.loadDoc(fullText);

    //Setting up the coreference algorithm:
    Emnlp8 corefClassifier = new Emnlp8();
    corefClassifier.setThreshold(-8.0);
    BestLinkDecoder decoder = new BestLinkDecoder(corefClassifier);

    //Applying coreference to all documents:
    List<ChainSolution<Mention>> preds =
      new ArrayList<ChainSolution<Mention>>();

    ChainSolution<Mention> sol = decoder.decode(doc);
    preds.add(sol); 
    doc.setPredEntities(sol);
    System.out.println(doc.toAnnotatedString(false, false, false, true));
  }


  /**
    * Read a line from an input stream, printing an error message and
    * terminating the program on error.
    *
    * @param in       The stream to read from.
    * @param filename The name of the file that the stream is reading from.
    * @return A line of text from the input stream without any newline
    *         character the line may have contained at the end.
   **/
  private static String readLine(BufferedReader in, String filename)
  {
    String line = null;

    try { line = in.readLine(); }
    catch (Exception e)
    {
      System.err.println("Can't read from '" + filename + "':");
      e.printStackTrace();
      System.exit(1);
    }

    return line;
  }
}

