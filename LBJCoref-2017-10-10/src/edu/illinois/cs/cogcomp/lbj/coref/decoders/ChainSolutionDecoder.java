package edu.illinois.cs.cogcomp.lbj.coref.decoders;


import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;
import edu.illinois.cs.cogcomp.lbj.coref.ir.solutions.ChainSolution;

/**
 * Base class for decoding decisions into chain solutions.
 * This is the base class for coreference decoders.
 * Also provides facilities for setting and getting options.
 * @param ST The solution type, which is a type of {@code ChainSolution}.
 * @param CT The type of elements in the solution.
 * @param ET The element type.
 * @author Eric Bengtson
 */
abstract public class ChainSolutionDecoder<ST extends ChainSolution<CT>,CT,ET>
extends DecoderWithOptions<ST> {
    
    /**
     * Decodes a document, typically by translating classifier decisions
     * to a solution type.
     * @param doc The document to decode.
     * @return A solution.
     */
    abstract public ST decode(Doc doc);

    /**
     * Get statistics encoded as a string.
     * Override to generate a decoder-specific statistics string.
     * @return A statistics string.
     */
    public String getStatsString() { return ""; }
}
