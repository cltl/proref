package edu.illinois.cs.cogcomp.lbj.coref.util.collections;

import java.util.*;

//TODO: Should this extend Map?
public class Histogram<T> {
    Map<T,Integer> m_map;

    public Histogram() {
	m_map = new HashMap<T,Integer>();
    }

    /** Creates a new histogram,
	incrementing by one for each occurance of an item in the Collection. */
    public Histogram(Collection<T> items) {
	this();
	for (T item : items) {
	    increment(item);
	}
    }

    /** increment each key in histogram by the corresponding value. */
    public void addAll(Map<T,Integer> histogram) {
	for (T item : histogram.keySet())
	    increment(item, histogram.get(item));
    }

    /** Gets the count of item.  If item is unseen, returns 0. */
    public int get(T item) {
	if (m_map.containsKey(item))
	    return m_map.get(item);
	else
	    return 0;
    }
    
    /** Sets the histogram's value of <code>item</code> to <code>to</code>. */
    public void set(T item, int to) {
	m_map.put(item, to);
    }

    /** Increment the count for item, setting it to one if never seen before. */
    public void increment(T item) {
	m_map.put(item, get(item) + 1);
    }
    public void increment(T item, int by) {
	m_map.put(item, get(item) + by);
    }


    public Set<T> keySet() {
	return m_map.keySet();
    }
}
