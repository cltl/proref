package edu.illinois.cs.cogcomp.lbj.coref.decoders;

import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;
import edu.illinois.cs.cogcomp.lbj.coref.ir.solutions.Solution;

/**
 * Interface for decoders that extract the gold (true) values,
 * or keys, from a document.
 * @param <ST> The type of solution produced by the decode method.
 * @author Eric Bengtson
 */
public interface KeyDecoder<ST extends Solution> extends SolutionDecoder<ST> {
    
    /**
     * Decodes a document.
     * @param doc The document to decode.
     * @return A solution.
     */
    public ST decode(Doc doc);
}
