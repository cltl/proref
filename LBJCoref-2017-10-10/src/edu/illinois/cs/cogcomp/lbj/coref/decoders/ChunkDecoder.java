package edu.illinois.cs.cogcomp.lbj.coref.decoders;

import LBJ2.classify.Classifier;
import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;
import edu.illinois.cs.cogcomp.lbj.coref.ir.solutions.ChunkSolution;



/**
 * A decoder that translates a document to a ChunkSolution.
 * @author Eric Bengtson
 */
abstract public class ChunkDecoder extends DecoderWithOptions<ChunkSolution> {
    protected Classifier m_classifier;
    
    /**
     * Constructs a chunk decoder that does not use a classifier.
     * The decode method may throw a {@code NullPointerException}
     * if the classifier is not set and is accessed.
     */
    public ChunkDecoder() {
    }

    /**
     * Constructor for use when a Classifier is used in the decoding
     * process.
     * @param classifier The classifier.
     */
    public ChunkDecoder(Classifier classifier) {
	m_classifier = classifier;
    }
    
    /**
     * Translates a document into a set of chunks
     * represented by a {@code ChunkSolution},
     * generally by applying a given classifier.
     * @param doc A document.
     * @return A {@code ChunkSolution} representing a set of chunks.
     */
    abstract public ChunkSolution decode(Doc doc);
}
