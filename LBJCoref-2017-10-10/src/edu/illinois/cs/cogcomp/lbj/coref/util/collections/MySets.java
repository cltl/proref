package edu.illinois.cs.cogcomp.lbj.coref.util.collections;

import java.util.Collection;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

public class MySets {
    /** @return a new Set that contains all objects found in both a and b. */
    public static <T> Set<T> getIntersection(Collection<T> a, Collection<T> b) {
	Set<T> r = new HashSet<T>(a);
	r.retainAll(new HashSet<T>(b));
	return r;
    }

    /** @return a new Set that contains all objects found in either a or b. */
    public static <T> Set<T> getUnion(Collection<T> a, Collection<T> b) {
	Set<T> r = new HashSet<T>(a);
	r.addAll(b);
	return r;
    }

    /** @return a new Set containing
	all objects that are in a and are not in b.
	i.e. returns a - b.
	Note that this difference is NOT symmetric.
    */
    public static <T> Set<T> getDifference(Collection<T> a, Collection<T> b) {
	Set<T> r = new HashSet<T>(a);
	r.removeAll(b);
	return r;
    }

    /** @return A new Set containing the objects in exactly one of a or b. */
    public static <T> Set<T> getSymmetricDifference(
     Collection<T> a, Collection<T> b) {
	Set<T> r = new HashSet<T>(a);
	for (T bObj : b) {
	    if (r.contains(bObj)) r.remove(bObj);
	    else r.add(bObj);
	}
	return r;
    }
}
