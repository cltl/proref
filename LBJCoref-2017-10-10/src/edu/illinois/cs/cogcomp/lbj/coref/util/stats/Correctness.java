/*
  * Created on January 7, 2006
 */
package edu.illinois.cs.cogcomp.lbj.coref.util.stats;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.illinois.cs.cogcomp.lbj.coref.util.aux.Maps;

import LBJ2.classify.Classifier;

public class Correctness {

  public Correctness() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
    * Calculates the F1 for Classifiers that return a multi-class label,
    * all of which labels are considered positive except the "NONE",
    * "negative", and "false" labels.
    * 
    * @param predictor The predicting classifier.
    * @param oracle The true classifier.
    * @param examples The examples over which to evaulate the predictor.
    * @return The F-Score.
   */
  static public double calcMultiNoneF1(Classifier predictor,
                                       Classifier oracle, List<? extends Object> examples)
  {
    int truePos = 0, predPos = 0, correctPos = 0;
    for (Object ex : examples) {
      String label = oracle.discreteValue(ex);
      String pred = predictor.discreteValue(ex);
      if (!label.equals("false") && !label.equals("negative")
          && !label.equals("NONE"))
        truePos++;
      if (!pred.equals("false") && !pred.equals("negative")
          && !pred.equals("NONE"))
        predPos++;
      if (label.equals(pred) && !label.equals("false")
          && !label.equals("negative") && !label.equals("NONE")
          && !pred.equals("false") && !pred.equals("negative")
          && !pred.equals("NONE"))
        correctPos++;
    }
    return calcF1(correctPos, truePos, predPos);
  }

  /**
    * @param tested
    *                The Classifier producing labels to be tested.
    * @param correct
    *                The classifier producing known correct labels.
    * @param examples
    *                A list of examples to test.
    * @return A combined F (Beta=1) Score.
   */
  static public double calcF1Multi(Classifier tested, Classifier correct,
                                   List<? extends Object> examples) {
    Map<String,Integer> mTruePosPredicts = new HashMap<String,Integer>();
    Map<String,Integer> mTruePos = new HashMap<String,Integer>();
    Map<String,Integer> mPosPredicts = new HashMap<String,Integer>();
    for (Object ex : examples) {
      String label =  correct.discreteValue(ex);
      String pred = tested.discreteValue(ex);
      // TODO: Verify this' sensibility

      if (!label.equals("false") && !label.equals("negative")
          && !label.equals("NONE"))
        Maps.addOne(mTruePos, label);
      if (!pred.equals("false") && !pred.equals("negative")
          && !pred.equals("NONE"))
        Maps.addOne(mPosPredicts, pred);
      if (label.equals(pred) && !label.equals("false")
          && !label.equals("negative") && !label.equals("NONE"))
        Maps.addOne(mTruePosPredicts, label);
    }
    return calcF1Multi(mTruePosPredicts, mTruePos, mPosPredicts);
  }

  /**
    * Currently calculates based on the harmonic mean of the individual
    * labels.
   */
  static public double calcF1Multi(Map<String,Integer> truePosPredicts,
                                   Map<String,Integer> truePos, Map<String,Integer> posPredicts) {
    /* Harmonic mean is n / (1/f1 + 1/f2 + ... + 1/fn) */
    int n = 0;
    double totalInvF1 = 0.0;
    /* TODO: Check validity */
    /*
      * Only calculate F-Measure for labels with at least one of each of
      * truePosPredicts, truePos, and posPredicts
     */
    for (String id : truePosPredicts.keySet()) {
      if (!truePos.containsKey(id) || !posPredicts.containsKey(id))
        continue;
      int nTruePP = truePosPredicts.get(id);
      int nTruePos = truePos.get(id);
      int nPosPred = posPredicts.get(id);
      totalInvF1 += 1 / calcF1(nTruePP, nTruePos, nPosPred);
      n++;
    }
    return (double) n / totalInvF1;
  }

  /**
    * Since labels are always positive for their class (even in the two
    * class (e.g. binary) case, we assume that positive labels start with
    * T, t, P, or p and negative start with anything else (e.g. F, f, N, or
    * n)
    * 
    * @param tested
    *                Classifier to be tested
    * @param correct
    *                Classifier producing correct labels
    * @param examples
    *                List of examples to classify
    * @return F measure with Beta=1.
   */
  static public double calcF1(Classifier tested, Classifier correct,
                              List<? extends Object> examples) {
    List<String> predicted = new ArrayList<String>();
    List<String> gold = new ArrayList<String>();
    for (Object ex : examples) {
      String label = correct.discreteValue(ex);
      gold.add(label);
      String pred = tested.discreteValue(ex);
      predicted.add(pred);
    }
    return calcF1(gold, predicted);
  }

  static public double calcF1(List<String> gold,
                              List<String> predicted) {
    int smallSize = gold.size();
    if (predicted.size() < gold.size())
      smallSize = predicted.size();
    int truePosPredicts = 0;
    int truePos = 0;
    int posPredicts = 0;
    int totalPredicts = 0;
    for (int i = 0; i < smallSize; ++i) {
      String label = gold.get(i);
      String pred = predicted.get(i);
      if (label.length() == 0 || pred.length() == 0)
        continue; // Ignore it 
      char l0 = label.toLowerCase().charAt(0);
      char p0 = pred.toLowerCase().charAt(0);
      boolean bLabelPos = false;
      boolean bPredPos = false;
      if (l0 == 't' || l0 == 'p') {
        bLabelPos = true;
        truePos++;
      }
      if (p0 == 't' || p0 == 'p') {
        bPredPos = true;
        posPredicts++;
      }
      if (bLabelPos == true && bPredPos == true)
        truePosPredicts++;
      totalPredicts++;
    }
    return calcF1(truePosPredicts, truePos, posPredicts);
  }

  static public double calcF1(int truePosPredicts, int truePos,
                              int posPredicts) {
    double p = calcPrecision(truePosPredicts, posPredicts);
    double r = calcRecall(truePosPredicts, truePos);
    return calcF1(p, r);
  }

  static public double calcF1(double p, double r) {
    /* Avoid Div By Zero */
    if (p + r == 0) {
      if (p == 0 || r == 0) {
        System.err.println("WARNING: F1 = 0/0");
        return 0;
      } else {
        System.err.println("WARNING: F1 = " + (2 * p * r) + "/0");
        return Double.NaN;
      }
    }
    return 2 * p * r / (p + r);	
  }

  static public double calcPrecision(int truePosPredicts, int posPredicts) {
    /* Avoid Div By Zero: */
    if (posPredicts == 0) {
      if (truePosPredicts == 0) {
        System.err.println("WARNING: 0/0 precision calculated as 1.0.");
        return 1; // TODO: is this value correct?
      } else {
        System.err.println("WARNING: precision is " + truePosPredicts
                           + "/0");
        return Double.NaN; // TODO: Better value?
      }
    }
    return (double) truePosPredicts / (double) posPredicts;
  }

  static public double calcRecall(int truePosPredicts, int truePos) {
    /* Avoid Div By Zero: */
    if (truePos == 0) {
      if (truePosPredicts == 0) {
        System.err.println("WARNING: 0/0 recall calculated as 1.0.");
        return 1; // TODO: is this value correct?
      } else {
        System.err.println("WARNING: recall is " + truePosPredicts
                           + "/0");
        return Double.NaN; // TODO: Better value?
      }
    }
    return (double) truePosPredicts / (double) truePos;
  }

}
