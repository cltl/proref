package edu.illinois.cs.cogcomp.lbj.coref.test;

import java.util.List;

import edu.illinois.cs.cogcomp.lbj.coref.decoders.BestLinkDecoder;
import edu.illinois.cs.cogcomp.lbj.coref.io.loaders.DocAPFLoader;
import edu.illinois.cs.cogcomp.lbj.coref.io.loaders.DocLoader;
import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;
import edu.illinois.cs.cogcomp.lbj.coref.ir.solutions.ChainSolution;
import edu.illinois.cs.cogcomp.lbj.coref.ir.Mention;
import edu.illinois.cs.cogcomp.lbj.coref.learned.Emnlp8;

import LBJ2.classify.TestDiscrete;
import LBJ2.util.ClassUtils;
import LBJ2.util.TableFormat;


/**
  * Computes statistics over the specified implication.
  *
  * @author Nick Rizzolo
 **/
public class ConstraintStatistics
{
  public static void main(String[] args) {
    String corpus = null;
    String constraintName = null;
    boolean verbose = false;

    try {
      corpus = args[0];
      constraintName = args[1];
      if (args.length == 3) {
        verbose = args[2].equalsIgnoreCase("true");
        if (!verbose && !args[2].equalsIgnoreCase("false"))
          throw new Exception();
      }
      else if (args.length > 3) throw new Exception();
    }
    catch (Exception e) {
      System.err.println(
  "usage: java edu.illinois.cs.cogcomp.lbj.coref.ConstraintStatistics \\\n"
+ "              <corpus> <constraint> [<verbose=false>]");
      System.exit(1);
    }

    Constraint constraint = getConstraint(constraintName);
    DocLoader loader = new DocAPFLoader(corpus);
    List<Doc> docs = loader.loadDocs();

    Emnlp8 corefClassifier = new Emnlp8();
    corefClassifier.setThreshold(-8.0);
    BestLinkDecoder decoder = new BestLinkDecoder(corefClassifier);

    double[][] data = new double[2][2];

    for (Doc d : docs) {
      ChainSolution<Mention> sol = decoder.decode(d);
      d.setPredEntities(sol);
      List<List<Mention[]>> results = constraint.constrain(d);

      if (verbose) {
        System.out.println(d.getBaseFilename() + ":");
        System.out.println("  Constraint forces coref:");
      }

      for (Mention[] pair : results.get(0)) {
        boolean label =
          pair[0].getTrueEntityID().equals(pair[1].getTrueEntityID());
        boolean prediction = pair[0].getCorefMents().contains(pair[1]);
        int c = label ? 0 : 1;
        int r = label == prediction ? 0 : 1;
        ++data[r][c];
        if (verbose) mentionPairOutput(d, pair, label, prediction);
      }

      if (verbose) System.out.println("  Constraint forces not coref:");

      for (Mention[] pair : results.get(1)) {
        boolean label =
          pair[0].getTrueEntityID().equals(pair[1].getTrueEntityID());
        boolean prediction = pair[0].getCorefMents().contains(pair[1]);
        int c = label ? 1 : 0;
        int r = label == prediction ? 0 : 1;
        ++data[r][c];
        if (verbose) mentionPairOutput(d, pair, label, prediction);
      }

      if (verbose) System.out.println();
    }

    String[] columnLabels = { "constraint ->", "right", "wrong" };
    String[] rowLabels = { "prediction right", "prediction wrong" };
    int[] sigDigits = { 0, 0 };
    TableFormat.printTableFormat(System.out, columnLabels, rowLabels, data,
                                 sigDigits);
  }


  private static Constraint getConstraint(String name) {
    Class c = ClassUtils.getClass(name);
    Constraint result = null;

    try { result = (Constraint) c.newInstance(); }
    catch (Exception e) {
      System.err.println(
          "Can't instantiate condition '" + name
          + "'.  Make sure it implements ConstraintStatistics.Constraint and"
          + " is on the CLASSPATH.");
      System.exit(1);
    }

    return result;
  }


  private static void mentionPairOutput(Doc d, Mention[] pair, boolean label,
                                        boolean prediction) {
    int firstWord0 = pair[0].getExtentFirstWordNum(), w = firstWord0;
    int lastWord0 = pair[0].getExtentLastWordNum();
    int firstWord1 = pair[1].getExtentFirstWordNum();
    int lastWord1 = pair[1].getExtentLastWordNum();
    int s = pair[0].getSentNum();
    while (w > 0 && d.getSentNum(w - 1) == s) --w;

    int N = d.getWords().size();
    StringBuffer sentence = new StringBuffer();
    sentence.append("   ");
    for (; w < N && d.getSentNum(w) == s; ++w) {
      sentence.append(' ');
      if (w == firstWord0) sentence.append("*_1 ");
      if (w == firstWord1) sentence.append("*_2 ");
      sentence.append(d.getWord(w));
      if (w == lastWord1) sentence.append(" *_2");
      if (w == lastWord0) sentence.append(" *_1");
    }

    System.out.println(sentence);
    System.out.println(
        "      label: " + (label ? "" : "not ") + "corefed");
    System.out.println(
        "      prediction: " + (prediction ? "" : "not ") + "corefed");
  }


  public static interface Constraint
  {
    /**
      * Given a document, this method compiles a list of mention pairs which
      * this constraint will force to be coreferent and another list of
      * mention pairs that it will force to be non-coreferent.  The two lists
      * are themselves returned in a list.
      *
      * @param d  The document to constrain.
      * @return A list of two lists as described above.
     **/
    public List<List<Mention[]>> constrain(Doc d);
  }
}

