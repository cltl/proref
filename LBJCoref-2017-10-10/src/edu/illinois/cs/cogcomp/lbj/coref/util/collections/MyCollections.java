package edu.illinois.cs.cogcomp.lbj.coref.util.collections;

import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.ListIterator;

public class MyCollections {
    public MyCollections() {
	
    }
    /** removes duplicates from a _sorted_ list
	@param sortedList a sorted list
    */
    public static <T> List<T> getWithoutDuplicates(List<T> sortedList) {
	//First make into a linked list for quick removals:
	//(As a bonus, makes a copy at no extra charge.)
	List<T> list = new LinkedList<T>(sortedList);
	ListIterator<T> it = list.listIterator();
	if (!it.hasNext())
	    return list;
	T prev = it.next();
	while (it.hasNext()) {
	    T curr = it.next();
	    if (curr.equals(prev))
		it.remove();
	}
	return list;
    }
 
    public static double average(List<Double> list) {
	double total = 0.0;
	int n = list.size();
	for (double val : list) {
	    total += val / n;
	}
	return total;
    }
}
