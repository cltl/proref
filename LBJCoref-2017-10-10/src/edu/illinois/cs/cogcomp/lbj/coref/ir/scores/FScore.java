package edu.illinois.cs.cogcomp.lbj.coref.ir.scores;

import edu.illinois.cs.cogcomp.lbj.coref.util.stats.Correctness;

/** Author: Eric Bengtson */

/** Stores correctPredictionCount, PredictionCount, and targetCount,
  and calculates and returns Precision, Recall, and F-Measure.
 */
public class FScore extends Score {
  /* TODO: A more efficient implementation that still allows setting
     p/r directly, or split into two classes. */
  /* These might be empty (0) */
  private int m_correctPredictions;
  private int m_targets;
  private int m_predictions;

  /* These should always have meaningful values, BUT may not be updated until
     a getter function is called. (So always access them via a getter) */
  private double m_precision;
  private double m_recall;

  public FScore() {
    this(0);
  }

  public FScore(int targetCount) {
    m_correctPredictions = 0;
    m_targets = targetCount;
    m_predictions = 0;
  }

  public FScore(double precision, double recall) {
    m_precision = precision;
    m_recall = recall;
  }

  public FScore(int correctPredictions, int targets, int predictions) {
    m_correctPredictions = correctPredictions;
    m_predictions = predictions;
    m_targets = targets;
    updatePR();
  }

  private void updatePR() {
    m_precision 
      = Correctness.calcPrecision(m_correctPredictions, m_predictions);
    m_recall
      = Correctness.calcRecall(m_correctPredictions, m_targets);
  }

  /* These methods are slow due to updating frequently. */
  public void recordCorrectPrediction() {
    m_correctPredictions++;
    m_predictions++;
    updatePR();
  }
  public void recordMistakenPrediction() {
    m_predictions++;
    updatePR();
  }


  public void setTargetCount(int targets) {
    m_targets = targets;
  }

  public double getPrecision() {
    return m_precision;
  }

  public double getRecall() {
    return m_recall;
  }

  public double getF1() {
    return Correctness.calcF1(m_precision, m_recall);
  }

  public double getScoreDouble() {
    return this.getF1();
  }

  public String getScoreString() {
    String str = "";
    String newline = System.getProperty("line.separator");
    str += "Precision = " + this.getPrecision() + "," + newline;
    str += "Recall = " + this.getRecall() + "," + newline;
    str += "F-Measure = " + this.getF1() + "," + newline;
    return str;   
  }
}
