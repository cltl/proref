package edu.illinois.cs.cogcomp.lbj.coref.decoders;

import java.io.Serializable;

import LBJ2.classify.Classifier;
import edu.illinois.cs.cogcomp.lbj.coref.ir.Mention;
import edu.illinois.cs.cogcomp.lbj.coref.ir.examples.CExample;
import edu.illinois.cs.cogcomp.lbj.coref.ir.solutions.ChainSolution;


/** 
 * Translates coreference decisions to a ChainSolution via the decode
 * method.  Accepts a classifier that determines coreference of
 * {@code CExample} objects by returning "true" or "false".
 * Any parameters of the classifier should be set before constructing.
 * For compatibility with code that loads dynamically (such as the GUI),
 * subclasses should provide a constructor that takes
 * just a {@code Classifier}.
 * Exception: Classes that need more specific inputs to function
 * may omit this constructor, provided they comply with the subclass 
 * constructor requirements. (Currently, see {@code ScoredCorefDecoder}).
 * @author Eric Bengtson
 */
abstract public class CorefDecoder
 extends ChainSolutionDecoder<ChainSolution<Mention>,Mention,CExample>
 implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    /** The default classifier */
    protected Classifier m_classifier = null;
    
    /**
     * Construct a coreference decoder that uses a single classifier.
     * @param c A classifier that takes {@code CExample}s
     * and returns a "true" or "false".
     * Any parameters (such as any threshold) are assumed to have been set.
     */
    public CorefDecoder(Classifier c) {
	super();
	m_classifier = c;
    }    

    /**
     * Gets the coreference classifier.
     * @return The classifier.
     */
    public Classifier getClassifier() {
	return m_classifier;
    }

    /**
     * Sets the coreference classifier.
     * @param classifier The classifier.
     */
    public void setClassifier(Classifier classifier) {
	m_classifier = classifier;
    }

    /**
     * Indicates whether the member classifier considers the example
     * to be coreferential.
     * Override to modify coreference behavior to use other decision strategies.
     * @param ex The example whose coreference should be determined.
     * @return Whether the example is coreferential.
     */
    protected boolean predictedCoreferential(CExample ex) {
	if ( m_classifier.discreteValue(ex).equals("true") )
	    return true;
	else return false;
    }
    
}
