package edu.illinois.cs.cogcomp.lbj.coref.ir.solutions;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import edu.illinois.cs.cogcomp.lbj.coref.ir.Chunk;


/**
 * A ChunkSolution represents a set of chunks.
 * @author Eric Bengtson
 */
public class ChunkSolution extends SetSolution<Chunk> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * Constructs an empty chunk solution.
     */
    public ChunkSolution() {
	super();
    }

    /**
     * Constructs a ChainSolution containing some chunks.
     * @param chunks Some chunks.
     */
    public ChunkSolution(Collection<Chunk> chunks) {
	super(chunks);
    }

    /**
     * Gets the chunks of the chainSolution.
     * @return An unmodifiable view of the chunks.
     */
    public Set<Chunk> getChunks() {
	return getItems();
    }
}
