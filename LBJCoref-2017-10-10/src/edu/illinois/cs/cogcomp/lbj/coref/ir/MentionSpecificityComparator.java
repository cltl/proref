package edu.illinois.cs.cogcomp.lbj.coref.ir;

import java.util.Comparator;

/**
 * Determines which mention is a more specific mention of an entity.
 * Specificity is defined as having a more specific mention type
 * Proper names are more specific than nominals, which are more specific
 * than pronouns; or if not more specific, being longer.
 * NOTE: Two mentions do not need to be coreferential for their specificities
 * to be compared.
 */
public class MentionSpecificityComparator
 implements Comparator<Mention> {

    /** Constructs a mention specificity comparator. */
    public MentionSpecificityComparator() {
    }

    /**
     * Compares mentions by specificity in descending order.
     * Specificity is defined as having a more specific mention type
     * Proper names are more specific than nominals, which are more specific
     * than pronouns; or if not more specific, being longer.
     * @param a A mention.
     * @param b Another mention.
     * @return -1 if {@code a} is more specific 0 if they are the same
     * and 1 if {@code b} is more specific.
     */
    public int compare(Mention a, Mention b) {
	return compareBySpecificity(b, a); //Descending order
    }

    //OPTIMIZE:
    /**
     * Determines which mention is more specific, in ascending order.
     * Specificity is defined as having a more specific mention type
     * Proper names are more specific than nominals, which are more specific
     * than pronouns; or if not more specific, being longer.
     * Arguments may be null, and null is always the least specific.
     * @param a A mention.
     * @param b Another mention.
     * @return -1 if {@code a} is strictly less specific than {@code b},
     * 0 if their specificities are the same,
     * or 1 if {@code a} is more specific.
     */
    protected int compareBySpecificity(Mention a, Mention b) {
	if (a == null) { //a can't be more specific
	    if (b != null)
		return -1;
	    else
		return 0;
	} else if (b == null) { //and a not null: a must be more specific
	    return 1;
	}

	if (moreSpecificType(a, b)) {
	    return 1;
	} else if (moreSpecificType(b, a)) {
	    return -1;
	} else {
	    if (moreSpecificSameType(a, b))
		return 1;
	    else if (moreSpecificSameType(b, a))
		return -1;
	    else
		return 0;
	}
    }
    /**
     * Determines whether {@code a} is more specific than {@code b},
     * given that they have the same mention type.
     * @param a A mention.
     * @param b Another mention.
     * @return true when {@code a} is strictly more specific that {@code b},
     * assuming their mention types are equivalent.
     */
    protected boolean moreSpecificSameType(Mention a, Mention b) {
	Chunk aExtent = a.getExtent(), bExtent = b.getExtent();
	if (aExtent.getText().length() > bExtent.getText().length()) {
	    return true;
	} else if (aExtent.getStartWN() < bExtent.getStartWN()) {
	    return true;
	} else {
	    return false;
	}
    }
    /**
     * Determines whether {@code a}'s mention type
     * is strictly more specific than {@code b}'s.
     * Mention types, in decreasing order of specificity:
     * NAM, NOM, PRE, PRO, NONE
     * @param a A mention.
     * @param b Another mention.
     * @return true when {@code a}'s mention type
     * is strictly more specific than {@code b}'s.
     */
    protected boolean moreSpecificType(Mention a, Mention b) {
	String at = a.getType(), bt = b.getType();
	if (at.equals("NAM") && !bt.equals("NAM"))
	    return true;
	else if (at.equals("NOM") && !bt.equals("NAM") && !bt.equals("NOM"))
	    return true;
	else if (at.equals("PRE") && (bt.equals("PRO") || bt.equals("NONE")) )
	    return true;
	else if (bt.equals("NONE"))
	    return true;
	else
	    return false;
    }


}
