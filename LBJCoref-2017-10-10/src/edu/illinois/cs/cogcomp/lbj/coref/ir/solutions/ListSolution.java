package edu.illinois.cs.cogcomp.lbj.coref.ir.solutions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * A solution containing a set of objects of some type {@code T} 
 * @param T the type of object contained.
 * @author Eric Bengtson
 */
public class ListSolution<T> implements Solution, Serializable {
    private static final long serialVersionUID = 1L;
    private List<T> m_items;

    /**
     * Constructs an empty {@code ListSolution}.
     */
    public ListSolution() {
	m_items = new ArrayList<T>();
    }

    /**
     * Constructs a solution containing {@code items} as a list.
     * @param items The items (copied defensively into a list).
     */
    public ListSolution(Collection<T> items) {
	m_items = new ArrayList<T>(items);
    }

    /**
     * Gets the items.
     * @return An unmodifiable view of the items.
     */
    public List<T> getItems() {
	return Collections.unmodifiableList(m_items);
    }

    /**
     * Get a string representation of the items.
     */
    public String toString() {
	String s = "";
	for (T c : getItems()) {
	    s += c.toString() + "\n";
	}
	return s;
    }
}
