package edu.illinois.cs.cogcomp.lbj.coref.ir.examples;

import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;

/**
 * Represents a word in a document, for use in classification
 * or classifier training, along with utility methods for getting information
 * and features pertaining to the word.
 */
public class WordExample extends Example {

    /** The document containing the example word. */
    protected Doc m_doc;
    
    /** The (zero-based) position of the example word in the document. */
    protected int m_wordN;
    
    /** The number of words in the document. */
    protected int m_numWords;

    
    /* Constructors */
    
    /**
     * Constructs an example that does not refer to any word.
     * Not recommended for use unless word will be set.
     */
    public WordExample() {
	super();
    }
    
    /**
     * Constructs a word example from a given document and word number.
     * @param doc The document containing the example word.
     * @param wordNum The (zero-based) position of the word in the document.
     */
    public WordExample(Doc doc, int wordNum) {
	m_doc = doc;
	m_wordN = wordNum;
    }

    
    
    /* Accessors */

    public Doc getDoc() {
        return m_doc;
    }

    public int getWordNum() {
        return m_wordN;
    }


    /* Utilities and features */
    
    /**
     * Gets a lowercase version of the word.
     * @return the word, in lowercase.
     */
    public String getWord() {
        return m_doc.getWord(m_wordN).toLowerCase();
    }
    
    /**
     * Gets a lowercase version of the word at the word number plus the offset,
     * or the empty string if no such word exists.
     * @param offset The position relative to the word number
     * of the desired word.
     * @return the word, in lowercase, or the empty string if no such word.
     */
    public String getWord(int offset) {
        int targetN = m_wordN + offset;
        if (targetN < 0 || targetN >= m_doc.getWords().size()) {
            return "";
        }
        else {
            return m_doc.getWord(targetN).toLowerCase();
        }
    }
    
    /** 
     * Gets the word at the word number plus the offset,
     * without altering original case;
     * Note this does NOT mean that any extra casing is applied here
     * (although it may be applied at the Doc parsing stage).
     * @return The word (without altering its case).
     */
    public String getCasedWord() {
        return m_doc.getWord(m_wordN);
    }

    /** 
     * Gets the word without altering original case;
     * Note this does NOT mean that any extra casing is applied here
     * (although it may be applied at the Doc parsing stage.)
     * @param offset The position relative to the word number
     * of the desired word.
     * @return The word (without altering its case), or the empty string
     * if no such word exists.
     */
    public String getCasedWord(int offset) {
        int targetN = m_wordN + offset;
        return m_doc.getWord(targetN);
    }

    /**
     * Gets the part-of-speech (POS) tag of the word represented by this example.
     * @return The part-of-speech tag of the word.
     * @see Doc#getPOS(int)
     */
    public String getPOS() {
        return m_doc.getPOS(m_wordN);
    }

    /**
     * Gets the part-of-speech (POS) tag of the word
     * at the word number plus the offset.
     * @param offset The position relative to the word number
     * of the desired word's POS tag.
     * @return The part-of-speech tag of the specified word.
     * @see Doc#getPOS(int)
     */
    public String getPOS(int offset) {
        int targetN = m_wordN + offset;
        if (targetN < 0 || targetN >= m_doc.getWords().size()) {
            return "";
        }
        else {
            return m_doc.getPOS(targetN);
        }
    }

    /** 
     * Gets a description of the case of the word.
     * @return "allLower", "firstCap", "allCaps", "multiCase", "digit", "punc", "other"
     * In case of a single-character uppercase word, returns "firstCap"
     * Words beginning with a digit are "digit".  This implies that words
     * containing internal digits can still be "allLower" or "allUpper".
     * Words beginning with punctuation are "punc", and word-internal punc is
     * not considered "punc".
     * Zero length words are "other".
     * Words beginning with whitespace are "other".
     * multiCase means mixed case, but "mixed*" as a feature is disallowed.
     */
    public String getWordCase() {
        return getWordCase(0);
    }

    /** 
     * Gets a description of the case of the specified word.
     * @param offset The position relative to the word number
     * of the specified word. 
     * @return "allLower", "firstCap", "allCaps", "multiCase", "digit", "punc", "other"
     * In case of a single-character uppercase word, returns "firstCap"
     * Words beginning with a digit are "digit".  This implies that words
     * containing internal digits can still be "allLower" or "allUpper".
     * Words beginning with punctuation are "punc", and word-internal punc is
     * not considered "punc".
     * Zero length words are "other".
     * Words beginning with whitespace are "other".
     * multiCase means mixed case, but "mixed*" as a feature is disallowed.
     */
    public String getWordCase(int offset) {
        if (!m_doc.isCaseSensitive())
            return "uncased";
        int targetN = m_wordN + offset;
        if (targetN < 0 || targetN >= m_doc.getWords().size()) {
            return "";
        }
        if (targetN - 1 >= 0
         && getWordCase(m_doc.getWord(targetN - 1)).equals("punc")) {
            return "start";
        }
        return getWordCase(m_doc.getWord(targetN));
    }

    /** 
     * Gets a description of the case of the specified word.
     * @param word The specified word. 
     * @return "allLower", "firstCap", "allCaps", "multiCase", "digit", "punc", "other"
     * In case of a single-character uppercase word, returns "firstCap"
     * Words beginning with a digit are "digit".  This implies that words
     * containing internal digits can still be "allLower" or "allUpper".
     * Words beginning with punctuation are "punc", and word-internal punc is
     * not considered "punc".
     * Zero length words are "other".
     * Words beginning with whitespace are "other".
     * multiCase means mixed case, but "mixed*" as a feature is disallowed.
     */
    public String getWordCase(String word) {
        if (word.length() == 0)
            return "other";

        if ( Character.isDigit(word.charAt(0)) )
            return "digit";

        if ( Character.isWhitespace(word.charAt(0)) )
            return "other";

        if ( !Character.isLetterOrDigit(word.charAt(0)) )
            return "punc";

        if ( word.equals(word.toLowerCase()) )
            return "allLower";

        /* A one-letter word is first-cap rather than uppercase */
        if ( word.equals(word.toUpperCase()) && word.length() > 1 )
            return "allCaps";

        if ( Character.isUpperCase(word.charAt(0)) ) {
            if (word.length() == 1)
        	return "firstCap";
            else if (word.substring(1).equals(word.substring(1).toLowerCase()))
        	return "firstCap";
        }

        return "multiCase"; //These words are mixed case.
    }


}