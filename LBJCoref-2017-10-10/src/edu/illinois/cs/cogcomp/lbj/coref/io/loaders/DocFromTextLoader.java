package edu.illinois.cs.cogcomp.lbj.coref.io.loaders;

import LBJ2.classify.Classifier;
import edu.illinois.cs.cogcomp.lbj.coref.decoders.MentionDecoder;
import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;
import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.DocPlainText;


/**
  * Loads a document from a plain text string, rather than from a file.
  * <p>
  * To load one or more documents from plain text,
  * construct this and then call {@link #loadDoc(String)}
  * with the plain text as input.
  * </p>
  * @author Eric Bengtson
 */
public class DocFromTextLoader extends DocLoader {
  protected boolean m_doWordSplit = true;

  /**
    * Constructs a loader that will detect and type mentions automatically.
    * Words will be split by an automatic word splitting algorithm.
    * Sentence boundaries, quotations, and part-of-speech tags will
    * also automatically be discovered.
    * <p>
    * To load a document from plain text,
    * construct this and then call {@link #loadDoc(String)}
    * with the plain text as input.
    * </p>
    * @param mentionDetector The mention detector extracts mentions from
    * a document, by predicting the head and extent boundaries of all mentions.
    * @param mTyper Determines the mention types of each mention.
    * Takes {@code Mention} objects as input and returns the type as a string,
    * "NAM", "NOM", "PRE", or "PRO".
   */
  public DocFromTextLoader(MentionDecoder mentionDetector,
                           Classifier mTyper) {
    this(mentionDetector, mTyper, /* doWordSplit = */ true); 
  }

  /**
    * Constructs a loader that will detect and type mentions automatically.
    * Sentence boundaries, quotations, and part-of-speech tags will
    * automatically be discovered.
    * <p>
    * To load a document from plain text,
    * construct this and then call {@link #loadDoc(String)}
    * with the plain text as input.
    * </p>
    * @param mentionDetector The mention detector extracts mentions from
    * a document, by predicting the head and extent boundaries of all mentions.
    * @param mTyper Determines the mention types of each mention.
    * Takes {@code Mention} objects as input and returns the type as a string,
    * "NAM", "NOM", "PRE", or "PRO".
    * @param doWordSplit If true, split words using an automatic word splitting
    * algorithm; otherwise, split words based on whitespace.
   */
  public DocFromTextLoader(MentionDecoder mentionDetector,
                           Classifier mTyper, boolean doWordSplit) {
    super(mentionDetector, mTyper); 
    m_doWordSplit = doWordSplit;
  }

  /**
    * Constructs and returns a document from the given plain text.
    * @param text Plain text.
    * @return A document representing the specified text.
   */
  @Override
    protected Doc createDoc(String text) {
      DocPlainText d = new DocPlainText();
      d.loadFromPlainText(text, m_doWordSplit);
      return d;
    }

}
