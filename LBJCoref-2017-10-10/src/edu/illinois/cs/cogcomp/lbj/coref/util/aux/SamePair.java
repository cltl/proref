package edu.illinois.cs.cogcomp.lbj.coref.util.aux;

/** A Pair that requires both members to be of the same type. */
public class SamePair<T> extends Pair<T,T> {
    private static final long serialVersionUID = 1L;

    public SamePair() {
    }

    public SamePair(T _a, T _b) {
	a = _a;
	b = _b;
    }
    
    public static <T> SamePair<T> createSP(T _a, T _b) {
	return new SamePair<T>(_a, _b);
    }
}
