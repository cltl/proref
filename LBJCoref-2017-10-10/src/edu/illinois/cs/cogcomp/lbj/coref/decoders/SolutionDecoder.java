package edu.illinois.cs.cogcomp.lbj.coref.decoders;

import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;
import edu.illinois.cs.cogcomp.lbj.coref.ir.solutions.Solution;

/**
 * Descendants take classifiers (or a set of input structures),
 * and produce a Solution appropriate to the evaluation tasks.
 * For example, if the input is a Doc
 * and the classifiers determine BIO boundaries for mentions,
 * then the natural output (Solution) is a Collection of Mentions.
 * @param ST The type of the solution
 * @author Eric Bengtson
 */
public interface SolutionDecoder<ST extends Solution> {
    
    /**
     * Decode a document into a solution.
     * @param doc A document to process.
     * @return A solution representing the result of the decoding.
     */
    public ST decode(Doc doc);
    
    /**
     * Set an boolean option.
     * @param option The option name.
     * @param value The value of the option.
     */
    public void setOption(String option, boolean value);
}
