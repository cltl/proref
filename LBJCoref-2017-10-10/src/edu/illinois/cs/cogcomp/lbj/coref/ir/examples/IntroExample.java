package edu.illinois.cs.cogcomp.lbj.coref.ir.examples;

import java.io.Serializable;

import edu.illinois.cs.cogcomp.lbj.coref.ir.Mention;



/** 
 * An example to hold an Mention and knowledge of whether
 * the example is the first mention of an entity in a document.
 * @author Eric Bengtson
 */
public class IntroExample implements Serializable {
    private static final long serialVersionUID = 1L;

    public Mention m_m;

    /** Meaningless unless set */
    protected boolean m_isFirst = false;
    protected boolean m_firstMeaningful = false;

    /** 
     * Constructs an IntroExample for use in determining whether a mention
     * is the the first mention of an entity.
     * @param m The mention
     */
    public IntroExample(Mention m) {
	m_m = m;
    }

    /** 
     * Constructs an IntroExample for use in training.
     * @param m The mention
     * @param isFirst The label, indicating whether a mention
     * is the first mention of its entity.
     */    
    public IntroExample(Mention m, boolean isFirst) {
	m_m = m;
	setIsFirst(isFirst);
    }

    /**
     * Gets the label, which indicates whether the example mention
     * is the first mention of its entity.
     * @return Whether the mention is the first mention of an entity.
     */
    public boolean isFirst() {
	if (!m_firstMeaningful)
	    throw new RuntimeException("Accessing meaningless isFirst.");
	return m_isFirst;
    }

    /**
     * Sets the label, which indicates whether the example mention
     * is the first mention of its entity.
     * @param isFirst The label, indicating whether the example mention
     * is the first mention of its entity.
     */
    public void setIsFirst(boolean isFirst) {
	m_isFirst = isFirst;
	m_firstMeaningful = true;
    }
}
