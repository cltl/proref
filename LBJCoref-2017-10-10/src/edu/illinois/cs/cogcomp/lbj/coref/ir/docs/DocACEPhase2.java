/** @author Eric Bengtson */

package edu.illinois.cs.cogcomp.lbj.coref.ir.docs;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.illinois.cs.cogcomp.lbj.coref.util.xml.XMLException;

import LBJ2.classify.Classifier;
import edu.illinois.cs.cogcomp.lbj.coref.ir.Chunk;
import edu.illinois.cs.cogcomp.lbj.coref.ir.Entity;
import edu.illinois.cs.cogcomp.lbj.coref.ir.Mention;



public class DocACEPhase2 extends DocXMLBase {
    private static final long serialVersionUID = 2L;

    /**
     * Loads filename file and reads in the xml representation.
     * @param filename The name of the xml file to load.
     */
    public DocACEPhase2(String filename)
     throws XMLException {
	super(filename, "sgm.tmx.rdc.xml");
    }

    public DocACEPhase2(String filename,
	       Classifier caser) throws XMLException {
	super(filename, "sgm.tmx.rdc.xml", caser);
    }
    
    /** Default constructor: Not recommended. */
    public DocACEPhase2() {
	super();
    }



    //Load functions specific to this version:

    /** Loads an entity from an xml representation and returns it.
	As a side effect, adds true mentions to the document.
    */
    protected Entity loadEntity(Node nEntity) throws XMLException {
	String eId, eType, eSubtype = null, eSpecificity;
	try {
	    /* Get attributes */
	    NamedNodeMap attrs = nEntity.getAttributes();
	    eId = getOptAttrib(attrs, "ID", "");
	} catch (NullPointerException e) {
	    throw new XMLException("Expected Entity Information not found", e);
	}

	eType = ""; eSpecificity = "";

	List<Mention> ments = new ArrayList<Mention>();
	List<List<Chunk>> nameLists = new ArrayList<List<Chunk>>();

	/* Get mentions and attributes */
	NodeList nlEntContents = nEntity.getChildNodes();
	for (int i = 0; i < nlEntContents.getLength(); i++) {
	    Node nEntContent = nlEntContents.item(i);
	    if (nEntContent.getNodeType() != Node.ELEMENT_NODE)
		continue;
	    Element eEntContent = (Element) nEntContent;
	    if (nEntContent.getNodeName().equals("entity_type")) {
		NamedNodeMap eTypeAttrs = nEntContent.getAttributes();
		eSpecificity = getOptAttrib(eTypeAttrs, "GENERIC", "");
		//NodeList nlETypeContents = nEntContent.getChildNodes();
		//TODO: Robustness for extra content:
		eType = nEntContent.getFirstChild().getNodeValue();
	    }
	    else if (nEntContent.getNodeName().equals("entity_mention")) {
		Mention m = processEntityMention(eEntContent, eId, eType,
		    eSubtype, eSpecificity);
		ments.add(m);
	    } else if (nEntContent.getNodeName() == "entity_attributes") {
		List<Chunk> entNames = processAttributes(eEntContent, "name");
		nameLists.add(entNames);
	    }
	}
	Entity e = new Entity(eId, eType, eSubtype, eSpecificity);
	for (Mention m : ments) {
	    e.addMention(m);
	    this.addTrueMention(m); //Side effect.
	}
	for (List<Chunk> names : nameLists) {
	    e.addNames(names);
	}
	return e;
    }

    /**
         * Load a chunk.
         *
         * @param element An element containing a charseq Element.
         * @return The desired Chunk.
         */
    protected Chunk processChunk(Element element) throws XMLException {
	try {
	    Node charseq = element.getElementsByTagName("charseq").item(0);
	    NodeList kids = charseq.getChildNodes();
	    String sStart = "", sEnd = "";
	    for (int i = 0; i < kids.getLength(); i++) {
		Node kid = kids.item(i);
		if (kid.getNodeType() != Node.ELEMENT_NODE)
		    continue;
		Element eKid = (Element) kid;
		if (eKid.getNodeName().equals("start")) {
		    //NodeList nl = kid.getChildNodes();
		    sStart = kid.getFirstChild().getNodeValue();
		} else if (eKid.getNodeName().equals("end")) {
		    sEnd = kid.getFirstChild().getNodeValue();
		}
	    }
	    int start = Integer.parseInt(sStart);
	    int end = Integer.parseInt(sEnd);
	    String text = getPlainText().substring(start, end+1);
	    return new Chunk(this, start, end, text);
	} catch (NullPointerException e) {
	    throw new XMLException("Chunk malformed.");
	}
    }



    protected String getBaseFilename(String filename) {
	if (filename.endsWith(".sgm.tmx.rdc.xml"))
	    return filename.substring(0, filename.length() - 16);
	else
	    return filename;
    }

    public void write(boolean usePredictions) {
	this.write("predictions/"+m_docID+".pred.sgm.tmx.rdc.xml", usePredictions);
    }

    //TODO: Merge with APF's version and pull up.
    //FIXME: Finish changes:
    public void write(String filenameBase, boolean usePredictions) {
	//open file
	PrintStream dout;
	try {
	    dout = new PrintStream(
	    new FileOutputStream(filenameBase + ".sgm.tmx.rdc.xml"));
	} catch (IOException e) {
	    System.err.println("Cannot open file for writing.");
	    e.printStackTrace();
	    return;
	}
	dout.println("<?xml version=\"1.0\"?>");
	dout.println("<!DOCTYPE source_file SYSTEM \"./ace-rdc.v2.0.1.dtd\">");
	dout.println("<source_file URI=\"" + m_docID + ".sgm\" "
	 + "SOURCE=\"" + m_source + "\" TYPE=\"" + m_docType + "\" "
	 + "VERSION=\"" + m_version + "\" "
	 + "AUTHOR=\"" + m_annotationAuthor + "\" "
	 + "ENCODING=\"" + m_encoding + "\">");

	dout.println("<document DOCID=\"" + m_docID + "\">");

	for (Entity e : getEntities()) {
	    //TODO: Subtypes???
	    dout.println(toXMLString(e) + "\n");
	}

	//TODO: Use predicted relations when appropriate.
	/* TODO: Include relations in output?
	for (Relation r : m_relations) {
	    dout.println(toXMLString(r));
	}
	*/

	dout.println("</document>");
	dout.println("</source_file>");
	//Close file
	dout.close();
    }

    protected String toXMLString(Entity e) {
	String result = "<entity ID=\"" + e.getID() + "\">\n";

	result += "  <entity_type GENERIC=\""
	 + e.getSpecificity() + "\">";
	result += e.getType();
	result += "</entity_type>\n";

	List<Mention> nams = new ArrayList<Mention>();
	for (Mention m : e.getMentions()) {
	    if (m.getType().equals("NAM")) {
		nams.add(m);
	    }
	    result += toXMLString(m, "  ") + "\n";
	}
	if (nams.size() > 0) {
	    result += "  <entity_attributes>\n";
	    for (Mention nam : nams) {
		result += "    <name>\n";
		result += "      " + toXMLString(nam.getHead()) + "\n";
		result += "    </name>\n";
	    }
	    result += "  </entity_attributes>\n";
	}
	//FIXME: Add entity attributes?
	result += "</entity>";
	return result;
    }

    protected String toXMLString(Chunk c) {
	return "<charseq>\n" +
	"<!-- string = \"" + toXMLString(c.getText()) + "\" -->\n"
	+ "  <start>" + c.getStart() + "</start>"
	 + "<end>" + c.getEnd() + "</end></charseq>";
    }

}
