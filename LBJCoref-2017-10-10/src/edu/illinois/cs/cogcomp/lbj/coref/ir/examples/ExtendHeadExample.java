package edu.illinois.cs.cogcomp.lbj.coref.ir.examples;

import edu.illinois.cs.cogcomp.lbj.coref.ir.Chunk;
import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;


/**
 * Represents an example of a word associated with some head,
 * used to determine whether the word is part of the extent
 * of that head's mention.
 * Holds a head and a label indicating whether the word is in the extent.
 * @author Eric Bengtson
 */
public class ExtendHeadExample extends WordExample {

    /** Label indicating whether the example word is in the head. */
    public boolean m_in;
    
    /** The head associated with this example. */
    protected Chunk m_head;

    //TODO: Add prev/next example.
    
    /**
     * Constructs an example representing a word in a document,
     * and a head, and a label indicating whether the word
     * is part of the extent corresponding to the given head.
     * @param doc The document containing the word.
     * @param head The head associated with the extent of some mention.
     * @param wordN The (zero-based) word number of the word in the document.
     * @param in The label, indicating whether the given word is part of
     * the extent associated with the given head.
     */
    public ExtendHeadExample(Doc doc, Chunk head, int wordN, boolean in) {
	this(doc, head, wordN);
	m_in = in;
    }
    
    /**
     * Constructs an example representing a word in a document,
     * and a head: The example may or may not be a word in the extent
     * associated with the given head.
     * @param doc The document containing the word.
     * @param head The head associated with the extent of some mention.
     * @param wordN The (zero-based) word number of the word in the document.
     */
    public ExtendHeadExample(Doc doc, Chunk head, int wordN) {
	super(doc, wordN);
	m_head = head;
    }

    /**
     * Gets the head associated with this example.
     * @return The head.
     */
    public Chunk getHead() {
	return m_head;
    }

    /**
     * Gets the label indicating whether this example's word
     * is part of the extent associated with this example's head.
     * Note that if the label has not been set, the result of this method
     * is undefined.
     * @return Whether the example word is a member of the extent
     * associated with this example's head, or undefined if not specified. 
     */
    public boolean isInExtent() {
	return m_in;
    }
}
