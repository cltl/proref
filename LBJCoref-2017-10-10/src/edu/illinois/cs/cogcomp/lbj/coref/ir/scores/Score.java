package edu.illinois.cs.cogcomp.lbj.coref.ir.scores;

/** Author: Eric Bengtson */

/** Descendants store counts  related to correctness and calculate and return various scoring metrics. */
abstract public class Score {
    abstract public double getScoreDouble();
    abstract public String getScoreString();
    public String toString() {
	return getScoreString();
    }
}
