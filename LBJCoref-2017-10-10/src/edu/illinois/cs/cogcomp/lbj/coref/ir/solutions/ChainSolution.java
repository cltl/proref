package edu.illinois.cs.cogcomp.lbj.coref.ir.solutions;

import java.io.Serializable;
import java.util.*;

import edu.illinois.cs.cogcomp.lbj.coref.util.aux.Pair;

import edu.illinois.cs.cogcomp.lbj.coref.ir.Mention;



/** 
 * Represents a partition(a set of non-overlapping subsets
 * called chains) and edges linking some items in the partition,
 * along with edge labels.
 * Provides functionality for linking chains and recording information
 * about the links.
 * In all cases defensive copying is done to inputs and unmodifiable views
 * are returned.  However, the underlying elements (of type T) are never copied.
 * NOTE WELL: Depends on T objects being hashable
 * (only an issue if two objects might be identical but independently created.)
 * @param <T> The type of element in the partition.
 * @author Eric Bengtson
 */
public class ChainSolution<T> implements Solution, Serializable {
    private static final long serialVersionUID = 6L;
    
    /* Members */
    private List<Set<T>> m_chains;
    private Map<T,Set<T>> m_mapToChains = null;
    private List<Pair<T,T>> m_edges;
    private Map<T,T> m_antecedents; //maps from second to antecedent.
    private Map<T,Set<T>> m_referrers; //maps to all things referring to key.
    private Map<Pair<T,T>,List<String>> m_edgeLabels;

    /* Construction */
    
    /**
     * Constructs an empty solution.
     */
    public ChainSolution() {
	setup();
    }

    /**
     * Copy constructor.
     * Creates a new solution having the same edges as {@code otherSol}.
     * @code otherSol The solution from which to copy.
     */
    public ChainSolution(ChainSolution<T> otherSol) {
	this();
	add(otherSol);
    }

    /** 
     * Constructs a ChainSolution from a collection of chains (collections).
     * Chains are effectively copied
     * (so subsequent changes to any chains in {@code chains}
     * will not affect this).
     * @param chains A collection of chains.
     */
    public ChainSolution(Collection<Collection<T>> chains) {
	this();
	for (Collection<T> chain : chains) {
	    this.addChain(chain);
	}
    }

    /**
     * Initialize the member collections to empty.
     */
    protected void setup() {
    	//Use a linked list to facilitate quick merging of chains.
	m_chains = new LinkedList<Set<T>>();
	m_mapToChains = new HashMap<T,Set<T>>();
	m_edges = new ArrayList<Pair<T,T>>();
	m_antecedents = new HashMap<T,T>();
	m_referrers = new HashMap<T,Set<T>>();
	m_edgeLabels = new HashMap<Pair<T,T>,List<String>>();
    }

    
    /* Add methods */ 
    
    /** 
     * Adds all the chains from {@code otherSol} to this,
     * including the edge labels.
     * Edges are copied defensively.
     * Any existing edges in {@code this} that are also in {@code otherSol}
     * will be overwritten in {@code this}.
     * @param otherSol Another solution.
     */
    public void add(ChainSolution<T> otherSol) {
	//NOTE: Changed; does it still work perfectly?
	for (Pair<T,T> p : otherSol.getEdges()) {
	    List<String> edgeLabels = otherSol.getEdgeLabelsFor(p);
	    //NOTE: Overwrites any existing labels for this object pair.
	    this.recordEquivalence(p.a, p.b, edgeLabels);
	}
	for (T o : otherSol.getAllMembers()) {
	    this.recordExistence(o);
	}
    }

    /** 
     * Adds a chain.
     * Simply records the equivalence of the elements in the chain.
     * Chain is effectively copied (so subsequent changes to chain
     * will not affect this).
     * @param chain The chain to add.
     */
    public void addChain(Collection<T> chain) {
	Iterator<T> it = chain.iterator();
	T o = null, o2 = null;
	while (it.hasNext()) {
	    o = o2;
	    o2 = it.next();
	    if (o == null) {
		recordExistence(o2);
	    } else {
		recordEquivalence(o, o2);
	    }
	}
    }

    
    
    /* Relationship/presence determiners */
    
    /**
     * Determines whether two elements are in the same chain (subset).
     * Both elements should be contained in {@code this} partition.
     * @param a An element.
     * @param b Another element.
     * @return Whether the elements are in the same chain (subset).
     */ 
    public boolean areTogether(T a, T b) {
	return (getChainOf(a).equals(getChainOf(b)));
    }

    /**
     * Determines whether the partition contains the specified item
     * in any of its subsets.
     * @param item An item.
     * @return Whether the partition contains the specified item
     */
    public boolean contains(T item) {
	return getAllMembers().contains(item);
    }

    
    
    /* Getters */
    
    /**
     * Gets an unmodifiable view all elements of the partition.
     * @return An unmodifiable view of the set of all elements of the partition.
     */
    public Set<T> getAllMembers() {
	return Collections.unmodifiableSet(m_mapToChains.keySet());
    }
    
    /**
     * Gets an unmodifiable view of the chains of this partition.
     * @return An unmodifiable list of chains, each an unmodifiable set. 
     */
    public List<Set<T>> getChains() {
	List<Set<T>> copy = new ArrayList<Set<T>>();
	for (Set<T> set : m_chains) {
	    copy.add(Collections.unmodifiableSet(set));
	}
	return Collections.unmodifiableList(copy);
    }

    /**
     * Gets an unmodifiable view of the subsets of this partition.
     * @return An unmodifiable list of the subsets, each an unmodifiable set. 
     */
    public List<Set<T>> getSubsets() {
	return getChains();
    }
    
    /**
     * Gets the chain containing the specified item, or null if the item
     * is not present.
     * Gets an unmodifiable view of the desired chain. 
     * @return The subset (chain) containing the specified item or null if the
     * item is not present in the partition.  
     */
    public Set<T> getChainOf(T item) {
	if (!m_mapToChains.containsKey(item)) {
	    return null;
	}
	return Collections.unmodifiableSet(m_mapToChains.get(item));
    }

    /**
     * Gets the chain containing the specified item, or null if the item
     * is not present.
     * Gets an unmodifiable view of the desired chain. 
     * @return The subset (chain) containing the specified item or null if the
     * item is not present in the partition.  
     */
    public Set<T> getContainerFor(T item) {
	return getChainOf(item);
    }

    /**
     * Gets the chain that is a superset of the specified set.
     * @return An unmodifiable view of the chain
     * containing or equal to {@code subset},
     * or null if no chain is found.
     */
    public Set<T> getChainContaining(Set<T> subset) {
	for (Set<T> c : getChains()) {
	    if (c.containsAll(subset))
		return Collections.unmodifiableSet(c);
	}
	return null;
    }
    


    /**
     * Gets the edges.
     * @return An unmodifiable view of the list of edges, as {@code Pair}s.
     */
    public List<Pair<T,T>> getEdges() {
	return Collections.unmodifiableList(m_edges);
    }

    /** 
     * Gets the edges for a pair of objects.
     * @return An unmodifiable view of the (possibly empty) list of labels.
     */
    public List<String> getEdgeLabelsFor(Pair<T,T> p) {
	List<String> labels = m_edgeLabels.get(p);
	if (labels == null)
	    return Collections.emptyList();
	else
	    return Collections.unmodifiableList(labels);
    }


    /* Linkers */

    //NOTE: Whenever chains are updated, call this or set m_mapToChains = null
    protected void updateMapToChains(T obj, Set<T> chain) {
	m_mapToChains.put(obj, chain);
    }   
    
    /** 
     * Records the equivalence of two objects,
     * adding them to the same subset of the partition if not yet present,
     * or linking the subsets containing them if present.
     * Also, a label is added to the edge between the objects.
     * @param o1 The first object.
     * @param o2 The second object.
     * @param label The label to append to the edge between {@code o1}
     * and {@code o2}.
     */
    public void recordEquivalence(T o1, T o2, String label) {
	List<String> labels = m_edgeLabels.get(Pair.create(o1, o2));
	if (labels == null)
	    labels = new ArrayList<String>();
	labels.add(label);
	m_edgeLabels.put(Pair.create(o1,o2), labels);
	recordEquivalence(o1, o2);
    }

    /** 
     * Records the equivalence of two objects,
     * adding them to the same subset of the partition if not yet present,
     * or linking the subsets containing them if present.
     * labels are applied to the edge between the objects,
     * replacing any existing labels.
     * Labels are copied defensively.
     * @param o1 The first object.
     * @param o2 The second object.
     * @param labels The labels to apply to the edge between {@code o1}
     * and {@code o2}. Any existing labels are replaced.
     */
    public void recordEquivalence(T o1, T o2, List<String> labels) {
	m_edgeLabels.put(Pair.create(o1,o2), new ArrayList<String>(labels));
	recordEquivalence(o1, o2);
    }

    /** Records equivalence by recording the equivalence of
     * all pairs of items in the chains. This is needed in case elements
     * in the input chains are in distinct chains in this ChainSolution.
     * Note that this means the implementation is quadratic in the number of
     * members.
     * Effectively copies all links; subsequent modifications of {@code chainA}
     * or {@code chainB} will not affect this partition.
     * @param chainA One set of objects.
     * @param chainB Another set of objects.
     */
    public void recordEquivalence(Set<T> chainA, Set<T> chainB) {
	for (T a : chainA) {
	    for (T b : chainB) {
		this.recordEquivalence(a, b);
	    }
	}
    }

    /**
     * Links the chains.
     * An arbitrary pair of elements, one from each chain, is linked.
     * Both chains must already exist in full in the partition.
     * Subsequent modification of {@code chainA} or {@code chainB}
     * will not affect this partition.
     * @param chainA A chain.
     * @param chainB Another chain.
     */
    public void linkChains(Set<T> chainA, Set<T> chainB) {
	linkChains(chainA, chainB, "");
    }

    /**
     * Links the chains, applying label.
     * An arbitrary pair of elements, one from each chain, is linked.
     * Both chains must already exist in full in the partition.
     * Subsequent modification of {@code chainA} or {@code chainB}
     * will not affect this partition.
     * Label is copied defensively and overwrites any existing
     * labels between the linked mentions.
     * @param chainA A chain.
     * @param chainB Another chain.
     */
    public void linkChains(Set<T> chainA, Set<T> chainB, String label) {
	List<String> labels = new ArrayList<String>();
	labels.add(label);
	linkChains(chainA, chainB, labels);
    }

    /**
     * Links the chains, applying labels.
     * An arbitrary pair of elements from the chains are linked.
     * Both chains must already exist in full in the partition.
     * Subsequent modification of {@code chainA} or {@code chainB}
     * will not affect this partition. 
     * Labels are copied defensively and overwrite any existing
     * labels between the linked mentions.
     * @param chainA A chain.
     * @param chainB Another chain.
     * @param labels Labels.
     */
    public void linkChains(Set<T> chainA, Set<T> chainB, List<String> labels) {
	if (chainA.size() <= 0 || chainB.size() <= 0)
	    return; //Do nothing.

	//Find b in chainB without antecedent, and link it to a.
	//TODO: Find a in chainA that has no one pointing to it, or latest one:
	T b = null;
	for (T m : chainB) {
	    if (!m_antecedents.containsKey(m)) {
		b = m;
	    }
	}
	if (b == null)
	    b = chainB.iterator().next();
	link(chainA, b, labels);
    }
    

    /**
     * Links an object to a chain, applying a label.
     * Makes a link between one element of {@code chainA} and {@code b}.
     * {@code chainA} must already exist in full in the partition.
     * Subsequent modifications of {@code chainA}
     * will not affect this partition.
     * Label is copied defensively and overwrites any existing
     * labels between the linked mentions.
     * @param chainA A chain.
     * @param b An object.
     * @param label A label.
     */
    public void link(Set<T> chainA, T b, String label) {
	List<String> labels = new ArrayList<String>();
	labels.add(label);
	link(chainA, b, labels);
    }

    /**
     * Links an object to a chain, applying labels.
     * Makes a link between one element of {@code chainA} and {@code b}.
     * {@code chainA} must already exist in full in the partition.
     * Subsequent modifications of {@code chainA}
     * will not affect this partition.
     * Labels are copied defensively and overwrite any existing
     * labels between the linked mentions.
     * @param chainA A chain.
     * @param b An object.
     * @param labels The labels.
     */
    public void link(Set<T> chainA, T b, List<String> labels) {
	T a = chainA.iterator().next();
	int max = 0;
	for (T m : chainA) {
	    if (!m_referrers.containsKey(m)) {
		if (m instanceof Mention) {
		    int n = ((Mention) m).getExtent().getEnd();
		    if (n >= max) {
			a = m;
			max = n;
		    }
		} else {
		    a = m;
		}
	    }
	}
	recordEquivalence(a, b, labels);
    }
    

    /**
     * Utility function for merging chains.
     * @param c1 One chain.
     * @param c2 Another chain.
     * @param n1 The first chain's index in m_chains.
     * @param n2 The second chain's index in m_chains.
     */
    private void mergeChains(Set<T> c1, Set<T> c2, int n1, int n2) {
	//OPTIMIZE: Could be perhaps optimized a bit?
	c1.addAll(c2);
	//Move all c2 into c1:
	for (T obj : c2) {
	    this.updateMapToChains(obj, c1);
	}
	m_chains.remove(n2);
    }
    
    
    /**
     * Records the equivalence of two objects,
     * adding them to the same subset of the partition if not yet present,
     * or linking the subsets containing them if present.
     * @param o1 An object.
     * @param o2 Another object.
     */
    public void recordEquivalence(T o1, T o2) {
	/* precondition: each obj is contained in at most one chain.
	 * postcondition: each obj is contained in exactly one chain.
	 */
	m_edges.add(Pair.create(o1,o2));
	m_antecedents.put(o2, o1);
	Set<T> referrers = m_referrers.get(o1);
	if (referrers == null) {
	    referrers = new HashSet<T>();
	    m_referrers.put(o1, referrers);
	}
	referrers.add(o2);

	Set<T> chain1 = null;
	Set<T> chain2 = null;
	int n1 = -1;
	int n2 = -1;
	//TODO: Take advantage of existing map (will slow down merge)
	//Perhaps a List or Set of Chains is unnecessary and a Map is enough.
	for (int i = 0; i < m_chains.size(); ++i) {
	    Set<T> chain = m_chains.get(i);
	    if (chain.contains(o1)) {
		chain1 = chain;
		n1 = i;
	    }
	    if (chain.contains(o2)) {
		chain2 = chain;
		n2 = i;
	    }
	}

	if (chain1 != null && chain2 != null) { //Both found
	    if (n1 != n2)
		this.mergeChains(chain1, chain2, n1, n2);
	}
	else if (chain1 == null && chain2 == null) { //found  neither
	    Set<T> newChain = new HashSet<T>();
	    newChain.add(o1);
	    newChain.add(o2);
	    m_chains.add(newChain);
	    this.updateMapToChains(o1,newChain);
	    this.updateMapToChains(o2,newChain);
	}
	else if (chain2 == null) { // (only) First found
	    chain1.add(o2);
	    this.updateMapToChains(o2,chain1);
	}
	else if (chain1 == null) { // (only) Second found
	    chain2.add(o1);
	    this.updateMapToChains(o1, chain2);
	}
    }



    /**
     * Records the existence of two objects.
     * If either is not yet part of a subset, it will be added to
     * its own subset.
     * Otherwise, nothing will be done.
     * Note that this method does not guarantee that the objects
     * will be in separate subsets after it is called.
     * @param o1 An object.
     * @param o2 Another object. 
     */
    public void recordDistinctness(T o1, T o2) {
	//Note: If this impl ever changes, be sure to keep mapToChains updated.
	this.recordExistence(o1);
	this.recordExistence(o2);
    }


    /** 
     * Ensures that {@code c} is contained in some chain;
     * if it is not yet, it is added to its own new chain.
     */
    public void recordExistence(T c) {
	Set<T> chain = this.getChainOf(c);
	if (chain == null) {
	    chain = new HashSet<T>();
	    chain.add(c);
	    m_chains.add(chain);
	    this.updateMapToChains(c, chain);
	}
    }
   
    
    
    /* Output */

    /** 
     * Represents this partition as a string.
     */
    public String toString() {
	String s = "";
	String nl = System.getProperty("line.separator");
	for ( Set<T> chain : getChains()) {
	    s += "Chain:" + nl;
	    for (T obj : chain) {
		s += "  " + obj + nl;
	    }
	}
	return s;
    }

    /**
     * Gets a string showing the difference between this and another
     * ChainSolution.
     * Chains are aligned by a single arbitrary member from each solution.
     * @param other Another ChainSolution, typically a reference key.
     * @param fAbsentMark denotes things in {@code other},
     * but not found in {@code this}.
     * @param fPresentMark denotes things in this
     * but missing in {@code other}.
     * @return A string describing {@code this},
     * indicating where it differs from {@code other}.
     */
    public String toDiffString(ChainSolution<T> other, String fAbsentMark,
			       String fPresentMark) {
	String s = "";
	String nl = System.getProperty("line.separator");
	for ( Set<T> chain : this.getChains()) {
	    s += "Chain:" + nl;
	    if (chain.size() > 0) {
		T someObj = chain.iterator().next();
		Set<T> otherChain = other.getContainerFor(someObj);
		if (otherChain == null) {
		    otherChain = new HashSet<T>();
		    System.err.println(someObj
		     + " not found in other chain");
		}
		Set<T> fPresent = new HashSet<T>(chain);
		fPresent.removeAll(otherChain);
		Set<T> fAbsent = new HashSet<T>(otherChain);
		fAbsent.removeAll(chain);
		for (T obj : chain) {
		    if (fPresent.contains(obj) ) {
			s += "  " + fPresentMark + " ";
		    } else {
			int len = Math.max(fAbsentMark.length(),
					   fPresentMark.length());
			for (int i = 0; i < len + 3; ++i)
			    s += " ";
		    }
		    s += obj.toString() + nl;
		}
		for (T obj : fAbsent) {
		    if ( fAbsent.contains(obj) ) {
			s += "  " + fAbsentMark + " "
			+ obj.toString() + nl;
		    }
		}
	    }
	}
	return s;

    }

    
    
    /* Equals / Hashcode */
    
    /**
     * Two ChainSolutions are equal if their subsets are equal.
     * @param o Another object.
     * @return Whether the specified object is equal to this.
     */
    public boolean equals(Object o) {
	if (o == null)
	    return false;
	if (! (o instanceof ChainSolution) )
	    return false;

	@SuppressWarnings("unchecked")
	ChainSolution<T> oSol = (ChainSolution<T>) o;

	return (oSol.getChains().equals(this.getChains()));
    }
    

    /**
     * Generate hash code based on the collection of chains.
     * @return The hash code for this.
     */
    public int hashCode() {
	return this.getChains().hashCode();
    }
}
