package edu.illinois.cs.cogcomp.lbj.coref.util.collections;

import java.util.*;

//TODO: Extend Set?
public class Partition<T> {
    private Map<T,Set<T>> m_subsets;

    public Partition() {
	m_subsets = new HashMap<T,Set<T>>();
    }

    //TODO: Add semantically meaningful exception to throw?
    /** Creates a partition from Collections.
	@throws Exception if any element occurs in multiple subsets.
    */
    public Partition(Collection<Collection<T>> subsets) {
	m_subsets = new HashMap<T,Set<T>>();
	for (Collection<T> subset : subsets) {
	    Set<T> internalSubset = new HashSet<T>(subset);
	    if ( !Collections.disjoint(m_subsets.keySet(), internalSubset) ) {
		throw new RuntimeException(
		"Duplicate object found constructing partition"
		+ " from Collection of Collections."
		);
	    }
	    for (T obj : internalSubset) {
		m_subsets.put(obj, internalSubset);
	    }
	}
    }

    public Partition(Partition<T> p) {
	this();
	for (Set<T> subset: p.getSubsets()) {
	    this.addSubset(subset);
	}
    }

    /** Constructs a partition in which each item from singletons
	forms its own subset.
	Currently, duplicates will be dropped.
    */
    public static <T> Partition<T> createSingletonsPartition(
     Collection<T> singletons) {
    	Partition<T> p = new Partition<T>();
    	Set<T> items = new HashSet<T>(singletons); //prevent duplicates.
    	for (T item : items) {
    	    Set<T> singleton = new HashSet<T>();
    	    singleton.add(item);
    	    p.addSubset(singleton);
    	}
    	return p;
    }
    
    /** Adds <code>obj</code> to the partition as a new subset.
     * @return true if successful; false if <code>obj</code> already present.
     */
    public boolean add(T obj) {
	if (m_subsets.containsKey(obj))
	    return false;
	Set<T> newSet = new HashSet<T>();
	newSet.add(obj);
	m_subsets.put(obj, newSet);
	return true;
    }
       
    /** Add the object to subsetContaining.
	The obj must not be in the Partition.
	@return whether operation succeeded. */
    public boolean add(T obj, T toSubsetContaining) {
	if (m_subsets.keySet().contains(obj)
	 || !m_subsets.keySet().contains(toSubsetContaining))
	    return false;
	Set<T> targetSet = m_subsets.get(toSubsetContaining);
	targetSet.add(obj);
	m_subsets.put(obj, targetSet);
	return true;
    }

    /** Adds a subset to the Partition.
	The subset must not contain any elements already in the partition.
	subset will be backed internally.
	@return whether operation succeeded. */
    public boolean addSubset(Collection<T> subset) {
	Set<T> setCopy = new HashSet<T>(subset);
	if (!Collections.disjoint(setCopy, m_subsets.keySet()))
	    return false;
	
	for (T obj : setCopy) {
	    m_subsets.put(obj, setCopy);
	}
	return true;
    }

    /** Adds all subsets from <code>other</code> to <code>this</code>.
     *  In case adding any subset from <code>other</code> fails,
     *  returns false but still adds all addable subsets.
     * @param other Another Partition (disjoint from this one).
     * @return true if successful; false if some subset(s) could not be added.
     */
    public boolean addAll(Partition<T> other) {
	boolean result = true;
	for (Set<T> subset : other.getSubsets()) {
	    boolean bAdded = this.addSubset(subset);
	    if (!bAdded) result = false;
	}
	return result;
    }
    
    
    //TODO: Deal with the fact that this invalidates containers floating around.
    public void join(T partitionContainingA, T partitionContainingB) {
	Set<T> subsetA = m_subsets.get(partitionContainingA);
	if (subsetA == null) {
	    subsetA = new HashSet<T>();
	    subsetA.add(partitionContainingA);
	}
	Set<T> subsetB = m_subsets.get(partitionContainingB);
	if (subsetB == null) {
	    subsetB = new HashSet<T>();
	    subsetB.add(partitionContainingB);
	}
	Set<T> both = new HashSet<T>(subsetA);
	both.addAll(subsetB);
	for (T obj : both) {
	    m_subsets.put(obj, both);
	}
    }

    
    public boolean areTogether(T a, T b) {
	//NOTE: Assumes subsets are ==.
	return (m_subsets.get(a) == m_subsets.get(b));
    }

    public boolean contains(T obj) {
	return m_subsets.containsKey(obj);
    }
    
    /** Backed externally. */
    public Set<T> getAllMembers() {
	return new HashSet<T>(m_subsets.keySet());
    }

    public List<Set<T>> getSubsets() {
	//Avoid duplicates, but return a List.
	//TODO: Stop return List.
	return new ArrayList<Set<T>>(new HashSet<Set<T>>(m_subsets.values()));
    }
    
    /** @return an unmodifiable view of the Set containing obj,
     *  or null if <code>obj</code> not found. */
    public Set<T> getContainerFor(T obj) {
	if (!m_subsets.containsKey(obj))
	    return null;
	return Collections.unmodifiableSet(m_subsets.get(obj));
    }

    /** Returns a copy of this partition as a Collection of Collections.
	Subsets will be copies, but items will not be cloned.
    */
    public Collection<Collection<T>> toCollections() {
	Set<Collection<T>> subsets = new HashSet<Collection<T>>();
	for (Set<T> subset : m_subsets.values()) {
	    subsets.add(new HashSet<T>(subset));
	}
	return subsets;
    }

    public String toString() {
	String s = "[";
	boolean any = false;
	Set<Set<T>> subsets = new HashSet<Set<T>>(m_subsets.values());
	for (Set<T> subset : subsets) {
	    if (any) s += ", ";
	    s += subset;
	    any = true;
	}
	s += "]";
	return s;
    }
    
}
