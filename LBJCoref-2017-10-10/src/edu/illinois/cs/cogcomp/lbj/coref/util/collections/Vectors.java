package edu.illinois.cs.cogcomp.lbj.coref.util.collections;

import java.util.Collection;
import java.util.Vector;

public class Vectors {

    public static Vector<Number> average(Collection<Vector<Number>> items) {
	Vector<Number> total = sum(items);
	return divide(total, items.size());
    }
    
    /** @return a vector of numbers with dimension equal to the smallest
	dimension of any of the items, or null if items is empty.
	(returns null on empty input because the dimensionality of the vector
	would otherwise be undefined.)
    */
    public static Vector<Number> sum(Collection<Vector<Number>> items) {
	Vector<Number> total = null;
	for (Vector<Number> item : items) {
	    if (total == null)
		total = new Vector<Number>(item);
	    else {
		int d = Math.min(total.size(), item.size());
		for (int i = 0; i < d; ++i) {
		    double val = total.get(i).doubleValue();
		    val += item.get(i).doubleValue();
		    total.set(i, val);
		}
	    }
	}
	return total;
    }

    public static Vector<Number> divide(
				    Vector<Number> numerator, double divisor) {
	Vector<Number> result = new Vector<Number>(numerator.size());
	for (int i = 0; i < numerator.size(); ++i) {
	    double numeratori = numerator.get(i).doubleValue();
	    result.add( numeratori / divisor );
	}
	return result;
    }
}
