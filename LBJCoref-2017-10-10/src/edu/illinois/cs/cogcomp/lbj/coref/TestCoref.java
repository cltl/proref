package edu.illinois.cs.cogcomp.lbj.coref;

import java.util.List;
import java.util.ArrayList;

import edu.illinois.cs.cogcomp.lbj.coref.decoders.BestLinkDecoder;
import edu.illinois.cs.cogcomp.lbj.coref.decoders.CorefKeyDecoder;
import edu.illinois.cs.cogcomp.lbj.coref.io.loaders.DocAPFLoader;
import edu.illinois.cs.cogcomp.lbj.coref.io.loaders.DocLoader;
import edu.illinois.cs.cogcomp.lbj.coref.ir.Mention;
import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;
import edu.illinois.cs.cogcomp.lbj.coref.ir.scores.Score;
import edu.illinois.cs.cogcomp.lbj.coref.ir.solutions.ChainSolution;
import edu.illinois.cs.cogcomp.lbj.coref.learned.Emnlp8;
import edu.illinois.cs.cogcomp.lbj.coref.parsers.CoParser;
import edu.illinois.cs.cogcomp.lbj.coref.scorers.BCubedScorer;


/**
  * This program tests the {@link Emnlp8} classifier on ACE 2004 testing data.
  *
  * <h4>Usage</h4>
  * <blockquote><pre>
  *   java edu.illinois.cs.cogcomp.lbj.coref.TestCoref &lt;corpus&gt; \
  *                                          [&lt;preprocessed=false&gt;]
  * </pre></blockquote>
  *
  * <h4>Input</h4>
  * <code>&lt;corpus&gt;</code> is the name (only file name; don't include a
  * path) of a file containing a list of ACE 2004 data file names.  The
  * directory containing <code>&lt;corpus&gt;</code> as well as the directory
  * containing the files named in <code>&lt;corpus&gt;</code> must be on the
  * <code>CLASSPATH</code>.  <code>&lt;preprocessed&gt;</code> should most
  * likely be left false (the default), but can be set true if preprocessing
  * has already been performed.
  *
  * <h4>Output</h4>
  * Three lines of output are sent to <code>STDOUT</code>.  They report the
  * <i>B<sup>3</sup></i> precision, recall, and <i>F<sub>1</sub></i> measures
  * of the classifier's predictions.
 **/
public class TestCoref
{
  public static void main(String[] args)
  {
    String corpusName = null;
    boolean useOfflinePreprocessing = false;

    try
    {
      corpusName = args[0];
      if (args.length > 1)
        useOfflinePreprocessing = "true".startsWith(args[1].toLowerCase());
      if (args.length > 2) throw new Exception();
    }
    catch (Exception e)
    {
      System.err.println(
  "usage: java edu.illinois.cs.cogcomp.lbj.coref.TestCoref <corpus> \\\n"
+ "                                                  [<preprocessed=false>]\n"
+ "       <corpus> is the name (only file name; don't include a path) of a\n"
+ "                file containing a list of ACE 2004 data file names.  The\n"
+ "                directory containing <corpus> must be on the CLASSPATH.\n"
+ "       <preprocessed> should most likely be left false (the default),\n"
+ "                      but can be set true if preprocessing has already\n"
+ "                      been performed.  See the README for more info.");
      System.exit(1);
    }

    DocLoader loader = new DocAPFLoader(corpusName, useOfflinePreprocessing);
    List<Doc> docs = loader.loadDocs();

    //Setting up the coreference algorithm:
    Emnlp8 corefClassifier = new Emnlp8();
    corefClassifier.setThreshold(-8.0);
    BestLinkDecoder decoder = new BestLinkDecoder(corefClassifier);

    //Applying coreference to all documents:
    List<ChainSolution<Mention>>
      keys = new ArrayList<ChainSolution<Mention>>(),
      preds = new ArrayList<ChainSolution<Mention>>();

    for (Doc d : docs) {
      keys.add(new CorefKeyDecoder().decode(d));
      ChainSolution<Mention> sol = decoder.decode(d);
      preds.add(sol); 
      d.setPredEntities(sol);
    }

    //Scoring:
    BCubedScorer scorer = new BCubedScorer();
    Score score = scorer.getScore(keys, preds);

    //Print the score:
    System.out.println(score);
  }
}

