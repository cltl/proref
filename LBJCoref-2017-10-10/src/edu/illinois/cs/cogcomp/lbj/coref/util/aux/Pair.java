package edu.illinois.cs.cogcomp.lbj.coref.util.aux;

import java.io.Serializable;

/** A Pair of objects delegating hashCode and equals to its members.
 *  Its members' types need not be the same.
 */
public class Pair<A,B> implements Serializable {
    private static final long serialVersionUID = 2L;
    public A a;
    public B b;

    public Pair() {
    }

    public Pair(A _a, B _b) {
	a = _a;
	b = _b;
    }

    public static <S,T> Pair<S,T> create(S _a, T _b) {
	return new Pair<S,T>(_a, _b);
    }

    public A getFirst() {
	return a;
    }
    public B getSecond() {
	return b;
    }
    
    public void setFirst(A first) {
	a = first;
    }
    
    public void setSecond(B second) {
	b = second;
    }
    
    public boolean equals(Object obj) {
	if (obj == this)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Pair<?,?> p = (Pair<?,?>) obj;
	return a.equals(p.a) && b.equals(p.b);
    }

    public int hashCode() {
	return 31 * ( a.hashCode() + 31 ) + b.hashCode();
    }

    public String toString() {
	return "[" + a + ", " + b + "]";
    }
}

