package edu.illinois.cs.cogcomp.lbj.coref.features;

import java.util.HashMap;
import java.util.Map;

import edu.illinois.cs.cogcomp.lbj.coref.ir.Mention;
import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;
import edu.illinois.cs.cogcomp.lbj.coref.ir.examples.CExample;



/**
  * A collection of features related to the entity type of a mention
  * or the relationship between the entity types of mentions.
  * An entity type is one of "PER" (person), "ORG" (organization),
  * "GPE" (Geo-Political Entity), "LOC" (location), "VEH" (vehicle),
  * "FAC" (facility), or "WEA" (weapon).
 */
public class EntityTypeFeatures {

  private static Map<String,String> m_wneTypeBetterCache = null;

  /** No need to construct collection of static features. */
  protected EntityTypeFeatures() {
  }

  /**
    * Determines whether the entity types of two mentions match,
    * as determined by {@link #getEType}.
    * No caching is used.
    * @param ex The example containing the mentions in question.
    * @return "true", "false", or "unknown".
   */
  public static String doETypesMatch(CExample ex) {
    boolean useCache = false;
    return doETypesMatch(ex, useCache);
  }

  /**
    * Determines whether the entity types of two mentions match,
    * as determined by {@link #getEType}.
    * @param ex The example containing the mentions in question.
    * @param useCache Whether caching should be done.
    * @return "true", "false", or "unknown".
   */
  public static String doETypesMatch(CExample ex, boolean useCache) {

    String t1 = getEType(ex.getM1(), useCache);
    String t2 = getEType(ex.getM2(), useCache);

    if (t1.equals("unknown") || t2.equals("unknown"))
      return "unknown";
    else if (t1.equals(t2))
      return "true";
    else //different and neither are unknown
      return "false";
  }

  /**
    * Determines the entity type of the mention {@code m}.
    * If the {@code m_predwnEType} caches the predicted entity type,
    * return that value; otherwise, depending on the mention type,
    * delegate to the appropriate method.  In the case of "PRE", determine
    * whether a proper noun based on case (if the document is case sensitive)
    * or else try considering it as both a "NAM" and "NOM" and if only
    * one returns non-"unknown", return that; finally, if both return a value,
    * choose the value determined assuming it is a "NAM".
    * Caching will not be done.
    * @param m The mention whose entity type should be determined.
    * @return The entity type: one of "PER" (person), "ORG" (organization),
    * "GPE" (Geo-Political Entity), "LOC" (location), "VEH" (vehicle),
    * "FAC" (facility), or "WEA" (weapon), or "unknown".
   */
  public static String getEType(Mention m) {
    boolean useCache = false;
    return getEType(m, useCache);
  }

  /**
    * Determines the entity type of the mention {@code m}.
    * If the {@link Mention#m_predwnEType} caches the predicted entity type,
    * return that value; otherwise, depending on the mention type,
    * delegate to the appropriate method.  In the case of "PRE", determine
    * whether a proper noun based on case (if the document is case sensitive)
    * or else try considering it as both a "NAM" and "NOM" and if only
    * one returns non-"unknown", return that; finally, if both return a value,
    * choose the value determined assuming it is a "NAM".
    * @param m The mention whose entity type should be determined.
    * @param useCache Whether caching should be done.
    * @return The entity type: one of "PER" (person), "ORG" (organization),
    * "GPE" (Geo-Political Entity), "LOC" (location), "VEH" (vehicle),
    * "FAC" (facility), or "WEA" (weapon), or "unknown".
   */
  public static String getEType(Mention m, boolean useCache) {
    String key = m.getHead().getText();

    if (useCache) {
      //If result depends on extent, change key.
      if (m_wneTypeBetterCache == null)
        m_wneTypeBetterCache = new HashMap<String,String>();
      else if (m_wneTypeBetterCache.containsKey(key))
        return m_wneTypeBetterCache.get(key);
    }

    String result = "unknown";

    if (!m.m_predwnEType.equals("unknown")) {
      result = m.m_predwnEType;
    } else if (m.getType().equals("NAM")) {
      result = getNameEType(m);
    } else if (m.getType().equals("PRE")) {
      if (m.getDoc().isCaseSensitive()) {
        char h1 = m.getHead().getText().charAt(0);
        if (Character.isUpperCase(h1))
          result = getNameEType(m);
        else
          result = getNominalEType(m);
      } else {
        String wnNAM = getNameEType(m);
        String wnNOM = getNominalEType(m);
        if ( wnNAM.equals("unknown") && !wnNOM.equals("unknown") ) {
          result = wnNOM;
        } else {
          result = wnNAM;
        }
      }
    } else if (m.getType().equals("NOM")) {
      result = getNominalEType(m);
    } else if (m.getType().equals("PRO")) {
      result = getPronounEType(m);
    } else {
      result = "unknown";
    }

    if (useCache) {
      m_wneTypeBetterCache.put(key, result);
    }

    return result;
  }

  /**
    * Determines the entity type of a mention, assuming that the mention
    * is a proper name (ACE mention type "NAM").
    * Checks Various Gazetteers, including lists of first names,
    * last names, honors, cities, states, countries, corporations,
    * sports teams, and universities.
    * In case of a conflict, "unknown" is returned.
    * @param m A mention assumed to be a "NAM".
    * @return The entity type: one of "PER" (person), "ORG" (organization),
    * "GPE" (Geo-Political Entity), "LOC" (location), "VEH" (vehicle),
    * "FAC" (facility), or "WEA" (weapon), or "unknown".
   */
  public static String getNameEType(Mention m) {
    String h = m.getHead().getText().toLowerCase();
    String[] words = h.split("\\s");
    String lastWord = words[words.length-1];
    String eType = "unknown";
    //TODO: More gazetteers:
    if (Gazetteers.getMaleFirstNames().contains(words[0])
        || Gazetteers.getFemaleFirstNames().contains(words[0])
        || Gazetteers.getHonors().contains(words[0]) //Assuming all personal honors.
       ) {
      eType = "PER";
       }
    if (Gazetteers.getLastNames().contains(lastWord)) {
      if (eType.equals("PER"))
        return "PER"; //Twice so confident.
      eType = "PER";
    }
    if ( Gazetteers.getCities().contains(h) || Gazetteers.getCountries().contains(h)
         || Gazetteers.getStates().contains(h)) {
      if (!eType.equals("unknown"))
        return "unknown";
      eType = "GPE";
         }
    if (Gazetteers.getOrgClosings().contains(lastWord) || Gazetteers.getPolParties().contains(h)
        || Gazetteers.getCorporations().contains(h) || Gazetteers.getSportTeams().contains(h)
        || Gazetteers.getUniversities().contains(h)) {
      if (!eType.equals("unknown"))
        return "unknown";
      eType = "ORG";
        } 
    return eType;
  }

  /**
    * Determines the Entity Type of a mention, assuming that the mention
    * is a common name phrase (ACE mention type "NOM").
    * Checks whether a hypernym of the phrase is one of
    * "person", "political unit", "location", "organization",
    * "weapon", "vehicle", "industrial plant", or "facility", 
    * and returns the appropriate entity type. 
    * @param m A mention assumed to be a "NOM".
    * @return The entity type: one of "PER" (person), "ORG" (organization),
    * "GPE" (Geo-Political Entity), "LOC" (location), "VEH" (vehicle),
    * "FAC" (facility), or "WEA" (weapon), or "unknown".
   */
  public static String getNominalEType(Mention m) {
    String h = m.getHead().getText().toLowerCase();
    //Order matters because e.g. political unit is-a group.
    if (WordNetTools.getWN().areHypernyms(h, "person"))
      return "PER";
    else if (WordNetTools.getWN().areHypernyms(h, "political unit"))
      return "GPE";
    else if (WordNetTools.getWN().areHypernyms(h, "location"))
      return "LOC"; //TODO?
    else if (WordNetTools.getWN().areHypernyms(h, "organization"))
      return "ORG";
    else if (WordNetTools.getWN().areHypernyms(h, "weapon"))
      return "WEA";
    else if (WordNetTools.getWN().areHypernyms(h, "vehicle"))
      return "VEH";
    else if (WordNetTools.getWN().areHypernyms(h, "industrial plant")) //TODO: Better?
      return "FAC";
    else if (WordNetTools.getWN().areHypernyms(h, "facility")) //TODO: Better?
      return "FAC";
    else
      return "unknown";
  }

  /** Determine the entity type of the mention, assuming it is a pronoun.
    * Does not attempt to distinguish amongst non-personal pronouns.
    * @param m A mention assumed to be a "PRO".
    * @return "PER" or "unknown".
   */
  public static String getPronounEType(Mention m) {
    String h = m.getHead().getText().toLowerCase();
    //TODO: This could be improved by returning "NON-x".
    if (h.equals("he") || h.equals("him") || h.equals("his")
        || h.equals("himself")	   
        || h.equals("she") || h.equals("her") || h.equals("hers")
        || h.equals("herself")
       ) {
      return "PER";
       }
    return "unknown";
  }

  /* ETypes and Tokens */

  /**
    * Gets the predicted entity type, or the pronoun word if a pronoun.
    * Uses the predicted entity type computed using {@link #getEType}.
    * Uses the default mention types, which may be gold.
    * Does not cache results.
    * @param ex The example.
    * @return For each mention, the predicted entity type,
    * or, if the mention is a pronoun, the token,
    * conjoined by {@literal "&&"}.
   */
  public static String predETypeOrProWord(CExample ex) {
    boolean useCache = false;
    return predETypeOrProWord(ex, useCache);
  }

  /**
    * Gets the predicted entity type, or the pronoun word if a pronoun.
    * Uses the predicted entity type computed using {@link #getEType}.
    * Uses the default mention types, which may be gold.
    * @param ex The example.
    * @param useCache Whether caching should be done.
    * @return For each mention, the predicted entity type,
    * or, if the mention is a pronoun, the token,
    * conjoined by {@literal "&&"}.
   */
  public static String predETypeOrProWord(CExample ex, boolean useCache) {
    Mention m1 = ex.getM1(), m2 = ex.getM2();
    Doc d = ex.getDoc();
    String e1 =
      m1.getType().equals("PRO") ? d.getWord(m1.getHeadLastWordNum())
                                 : getEType(ex.getM1(), useCache);
    String e2 =
      m2.getType().equals("PRO") ? d.getWord(m2.getHeadLastWordNum())
                                 : getEType(ex.getM2(), useCache);
    return e1 + "&&" + e2;
  }
}

