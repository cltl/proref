/**
  *
 */
package edu.illinois.cs.cogcomp.lbj.coref.ir;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import edu.illinois.cs.cogcomp.lbj.coref.ir.docs.Doc;


/**
  * Represents the ACE mention of some ACE entity in the context of a document.
  * A mention has a head (its head noun phrase) and an extent
  * (the largest noun phrase headed by the head),
  * along with some metadata.
  * Generally, {@code Mention}s should be used as singletons,
  * constructed only in the loading of a document, primarily stored in
  * and retrieved from a {@code Doc}.
  * Thus, classes should store and manipulate references to an
  * {@code Mention} rather than making copies.
  * @author Eric Bengtson
 */
public class Mention implements Serializable, Comparable<Mention> 
{
  private static final long serialVersionUID = 62L;
  private Doc m_doc;
  private String m_id;
  private String m_type; /* NAM, NOM, or PRO */
  private String m_ldcType;
  private String m_ldcAtr = null;
  private String m_role;
  private Chunk m_head;
  private Chunk m_extent;
  private String m_trueEntityID = null;
  private String m_predEntityID = null;
  private String m_entityType;
  private String m_subtype;
  private String m_specificity;
  public boolean m_isTrueMention;

  private transient Set<Mention> m_corefMents = null;

  public transient char m_predGender = 'u';
  public transient char m_predNumber = 'u';
  public transient String m_predwnEType = "unknown";

  //FIXME:
  /**
    * A complete constructor.
    * @param d The document containing this mention.
    * @param id The mention's ID.
    * @param type The mention's mention type (NAM, NOM, PRE, or PRO).
    * @param ldcType The LDC Type.
    * @param ldcAtr The LDC Attribute.
    * @param role The role.
    * @param extent The extent of the mention.
    * @param head The head of the mention.
    * @param entityID The entity ID of the entity containing this mention
    * (PER, GPE, ORG, LOC, VEH, WEA, or FAC).
    * @param subtype The entity sub-type.
    * @param specificity The specificity (SPC or GEN).
    * @param isTrueMention Whether the mention is true or predicted.
   */
  public Mention(Doc d,
                 String id, String type, String ldcType, String ldcAtr,
                 String role, Chunk extent, Chunk head, String entityID,
                 String entityType, String subtype, String specificity,
                 boolean isTrueMention) {
    m_extent = extent;
    m_doc = d;
    m_id = id;
    m_type = type;
    m_ldcType = ldcType;
    m_ldcAtr = ldcAtr;
    m_role = role;
    m_head = head;
    /* FIXME: Probably don't want to count punctuation as whitespace */
    m_trueEntityID = entityID;
    m_entityType = entityType;
    m_subtype = subtype;
    m_specificity = specificity;
    m_isTrueMention = isTrueMention;
    m_corefMents = new HashSet<Mention>();
    m_corefMents.add(this);
  }

  /**
    * A basic constructor that assumes the head and the extent are identical.
    * All other parameters are set to "NONE" or false.
    * @param d The document containing this mention.
    * @param headAndExtent The head and the extent (assumed to be identical).
   */
  public Mention(Doc d, Chunk headAndExtent) {
    this(d, "NONE", "NONE", "NONE", "NONE", "NON",
         headAndExtent, headAndExtent,
         "NONE", "NONE", "NONE", "NONE", false);
    if (headAndExtent == null) {
      throw new RuntimeException("null inappropriately as input.");
    }
  }

  /**
    * Gets the ACE head of this mention.
    * The head is the noun phrase that is the syntactic head of the mention.
    * @return The head of the mention.
   */
  public Chunk getHead() {
    return m_head;
  }

  /**
    * Gets the ACE extent of this mention.
    * The extent is the largest noun phrase headed by the head
    * of this mention.
    * @return The extent of the mention.
   */
  public Chunk getExtent() {
    return m_extent;
  }

  /**
    * Gets the document containing this mention.
    * @return the Doc containing this mention.
   */
  public Doc getDoc() {
    return m_doc;
  }


  /* Position */

  /**
    * Gets the position in the document of the first word in the head.
    * @return The word number in the document of the head's first word.
   */
  public int getHeadFirstWordNum() {
    return getHead().getStartWN();
  }

  /**
    * Gets the position in the document of the last word in the head.
    * @return The word number in the document of the head's last word.
   */
  public int getHeadLastWordNum() {
    return getHead().getEndWN();
  }


  /**
    * Gets the position in the document of the first word in the extent.
    * @return The word number in the document of the extent's first word.
   */
  public int getExtentFirstWordNum() {
    return getExtent().getStartWN();
  }

  /**
    * Gets the position in the document of the last word in the extent.
    * @return The word number in the document of the extent's last word.
   */
  public int getExtentLastWordNum() {
    return getExtent().getEndWN();
  }


  /**
    * Gets the sentence number of the mention,
    * which is defined as the sentence number of the first word in the head.
    * @return The sentence number.
   */
  public int getSentNum() {
    return m_doc.getSentNum(getHeadFirstWordNum());
  }



  /* Metadata */

  /* IDs */

  /** 
    * Gets the mention ID of this mention.
    * @return the mention ID, which is <b>not</b> the same as the entity ID.
   */
  public String getID() {
    return m_id;
  }

  /**
    * Sets the mention ID.
    * @param id The new ID.
   */
  public void setID(String id) {
    m_id = id;
  }

  //TODO: Ensure this rule is followed or improve it.
  /** 
    * Gets the entity ID.
    * @return the predicted entityID if available and not "NONE",
    * else return the true entityID if available and not "NONE",
    * else return "NONE".
   */
  public String getEntityID() {
    if (m_predEntityID != null && !m_predEntityID.equals("NONE"))
      return m_predEntityID;
    else if (m_trueEntityID != null && !m_trueEntityID.equals("NONE"))
      return m_trueEntityID;
    return "NONE";
  }

  /**
    * Gets the predicted entity ID, which may be NONE.
    * @return The predicted entity ID.
   */
  public String getPredictedEntityID() {
    return m_predEntityID;
  }

  /**
    * Sets the predicted entity ID.
    * @param id The new predicted entity ID.
   */
  public void setPredictedEntityID(String id) {
    m_predEntityID = id;
  }

  /**
    * Get the true entity ID. 
    * @return The true entity ID,
    * or "NONE" or null if no true entity ID has been set.
   */
  public String getTrueEntityID() {
    return m_trueEntityID;
  }

  /**
    * Sets the true entity ID.
    * @param id The true Entity ID.
   */
  public void setTrueEntityID(String id) {
    m_trueEntityID = id;
  }

  /**
    * Strips all identifying info that would serve as a label for coreference.
   */
  public void stripID() {
    m_trueEntityID = "NONE"; //TODO: Anything else?
  }


  /* Types */

  /**
    * Gets the mention type.
    * @return The Mention Type (NAM, NOM, PRE, PRO, or NONE).
   */
  public String getType() {
    return m_type;
  }

  /** 
    * Sets the mention type.
    * @param type The mention type (NAM, NOM, PRE, PRO, or NONE).
   */
  public void setType(String type) {
    m_type = type;
  }

  /**
    * Gets the entity type.
    * @return The Entity Type, PER, LOC, ORG, GPE, WEA, VEH, or FAC.
   */
  public String getEntityType() {
    return m_entityType;
  }

  /**
    * Sets the entity type.
    * @param type The entity type.
   */
  public void setEntityType(String type) {
    m_entityType = type;
  }

  //TODO: Where does this come from?
  /** 
    * Gets the entity subtype for this mention.
    * @return The entity subtype.
   */
  public String getSubtype() {
    return m_subtype;
  }

  public String getLdcType() {
    return m_ldcType;
  }


  /* Other metadata */

  /**
    * Gets the specificity.
    * @return The Specificity of the Mention,
    * either Generic ("GEN") or Specific ("SPC").
   */
  public String getSpecificity() {
    return m_specificity;
  }

  /**
    * Get the LDC Attribute.
    * @return The LDC Attribute.
   */
  public String getLdcAtr() {
    return m_ldcAtr;
  }

  /**
    * Get the role.
    * @return The role.
   */
  public String getRole() {
    return m_role;
  }



  /* Coreferential mentions */

  /** 
    * Gets the set of mentions known (or believed) to be coreferential
    * with this mention.
    * Includes this. 
    * Backed internally.
    * Note: Not all of this project records coreferential mentions
    * for access here.
   */
  public Set<Mention> getCorefMents() {
    if (m_corefMents == null) {
      m_corefMents = new HashSet<Mention>();
      m_corefMents.add(this);
    }
    return m_corefMents;
  }

  /**
    * Adds a mention to the set of mentions coreferential with this.
    * Note: Not all of this project records coreferential mentions
    * using this and related methods.
    * @param m The mention coreferential with this.
   */
  public void addCorefMent(Mention m) {
    if (m_corefMents == null) {
      m_corefMents = new HashSet<Mention>();
      m_corefMents.add(this);
    }
    m_corefMents.add(m);
  }

  /**
    * Adds all the mentions known (or believed)
    * to be coreferential with {@code m}
    * to the set of mentions coreferential with {@code this}.
    * After this method is called, {@code this.getCorefMents()}
    * will also contain all the mentions in {@code m.getCorefMents()}
    * (in addition to any other mentions already included).
    * Note: Not all of this project records coreferential mentions
    * using this and related methods.
    * @param m The target mention.
   */
  public void addCorefMentsOf(Mention m) {
    if (m_corefMents == null) {
      m_corefMents = new HashSet<Mention>();
      m_corefMents.add(this);
    }
    this.m_corefMents.addAll(m.getCorefMents());
  }



  /* Comparison */

  /** 
    * equals and hashCode methods inspired by article hosted on
    * Technofundo, called "Equals and Hash Code", by Manish Hatwalne
    * Available as of Feb. 27 2007 at URL:
    * http://www.geocities.com/technofundo/tech/java/equalhash.html
    * NOTE: comparison is by head and extent only
    * (but this is subject to change).
   */
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || (o.getClass() != this.getClass()) )
      return false;
    Mention m = (Mention) o; //Same class, so must be valid.
    if (   m.getHead().equals(this.getHead())
           && m.getExtent().equals(this.getExtent())
           //NOTE: Only heads and extents are compared!
           //(IDs are NOT compared)
       )
      return true;
    else
      return false;
  }

  /**
    * Gets the hash code, which takes into account the hash codes
    * of the head and extent,
    * but NOT the ID or any other metadata.
   */
  public int hashCode() {
    //NOTE: id is NOT taken into account. (since ID won't be correct
    // on predicted mentions.
    int value = 217; // 7 * 31;
    value += this.getHead().hashCode();
    value *= 31;
    value += this.getExtent().hashCode();
    //NOTE: m_text isn't taken into account.
    return value;
  }

  /** Compare first by extent, then by head. */
  public int compareTo(Mention m) {
    int extComp = this.getExtent().compareTo(m.getExtent());
    if ( extComp != 0)
      return extComp;
    else
      return this.getHead().compareTo(m.getHead());
  }    



  /* Output */

  /**
    * Gets the mention as a string, with newlines replaced by spaces.
   */
  public String toString() {
    return getText().replaceAll("\n", " ") + "(" + getID() + ")";
    //+ "( " + getExtent().getText().replaceAll("\n", " ") + " )";
    //+ gc.discreteValue(new GExample(this));
  }

  /**
    * Gets the mention data as a full string, showing entity IDs.
    * @return The mention data as a string.
   */
  public String toFullString() {
    return toFullString(true);
  }

  /**
    * Gets the mention data as a full string.
    * @param showEID Whether the Entity IDs should be included.
    * @return The mention data as a string.
   */
  public String toFullString(boolean showEID) {
    String s = "TEXT='" + getText()
      + "', mID=" + getID() + "'";

    if (getEntityID() != null && showEID) {
      s += ", eID=" + getEntityID();
    }
    s += ", eTYPE=" + m_entityType	
      + ", TYPE=" + m_type
      + ", ROLE=" + m_role
      + ", eTYPE=" + m_entityType;

    return s;
  }

  /**
    * Gets the mention as a string, with vertical bars marking the head
    * unless the extent and head are identical,
    * and with newlines replaced by spaces.
    * @return The mention text.
   */
  public String getText() {
    String h = getHead().getText().replaceAll("\n", " ");
    String e = getExtent().getText().replaceAll("\n", " ");
    if (e.equals(h))
      return e;
    int s = e.indexOf(h);
    if (s >= 0) {
      if (s == e.lastIndexOf(h)) {
        return e.substring(0,s) + "|" + h + "|"
          + e.substring(s + h.length());
      }
    }
    return e;
  }

  /** 
    * Gets the text up to but not including the head.
    * Includes any trailing spaces.
    * @return the text before the head of this mention.
   */
  public String getPreHeadText() {
    int eStartCN = getExtent().getStart();
    int hStartCN = getHead().getStart();
    int preToCN = hStartCN - eStartCN;
    return getExtent().getText().substring(0, preToCN);
  }

  /**
    * Gets the text up to and including the head.
    * @return The text up to and including the head.
   */
  public String getThruHeadText() {
    int eStartCN = getExtent().getStart();
    int hEndCN = getHead().getEnd();
    if (eStartCN > hEndCN)
      System.err.println("Warning: Head and Extents do not overlap");
    if (eStartCN < 0 || hEndCN < 0)
      System.err.println("Warning: Head or Extent has bad location.");
    return m_doc.getPlainText().substring(eStartCN, hEndCN + 1);
  }


  /* Serialization */

  /**
    * Serialization write method.
    * @param out The stream to write to.
    * @throws IOException
   */
  private void writeObject(ObjectOutputStream out) throws IOException {
    out.defaultWriteObject();
  }

  /**
    * Serialization read method.
    * @param in The stream to read from.
    * @throws IOException
    * @throws ClassNotFoundException
   */
  private void readObject(ObjectInputStream in) throws IOException,
          ClassNotFoundException {
            in.defaultReadObject();
            m_predGender = 'u';
            m_predNumber = 'u';
            m_predwnEType = "unknown";
  }

}
