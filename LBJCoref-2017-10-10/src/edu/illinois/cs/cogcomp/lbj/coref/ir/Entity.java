package edu.illinois.cs.cogcomp.lbj.coref.ir;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


/** 
 * An entity is a collection of mentions that are coreferential,
 * combined with some metadata about them.
 * @author Eric Bengtson */
public class Entity implements Serializable, Comparable<Entity> {
    private static final long serialVersionUID = 55L;
    private String m_id;
    private String m_type;
    private String m_subtype;
    private String m_specificity;
    private List<Mention> m_mentions;
    private List<Mention> m_sortedMentions; //Cache.
    private List<Chunk> m_names;

    /** Basic constructor. Not recommended. */
    public Entity() {
	    m_id = "";
	    m_type = "";
	    m_subtype = "";
	    m_specificity = "";
	    initListsDefault();
    }

    /**
     * Creates an empty entity (containing no mentions)
     * @param id The entity ID.
     * @param type The entity TYPE (PER, ORG, GPE, LOC, WEA, VEH, or FAC)
     * @param subtype The entity SUBTYPE.
     * @param specificity The specificity, specific (SPC) or generic (GEN)
     * This is the attribute {@code CLASS}
     * in an {@code .apf.xml} file.
     */
    public Entity(String id, String type, String subtype, String specificity) {
	    m_id = id;
	    m_type = type;
	    m_subtype = subtype;
	    m_specificity = specificity;
	    initListsDefault();
    }

    /**
     * Creates an entity with the specified mentions.
     * @param id The entity ID.
     * @param type The entity TYPE (PER, ORG, GPE, LOC, WEA, VEH, or FAC)
     * @param subtype The entity SUBTYPE.
     * @param specificity The specificity, specific (SPC) or generic (GEN)
     * This is the attribute {@code CLASS}
     * in an {@code .apf.xml} file.
     * @param mentions a list of {@code Mention}s.
     * @param names a List of {@code Chunk}s representing names.
     */
    public Entity(String id, String type, String subtype, String specificity, List<Mention> mentions, List<Chunk> names) {
	    m_id = id;
	    m_type = type;
	    m_subtype = subtype;
	    m_specificity = specificity;
	    m_mentions = mentions;
	    m_names = names;
    }

    private void initListsDefault() {
	    m_mentions = new LinkedList<Mention>();
	    m_names = new LinkedList<Chunk>();
    }

    /**
     * Adds a mention to the entity.
     * @param m A mention.
     */
    public void addMention(Mention m) {
	    m_mentions.add(m);
    }

    /** 
     * Gets a reference to the list of mentions.
     * @return A reference to the list of mentions.
     */
    public List<Mention> getMentions() {
	return m_mentions;
    }

    /** 
     * Gets a reference to the list of mentions sorted in their natural order
     * defined by their compare function.
     * This will be cached for performance.
    */
    public List<Mention> getSortedMentions() {
	if (m_sortedMentions == null) {
	    m_sortedMentions = new ArrayList<Mention>(m_mentions);
	    Collections.sort(m_sortedMentions);
	}
	return m_sortedMentions;
    }

    /**
     * Gets the specified mention.
     * @param i The position in the list of mentions.
     * @return The specified mention.
     */
    public Mention getMention(int i) {
	    return m_mentions.get(i);
    }

    /**
     * Gets the ID of the entity.
     * @return The entity ID.
     */
    public String getID() {
	return m_id;
    }

    /**
     * Gets the entity type (PER, ORG, GPE, LOC, WEA, VEH, or FAC)
     * @return The entity type.
     */
    public String getType() {
	return m_type;
    }

    /**
     * Gets the entity subtype.
     * @return the entity subtype.
     */
    public String getSubtype() {
	return m_subtype;
    }

    /**
     * Gets the specificity (SPC or GEN). 
     * @return The Specificity (e.g. the class) */
    public String getSpecificity() {
	return m_specificity;
    }

    /**
     * Add all the chunks as names to the existing set of names.
     * @param entNames The names to add.
     */
    public void addNames(List<Chunk> entNames) {
	    m_names.addAll(entNames);
    }

    /**
     * Generate a string representation of this.
     * @return This as a string.
     */
    public String toString() {
	    return "ID:"+m_id+", TYPE:"+m_type+", SUBTYPE:"+m_subtype+", CLASS:"
	    +m_specificity+", MENTIONS:"+m_mentions+", NAMES:"+m_names+"\n";
    }

    /**
     * Gets the short ID, which is the entity ID disregarding
     * the DocID and the "-E".
     * @return The shortened entity ID.
     */
    public String getShortID() {
	String longID = getID();
	int b = longID.lastIndexOf("-E");
	if (b == -1)
	    b = 0;
	else
	    b += 2;
	return longID.substring(b);
    }

    /**
     * Compare by short eID numerically, or lexicographically if same numbers.
     */
    public int compareTo(Entity e) {
	Integer aInt = new Integer( getShortID().replaceAll("[^0-9]", ""));
	Integer bInt = new Integer(e.getShortID().replaceAll("[^0-9]", ""));
	int compVal = aInt.compareTo(bInt);
	if (compVal != 0)
	    return compVal;
	else
	    return getShortID().compareTo(e.getShortID());
    }

}
