package edu.illinois.cs.cogcomp.lbj.coref.decoders;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

import edu.illinois.cs.cogcomp.lbj.coref.util.aux.CompPair;

import edu.illinois.cs.cogcomp.lbj.coref.util.collections.MyCollections;

import LBJ2.classify.Classifier;
import LBJ2.classify.Feature;
import LBJ2.classify.FeatureVector;
import LBJ2.learn.LinearThresholdUnit;
import LBJ2.learn.SparsePerceptron;
import edu.illinois.cs.cogcomp.lbj.coref.ir.examples.CExample;



/**
 * Base class for decoders which depend on a score from a learned classifier.
 * For compatibility with code that loads dynamically (such as the GUI)
 * subclasses should provide at least two constructors:
 * A constructor that takes just a {@code LinearThresholdUnit}
 * and a constructor that takes a {@code LinearThresholdUnit}
 * and a {@code Classifier} (in that order).
 * @author Eric Bengtson
 */
abstract public class ScoredCorefDecoder extends CorefDecoder {
    
    private static final long serialVersionUID = 1L;
    
    /** The scoring classifier. */
    protected LinearThresholdUnit m_scorer = null;
    
    /** Determines whether scores should be cached. */
    protected boolean m_cacheScores = false;
    
    /** The scores cache */
    private Map<CExample,Double> m_scoreCache;
    
    /**
     * Determines whether all uncached (or cache missed) calls
     * of the getTrueScore method record the score.
     * To produce statistics, this may be enabled.
     * But the performance may not be scalable for long-running servers.
     */
    protected boolean m_recordAllScores = false;
    
    /** The collection of all scores */
    private List<Double> m_allScores;

    
    
    /* Construction */
    
    /**
     * Constructs a decoder whose decisions will depend on the scores
     * of a linear threshold unit, which will also be used to make coreference
     * decisions via its {@code discreteValue} method.
     * @param c A coreference classifier which takes {@code CExample}s
     * and indicates coreference by returning "true" or "false", and also
     * produces a score.
     */
    public ScoredCorefDecoder(LinearThresholdUnit c) {
	super(c);
	m_scorer = c;
	setup();
    }
    
    /**
     * Constructor for use when the scoring classifier is not sufficient
     * to decide whether links should be made,
     * such as when inference is being applied.
     * Both {@code scorer} and {@code decider} must return "true" for
     * {@link #predictedCoreferential(CExample)} to return true.
     * @param scorer Determines the score or confidence.
     * Takes {@code CExample}s and returns a score.
     * @param decider Final arbiter of linking decisions.
     * Takes {@code CExample}s and returns "true" or "false". 
     */
    public ScoredCorefDecoder(LinearThresholdUnit scorer, Classifier decider) {
	super(decider);
	m_scorer = scorer;
	setup();
    }
    
    /**
     * Initializes members.
     */
    protected void setup() {
	m_scoreCache = new WeakHashMap<CExample,Double>();
	m_allScores = new ArrayList<Double>();
    }
    
    
    
    /* Coreference scores and values */
    
    /**
     * Gets the scoring classifier.
     * @return The scorer.
     */
    public LinearThresholdUnit getScorer() {
	return m_scorer;
    }

    /**
     * Sets the scoring classifier.
     * If the scorer and decider were identical, they will both be changed
     * to the given {@code scorer}.  Otherwise, only the scorer will be updated.
     * @param scorer The new scoring classifier.
     */
    public void setScorer(LinearThresholdUnit scorer) {
	if (m_scorer == super.getClassifier()) { //Also update classifier:
	    setClassifier(scorer);
	}
	m_scorer = scorer;
	m_scoreCache.clear();
    }

    /**
     * Determines whether an example is coreferential.
     * An example is coreferential if the scorer returns
     * "true" when applied to the example and
     * any non-scoring classifier also returns "true".
     * If there is only a scoring classifier, its decisions will be used.
     * @param ex The example to be classified as coreferential or not.
     * @return Whether the example is coreferential according to the scoring
     * classifier AND any specified non-scoring classifier. 
     */
    @Override
    protected boolean predictedCoreferential(CExample ex) {
	//FIXME double check.
	return ( super.predictedCoreferential(ex)
		 && m_scorer.discreteValue(ex).equals("true")
	);
	    
    }
    
    

    /* Scores */
    
    /**
     * Gets the score for the true prediction of the scoring classifier
     * applied to the example.
     * @param ex The example to score.
     * @return The scoring classifier's confidence that the example
     * is coreferential.
     */
    public double getTrueScore(CExample ex) {
	/* If scorer setter available, be sure to reset cache there. */
	if (m_cacheScores && m_scoreCache.containsKey(ex))
	    return m_scoreCache.get(ex);

	double score = getScorer().scores(ex).get("true");
	
	if (m_recordAllScores)
	    recordScore(score); 
	
	if (m_cacheScores)
	    m_scoreCache.put(ex,score);
	return score;
    }

        
    
    /* Statistics */

    /**
     * Get statistics encoded as a string.
     * Indicates the range of scores along with the rank of the threshold
     * among all scores generated.
     * Enable recording of scores for this method to return anything.
     * @return A statistics string.
     */
    @Override
    public String getStatsString() {
	String s = "";
	if (m_allScores.size() == 0)
	    return s;
	Collections.sort(m_allScores);
	s += "Low score was: " + m_allScores.get(0) + "\n";
	s += "High score was: " + m_allScores.get(m_allScores.size()-1) + "\n";

	int pos = Collections.binarySearch(m_allScores, 0.0); 
	//assumes scores are around 0 (adjusted for threshold ahead of time).
	
	s += "Rank with dup: " + pos + " / " + m_allScores.size()
	+ " = " + pos / (double) m_allScores.size() + "\n";

	List<Double> noDup = MyCollections.getWithoutDuplicates(m_allScores);
	pos = Collections.binarySearch(noDup, 0.0);
	s += "Rank without dup: " + pos + " / " + noDup.size()
	+ " = " + pos / (double) noDup.size() + "\n";
	return s;
    }
    
    /**
     * Record a score for statistics purposes.
     * @param score A confidence score.
     */
    private void recordScore(double score) {
	m_allScores.add(score);
    }
    
    
    
    /* Determine which features are active */
    
    /**
     * If the scoring classifier is a SparsePerceptron,
     * generate a list of labels, each indicating the name and weight
     * of an active feature.
     * @param ex The example to be processed by the scorer.
     * @return A list of strings, each giving the name and weight of an
     * active feature.
     */
    protected List<String> getEdgeLabels(CExample ex) {
	List<String> edgeLabels = new ArrayList<String>();
	//TODO: Accept other classifier types.
	if (!(getScorer() instanceof SparsePerceptron)) {
	    System.err.println("Not a SparsePerceptron; cannot make labels.");
	    //return edgeLabels;
	}
	Map<String,Double> fWeights = new HashMap<String,Double>();
	if (getScorer() instanceof SparsePerceptron) {
	    fWeights = getFeatureWeights((SparsePerceptron)getScorer());
	}

	Classifier featC = getScorer().getExtractor();
	FeatureVector fv = featC.classify(ex);
	List<CompPair<String,Double>> weights
	= new ArrayList<CompPair<String,Double>>();


  int N = fv.featuresSize();
  for (int i = 0; i < N; ++i) {
	    Feature f = fv.getFeature(i);
	    String fStr = f.toString();
	    if (fWeights.containsKey(fStr)) {
		double fWeight = fWeights.get(fStr);
		weights.add(CompPair.create(fStr, fWeight));
	    } else {
		weights.add(CompPair.create(fStr, Double.NaN));
	    }

	    if (!fStr.contains("_AND_")) {
		String fStrEx = fStr + "_ALL";
		if (fWeights.containsKey(fStrEx)) {
		    double fWeight = fWeights.get(fStrEx);
		    weights.add(CompPair.create(fStrEx, fWeight));
		}
	    }
	}
	Collections.sort(weights, new Comparator<CompPair<String,Double>> (){
	    public int compare(CompPair<String,Double> p1,
		    CompPair<String,Double> p2)
	    {
		return Double.compare(Math.abs(p2.b), Math.abs(p1.b));
	    }
	});
	for (CompPair<String,Double> pair : weights) {
	    String f = pair.a;
	    if (f.contains(":"))
		f = f.substring(f.indexOf(":") + 1);
	    edgeLabels.add(f + " = " + pair.b);
	}
	return edgeLabels;
    }

    /**
     * Gets all the known features of the given classifier and their weights.
     * @param c The classifier to examine.
     * @return A map from the known features of {@code c} to their
     * weights.
     */
    protected Map<String,Double> getFeatureWeights(SparsePerceptron c) {
	ByteArrayOutputStream sout = new ByteArrayOutputStream();
	PrintStream out = new PrintStream(sout);
	c.write(out);
	String s = sout.toString();
	String[] lines = s.split("\n");
	Map<String,Double> feats = new HashMap<String,Double>();
	for (int i = 1; i < lines.length; ++i) {
	    String line = lines[i];
	    String[] parts = line.split("   *");
	    if (1 < parts.length)
		feats.put(parts[0], Double.parseDouble(parts[1]));
	}
	//TODO: A non-hard-coded way to deal with conjunctions:
	//Map<String,Double> mins = getMinimumPartsOfCompounds(feats);
	//Map<String,Double> totals = getSumsOfParts(feats, mins);
	Map<String,Double> totals = getTotalsOfPartsOfCompounds(feats);
	for (Map.Entry<String,Double> pair : totals.entrySet()) {
	    feats.put(pair.getKey() + "_ALL", pair.getValue());
	}
	return feats;
    }

    /**
     * Gets the smaller feature weight for each conjunction of features
     * in the given feature-weight map.
     * @param map A map from features to weights.
     * Some of the features may be conjunctions of multiple features.
     * @return A map from each feature {@code f} that is part of a conjunction
     * to the smallest weight of the features of which {@code f} is part.  
     */
    protected Map<String,Double> getMinimumPartsOfCompounds(
	    Map<String,Double> map) {
	Map<String,Double> mins = new HashMap<String,Double>();
	String delimiter = "_AND_";
	int delimiterLength = delimiter.length();
	for (Map.Entry<String,Double> pair : map.entrySet()) {
	    String key = pair.getKey();
	    if ( !key.contains(delimiter) )
		continue;
	    double value = pair.getValue();
	    int andPos = key.indexOf(delimiter);
	    if (andPos < 0) continue;
	    int openParenPos = key.indexOf("(", andPos+5);
	    if (openParenPos < 0) continue;
	    int ampPos = key.indexOf("&", openParenPos + 1);
	    if (ampPos < 0) continue;
	    int closeParenPos = key.indexOf(")", ampPos + 1);
	    if (closeParenPos < 0) continue;
	    String fName1 = key.substring(0,andPos);
	    String fName2 = key.substring(andPos+delimiterLength, openParenPos);
	    String fVal1 = key.substring(openParenPos + 1, ampPos);
	    String fVal2 = key.substring(ampPos + 1, closeParenPos);
	    String feat1 = fName1 + "(" + fVal1 + ")";
	    String feat2 = fName2 + "(" + fVal2 + ")";

	    if ( !mins.containsKey(feat1) )
		mins.put(feat1, value);
	    else if ( Math.signum(value) != Math.signum(mins.get(feat1)) )
		mins.put(feat1, 0.0);
	    else if ( Math.abs(value) < Math.abs(mins.get(feat1)) )
		mins.put(feat1, value);

	    if ( !mins.containsKey(feat2) )
		mins.put(feat2, value);
	    else if ( Math.signum(value) != Math.signum(mins.get(feat2)) )
		mins.put(feat2, 0.0);
	    else if ( Math.abs(value) < Math.abs(mins.get(feat2)) )
		mins.put(feat2, value);
	}
	return mins;
    }

    /**
     * Gets, for each feature, the total of all the weights
     * of each conjunction that feature occurs in. 
     * @param map A map from features to weights.
     * @return A map from each feature {@code f} to the total of all the weights
     * of each conjunction that {@code f} occurs in. 
     */
    protected Map<String,Double> getTotalsOfPartsOfCompounds(
	    Map<String,Double> map) {
	Map<String,Double> totals = new HashMap<String,Double>();
	String delimiter = "_AND_";
	int delimiterLength = delimiter.length();
	for (Map.Entry<String,Double> pair : map.entrySet()) {
	    String key = pair.getKey();
	    if ( !key.contains(delimiter) )
		continue;

	    double value = pair.getValue();

	    int andPos = key.indexOf(delimiter);
	    int openParenPos = key.indexOf("(", andPos + delimiterLength);
	    if (openParenPos < 0)
		continue;
	    int ampPos = key.indexOf("&", openParenPos + 1);
	    if (ampPos < 0)
		continue;
	    int closeParenPos = key.indexOf(")", ampPos + 1);
	    if (closeParenPos < 0)
		continue;
	    String fName1 = key.substring(0,andPos);
	    String fName2 = key.substring(andPos+delimiterLength, openParenPos);
	    String fVal1 = key.substring(openParenPos + 1, ampPos);
	    String fVal2 = key.substring(ampPos + 1, closeParenPos);
	    String feat1 = fName1 + "(" + fVal1 + ")";
	    String feat2 = fName2 + "(" + fVal2 + ")";

	    if ( !totals.containsKey(feat1) )
		totals.put(feat1, value);
	    else
		totals.put(feat1, value + totals.get(feat1));

	    if ( !totals.containsKey(feat2) )
		totals.put(feat2, value);
	    else
		totals.put(feat2, value + totals.get(feat2));
	}
	return totals;
    }

    /**
     * Gets the total of all the weight each feature contributes.
     * @param feats A map from features to weights.
     * @param mins A map from features to the minimum weight.
     * @return A map from each feature {@code f} to the total weight {@code f}
     * contributes. 
     */
    protected Map<String,Double> getSumsOfParts(Map<String,Double> feats, Map<String,Double> mins) {
	Map<String,Double> result = new HashMap<String, Double>();
	String delimiter = "_AND_";
	int delimiterLength = delimiter.length();
	for (String key : feats.keySet()) {
	    //TODO: Don't repeat self:
	    if ( !key.contains(delimiter) )
		continue;

	    int andPos = key.indexOf(delimiter);
	    if (andPos < 0) continue;
	    int openParenPos = key.indexOf("(", andPos + delimiterLength);
	    if (openParenPos < 0) continue;
	    int ampPos = key.indexOf("&", openParenPos + 1);
	    if (ampPos < 0) continue;
	    int closeParenPos = key.indexOf(")", ampPos + 1);
	    if (closeParenPos < 0) continue;
	    String fName1 = key.substring(0,andPos);
	    String fName2 = key.substring(andPos+delimiterLength, openParenPos);
	    String fVal1 = key.substring(openParenPos + 1, ampPos);
	    String fVal2 = key.substring(ampPos + 1, closeParenPos);
	    String feat1 = fName1 + "(" + fVal1 + ")";
	    String feat2 = fName2 + "(" + fVal2 + ")";

	    double val1 = 0.0;
	    if (result.containsKey(feat1))
		val1 = result.get(feat1);
	    val1 += mins.get(feat1);
	    result.put(feat1, val1);

	    double val2 = 0.0;
	    if (result.containsKey(feat2))
		val2 = result.get(feat2);
	    val2 += mins.get(feat2);
	    result.put(feat2, val2);
	}
	return result;
    }

}
