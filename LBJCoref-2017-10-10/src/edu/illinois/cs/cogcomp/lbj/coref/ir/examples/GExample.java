package edu.illinois.cs.cogcomp.lbj.coref.ir.examples;

import edu.illinois.cs.cogcomp.lbj.coref.ir.Mention;


/**
 * An example representing a mention and its gender,
 * for use as the input to gender classifiers.
 * @author Eric Bengtson
 */
public class GExample extends Example {

    private String m_text = "";
    private String m_val = "u"; //Default to unknown

    /** 
     * Constructs a gender example representing the head of the mention;
     * subject to change.
     * @param m The mention.
     */
    public GExample(Mention m) {
	m_text = m.getHead().getText();
    }
    
    /** 
     * Constructs a gender example from a line matching some text with a gender.
     * The line should be in the format "phrase == gender".
     * Where gender is "m", "f", "u",
     * or possibly "p" for person (depending on the classifier). 
     * @param line A line of text.
     */
    public GExample(String line) {
	String[] parts = line.split(" == ");
	m_text = parts[0].trim();
	if (1 < parts.length)
	    m_val = parts[1].trim();
    }

    /**
     * Gets the text of the mention.
     * @return The text of the mention.
     */
    public String getText() {
	return m_text;
    }

    /**
     * Gets the gender label.
     * @return The gender label, one of "m", "f", "u",
     * or possibly "p" for person (depending on the classifier). 
     */
    public String getLabel() {
	return m_val;
    }
}
