package edu.illinois.cs.cogcomp.lbj.coref.ir.relations;

import java.io.Serializable;

import edu.illinois.cs.cogcomp.lbj.coref.ir.Chunk;


/**
 * Represents a relation mention argument.
 * @author Eric Bengtson
 */
public class RelationMentionArgument implements Serializable {

    private static final long serialVersionUID = 1253857698746608058L;
    public String m_id;
    public int m_argNum;
    public Chunk m_extent;

    /**
     * Constructs a relation mention argument.
     * @param id The ID.
     * @param argNum The argument number (1 or 2).
     * @param extent The extent of the relation mention argument.
     */
    public RelationMentionArgument(String id, int argNum, Chunk extent) {
	m_id = id;
	m_argNum = argNum;
	m_extent = extent;
    }

    /**
     * Gets the ID of the relation mention argument.
     * @return The ID of the relation mention argument.
     */
    public String getID() {
	return m_id;
    }

    /**
     * Gets the argument number.
     * @return The argument number (1 or 2).
     */
    public int getArgNum() {
	return m_argNum;
    }

    /**
     * Gets the extent of the relation mention argument.
     * @return The extent of the relation mention argument.
     */
    public Chunk getExtent() {
	return m_extent;
    }

    /**
     * Gets a string representation of this relation mention argument.
     * @return A string representation of this relation mention argument.
     */
    public String toString() {
	return "<"+m_id+", "+m_argNum+", "+m_extent+">";
    }

}
