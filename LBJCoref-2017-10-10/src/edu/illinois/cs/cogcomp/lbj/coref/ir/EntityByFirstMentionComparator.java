package edu.illinois.cs.cogcomp.lbj.coref.ir;

import java.util.Comparator;
import java.util.List;


/**
 * Comparator for entities that which orders entities
 * by the order of appearance of their mentions.
 */
public class EntityByFirstMentionComparator implements Comparator<Entity> {
    /** Sorts by smallest character number of entity's first
     * (according to Mention's compare function) mention's extent
     * (Start, then end).
     */
    public int compare(Entity o1, Entity o2) {
	List<Mention> ments1 = o1.getSortedMentions();
	List<Mention> ments2 = o2.getSortedMentions();
	if (ments1.size() == 0 && ments2.size() == 0)
	    return 0;
	else if (ments1.size() == 0)
	    return -1; //empty comes first.
	else if (ments2.size() == 0)
	    return 1;

	Mention m1 = ments1.get(0), m2 = ments2.get(0);
	int s1 = m1.getExtent().getStart(), s2 = m2.getExtent().getStart();
	//Do manual if to avoid issues if either is MAX_VALUE.
	if (s1 < s2) return -1;
	else if (s1 > s2) return 1;
	else { //s1 == s2 (for efficiency, do this last)
	    int e1 = m1.getExtent().getEnd(), e2 = m2.getExtent().getEnd();
	    if (e1 < e2) return -1;
	    else if (e1 > e2) return 1;
	    else { //and e1 == e2
		int hs1 = m1.getHead().getStart();
		int hs2 = m2.getHead().getStart();
		if (hs1 < hs2) return -1;
		else if (hs1 > hs2) return 1;
		else { //and hs1 == hs2
		    int he1 = m1.getHead().getEnd();
		    int he2 = m2.getHead().getEnd();
		    if (he1 < he2) return -1;
		    else if (he1 > he2) return 1;
		    else { //and he1 == he2
			return 0;
		    }
		}
	    }
	}
    }

}
