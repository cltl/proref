package edu.illinois.cs.cogcomp.lbj.coref.ir.solutions;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * A solution containing a set of objects of some type {@code T} 
 * @param T the type of object contained.
 * @author Eric Bengtson
 */
public class SetSolution<T> implements Solution, Serializable {
    private static final long serialVersionUID = 1L;
    private Set<T> m_items;

    /**
     * Constructs an empty {@code SetSolution}.
     */
    public SetSolution() {
	m_items = new HashSet<T>();
    }

    /**
     * Constructs a solution containing {@code items} as a set.
     * @param items The items (copied defensively into a set).
     */
    public SetSolution(Collection<T> items) {
	m_items = new HashSet<T>(items);
    }

    /**
     * Gets the items.
     * @return An unmodifiable view of the items.
     */
    public Set<T> getItems() {
	return Collections.unmodifiableSet(m_items);
    }

    /**
     * Get a string representation of the items.
     */
    public String toString() {
	String s = "";
	for (T c : getItems()) {
	    s += c.toString() + "\n";
	}
	return s;
    }
}
