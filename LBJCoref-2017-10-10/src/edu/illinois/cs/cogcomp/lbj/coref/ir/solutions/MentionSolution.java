package edu.illinois.cs.cogcomp.lbj.coref.ir.solutions;

import java.util.Collection;
import java.util.List;

import edu.illinois.cs.cogcomp.lbj.coref.ir.Mention;


/**
 * A solution that stores a list of mentions.
 * @author Eric Bengtson
 */
public class MentionSolution extends ListSolution<Mention> {

    private static final long serialVersionUID = 1L;

    /**
     * Constructs an empty {@code MentionSolution}.
     */
    public MentionSolution() {
	super();
    }

    /**
     * Constructor that takes a collection of mentions
     * and creates a defensive copy.
     * @param mentions A collection of mentions.
     */
    public MentionSolution(Collection<Mention> mentions) {
	super(mentions);
    }

    /**
     * Gets the items (as an unmodifiable view).
     * @return An unmodifiable view of the items.
     */
    public List<Mention> getMentions() {
	return getItems();
    }
}
