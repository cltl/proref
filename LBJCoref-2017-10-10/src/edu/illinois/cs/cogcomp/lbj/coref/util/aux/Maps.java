/*
 * Created on Jan 7, 2006
 *
 */
package edu.illinois.cs.cogcomp.lbj.coref.util.aux;

import java.util.Map;

public class Maps {

    public Maps() {
	super();
	// TODO Auto-generated constructor stub
    }

    /** 
     * Adds 1 to the Integer mapped to the key, or adds
     *  (k, new Integer(1)) to the Map.
     */
    public static <T> void addOne(Map<T,Integer> m, T k) {
	if(m.containsKey(k)) {
	    int num = m.get(k);
	    m.put(k, num + 1);
	}
	else {
	    m.put(k, 1);
	}	
    }

    public static <T> void addAllAToB(Map<T,Integer> a, Map<T,Integer> b) {
	for (Map.Entry<T,Integer> e : a.entrySet()) {
	    T aKey = e.getKey();
	    Integer aVal = e.getValue();
	    Integer bVal = b.get(aKey);
	    if (bVal == null) bVal = aVal;
	    else bVal = bVal + aVal;
	    b.put(aKey, bVal);
	}
    }

    //CAREFUL: Intentional repetition for efficiency.  Keep consistent.
    public static <K> int get(Map<K,Integer> m, K key) {
	if (m.containsKey(key))
	    return m.get(key);
	return 0; //Does this work for all types?
    }
    public static <K,V> V get(Map<K,V> m, K key, V defaultVal) {
	if (m.containsKey(key))
	    return m.get(key);
	return defaultVal;
    }
}
