package edu.illinois.cs.cogcomp.lbj.coref.util.aux;

/** Although Pair implements Comparable, it is only comparable if A and B
implement Comparable -- if they do not, in the future, Exceptions may be thrown.
*/
public class CompPair<A extends Comparable<A>,B extends Comparable<B>>
 extends Pair<A,B> implements Comparable<CompPair<A,B>>{
    //In parent class: public A a; public B b;
    private static final long serialVersionUID = 1L;

    public CompPair() {
    }

    public CompPair(A _a, B _b) {
	a = _a;
	b = _b;
    }

    public static <S extends Comparable<S>,T extends Comparable<T>>
     CompPair<S,T> create(S s, T t) {
	return new CompPair<S,T>(s, t);
    }

    /** Comparison function for ascending order sorts,
    sorting first by a and then by b, using A's and B's compareTo functions. 
    Consistent with equals only when A and B's compareTo methods
    are consistent with their equals methods. */
    public int compareTo(CompPair<A,B> pair) {
	int cA = ((Comparable<A>)a).compareTo(pair.a);
	if (cA != 0)
	    return cA;
	return ((Comparable<B>)b).compareTo(pair.b);
    }
}

