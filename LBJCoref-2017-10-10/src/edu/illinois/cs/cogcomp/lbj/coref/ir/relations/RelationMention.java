package edu.illinois.cs.cogcomp.lbj.coref.ir.relations;

import java.io.Serializable;

import edu.illinois.cs.cogcomp.lbj.coref.ir.Chunk;


/**
 * Represents the mention of a relation in a document.
 * @author Eric Bengtson
 */
public class RelationMention implements Serializable {

    private static final long serialVersionUID = -2676675086315804402L;
    private String m_id;
    private String m_ldcLexicalCondition;
    private Chunk m_extent;
    private RelationMentionArgument m_mArg1;
    private RelationMentionArgument m_mArg2; /* null for none */
    /* Extent from parent */

    /**
     * Constructs an empty relation mention.
     */
    public RelationMention() {
	//Should not get called.
	throw new RuntimeException("Illegally calling default constructor.");
    }

    /**
     * Constructs a relation mention.
     * @param id The relation mention ID.
     * @param ldcLexicalCondition The LDC lexical condition.
     * @param extent The extent of the relation mention text.
     * @param a1 The first relation mention argument.
     * @param a2 The second relation mention argument,
     * which may be null if no second argument exists.
     */
    public RelationMention(String id, String ldcLexicalCondition,
			    Chunk extent, RelationMentionArgument a1,
			    RelationMentionArgument a2) {
	m_id = id;
	m_ldcLexicalCondition = ldcLexicalCondition;
	m_extent = extent;
	m_mArg1 = a1;
	m_mArg2 = a2;
    }


    /**
     * Constructs a string representation of this relation mention.
     * @return A string representation of the relation mention.
     */
    public String toString() {
	return "<LDCEXTENT:'"+m_extent+"', mID:"+m_id
	+", a1:"+m_mArg1+", a2:"+m_mArg2+">";
    }

    /**
     * Gets the relation mention ID.
     * @return The relation mention ID.
     */
    public String getID() {
	return m_id;
    }

    /**
     * Gets the LDC lexical condition.
     * @return The LDC lexical condition.
     */
    public String getLDCLexicalCondition() {
	return m_ldcLexicalCondition;
    }

    /**
     * Gets the LDC extent of the relation mention.
     * @return The LDC extent of the relation mention.
     */
    public Chunk getLDCExtent() {
	return m_extent;
    }

    /**
     * Gets the first relation mention argument.
     * @return The first relation mention argument.
     */
    public RelationMentionArgument getArg1() {
	return m_mArg1;
    }
    
    /**
     * Gets the second relation mention argument.
     * @return The second relation mention argument.
     */
    public RelationMentionArgument getArg2() {
	return m_mArg2;
    }
}
