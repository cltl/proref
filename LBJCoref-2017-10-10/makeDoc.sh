rm -rf doc && mkdir -p doc

packages="edu.illinois.cs.cogcomp.lbj.coref"\
" edu.illinois.cs.cogcomp.lbj.coref.decoders"\
" edu.illinois.cs.cogcomp.lbj.coref.parsers"\
" edu.illinois.cs.cogcomp.lbj.coref.util.stats"\
" edu.illinois.cs.cogcomp.lbj.coref.util.aux"\
" edu.illinois.cs.cogcomp.lbj.coref.util.xml"\
" edu.illinois.cs.cogcomp.lbj.coref.util.io"\
" edu.illinois.cs.cogcomp.lbj.coref.util.collections"\
" edu.illinois.cs.cogcomp.lbj.coref.alignment"\
" edu.illinois.cs.cogcomp.lbj.coref.filters"\
" edu.illinois.cs.cogcomp.lbj.coref.features"\
" edu.illinois.cs.cogcomp.lbj.coref.io"\
" edu.illinois.cs.cogcomp.lbj.coref.io.loaders"\
" edu.illinois.cs.cogcomp.lbj.coref.exampleExtractors"\
" edu.illinois.cs.cogcomp.lbj.coref.scorers"\
" edu.illinois.cs.cogcomp.lbj.coref.ir"\
" edu.illinois.cs.cogcomp.lbj.coref.ir.docs"\
" edu.illinois.cs.cogcomp.lbj.coref.ir.scores"\
" edu.illinois.cs.cogcomp.lbj.coref.ir.examples"\
" edu.illinois.cs.cogcomp.lbj.coref.ir.solutions"\
" edu.illinois.cs.cogcomp.lbj.coref.ir.relations"\
" edu.brandeis.cs.steele.wn"\
" edu.brandeis.cs.steele.util"

javadoc -quiet -classpath ${CLASSPATH}:class -sourcepath src -d doc -use -author -private ${packages}

