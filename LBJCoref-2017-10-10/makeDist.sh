
cd .. && \
tar zcf LBJCoref/LBJCoref.tar.gz \
  LBJCoref/README \
  LBJCoref/build.sh \
  LBJCoref/makeDoc.sh \
  LBJCoref/makeJar.sh \
  LBJCoref/makeDist.sh \
  LBJCoref/Manifest.txt \
  LBJCoref/doc \
  `find LBJCoref/gazetteers -type f | grep -v '\.svn'` \
  `find LBJCoref/wordnet -type f | grep -v '\.svn'` \
  `find LBJCoref/src -type f | grep -v '\.svn' | grep -v '\.swp' | grep -v '\.swo'` \
  LBJCoref/corefFeatures.lbj \
  LBJCoref/emnlp8.lbj \
  LBJCoref/emnlpBasicCoref.lbj \
  LBJCoref/introduction.lbj \
  LBJCoref/mdExtendHeads.lbj \
  LBJCoref/mdHeads.lbj \
  LBJCoref/mentionDetectionFeatures.lbj \
  LBJCoref/mTypePredictor.lbj \
  LBJCoref/HLT2007.ALL \
  LBJCoref/HLT2007.TRAIN \
  LBJCoref/HLT2007.DEV \
  LBJCoref/HLT2007.TRAINANDDEV \
  LBJCoref/HLT2007.TEST

