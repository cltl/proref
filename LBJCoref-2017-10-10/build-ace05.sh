CLASSPATH=.:$(echo lib/*.jar | tr " " :)
ACE2005_DIR="../data:../data/ACE-2005-english"
classDir=class
lbjDir=lbj

mkdir -p $lbjDir $classDir
classpath="-cp ${CLASSPATH}:$classDir:$ACE2005_DIR"
javacArguments="-sourcepath src -d $classDir"
lbjArguments="-gsp $lbjDir"
javaArguments="-Xmx3g ${classpath}"
runLBJ="nice time java ${javaArguments} LBJ2.Main ${javacArguments} ${lbjArguments}"

echo ${runLBJ} introductionAce05.lbj

echo "---> corefFeatures" && ${runLBJ} corefFeatures.lbj \
&& echo "---> introduction" && ${runLBJ} introductionAce05.lbj

#   echo "---> corefFeatures" && ${runLBJ} corefFeatures.lbj \
#&& echo "---> introduction" && ${runLBJ} introduction.lbj \
#&& echo "---> emnlpBasicCoref" && ${runLBJ} emnlpBasicCoref.lbj \
#&& echo "---> mentionDetectionFeatures" && ${runLBJ} mentionDetectionFeatures.lbj \
#&& echo "---> mdHeads" && ${runLBJ} mdHeads.lbj \
#&& echo "---> mdExtendHeads" && ${runLBJ} mdExtendHeads.lbj \
#&& echo "---> mTypePredictor" && ${runLBJ} mTypePredictor.lbj \
#&& echo "---> emnlp8" && ${runLBJ} emnlp8.lbj \
#&& javac ${classpath} ${javacArguments} `find . -name '*.java'`

