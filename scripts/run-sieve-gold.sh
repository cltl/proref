cd CoreNLP

JARS=target/stanford-corenlp-3.7.0.jar:stanford-corenlp-models-current.jar:stanford-english-corenlp-models-current.jar:`echo lib/*.jar | tr ' ' ':'`

java -Xmx6g -cp $JARS edu.stanford.nlp.dcoref.SieveCoreferenceSystem \
        -props sieve-english-conll.properties \
        -dcoref.all_gold True \
		-dcoref.conll2011 $1 \
        -dcoref.conll.scorer ../reference-coreference-scorers/v8.01/scorer.pl
