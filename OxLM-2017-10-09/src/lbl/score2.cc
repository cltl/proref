// Compute the score of the last word of each line.
// Work interactively: every line you enter will get an immediate response.

#include <boost/program_options.hpp>

#include "lbl/context_processor.h"
#include "lbl/model.h"
#include "lbl/utils.h"
#include <math.h>       /* exp */

using namespace boost::program_options;
using namespace oxlm;
using namespace std;

template<class Model>
void score(const string& model_file) {
  Model model;
  model.load(model_file);

  boost::shared_ptr<ModelData> config = model.getConfig();
  boost::shared_ptr<Vocabulary> vocab = model.getVocab();
  string line;
  while (getline(cin, line)) {
	  boost::shared_ptr<Corpus> test_corpus = Corpus::from_cin_one_line(vocab, line);
	  if (test_corpus->size() <= 0) {
		  cout << 0;
	  } else {
		  ContextProcessor processor(test_corpus, config->ngram_order - 1);
		  int i = test_corpus->size()-1;
		  int word_id = test_corpus->at(i);
		  vector<int> context = processor.extract(i);
		  double log_prob = model.getLogProb(word_id, context);
		  cout << exp(log_prob);
	  }
	  // repeats the input so that our user can check if it matches what they sent
	  cout << ' ' << line << endl << std::flush;
  }
}

int main(int argc, char** argv) {
  options_description desc("Command line options");
  desc.add_options()
      ("help,h", "Print help message.")
      ("model,m", value<string>()->required(), "File containing the model")
      ("type,t", value<int>()->required(), "Model type");

  variables_map vm;
  store(parse_command_line(argc, argv, desc), vm);

  if (vm.count("help")) {
    cout << desc << endl;
    return 0;
  }

  notify(vm);

  string model_file = vm["model"].as<string>();
  ModelType model_type = static_cast<ModelType>(vm["type"].as<int>());
  switch (model_type) {
    case NLM:
      score<LM>(model_file);
      return 0;
    case FACTORED_NLM:
      score<FactoredLM>(model_file);
      return 0;
    case FACTORED_MAXENT_NLM:
      score<FactoredMaxentLM>(model_file);
      return 0;
    case FACTORED_TREE_NLM:
      return 0;
    default:
      cout << "Unknown model type" << endl;
      return 1;
  }

  return 0;
}
