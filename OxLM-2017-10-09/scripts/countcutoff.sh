
#Usage: ./preprocess-corpus-count.sh  CORPUS MIN_FREQ  OUTPUT

tr ' ' '\n' < $1 | awk '{x[$1]++} END {for (w in x){ print x[w] " " w}}' > $3.counts
perl -ne 'm/^(\d+) (.*)/; if ($1 >= '$2') {print "$2\n"};' < $3.counts | sort > $3.vocab
