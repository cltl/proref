from cort.core.spans import Span
from collections import deque
import re

class Frame(object):
    
    def __init__(self, sent, predicate_id, predicate_token, predicate_offset):
        self.sent = sent
        self.predicate_id = predicate_id
        self.predicate_token = predicate_token
        self.predicate_offset = predicate_offset
        self._frame_elements = []
        self._role2fe = {}
        
    def append(self, fe):
        assert fe.role not in self._role2fe
        self._frame_elements.append(fe)
        self._role2fe[fe.role] = fe
        fe.frame = self
        
    def get(self, role_name):
        return self._role2fe.get(role_name)
        
    def roles(self):
        return self._role2fe.keys()
        
    def __getitem__(self, key):
        if isinstance(key, str):
            return self._role2fe[key]
        else:
            return self._frame_elements[key]
        
    def __len__(self):
        return len(self._frame_elements)
        
    def __iter__(self):
        return iter(self._frame_elements)
        
    def __str__(self, *args, **kwargs):
        return '%s(%s)' %(self.predicate_id, 
                          ','.join(str(fe) for fe in self.frame_elements))

class FrameElement(object):
    
    def __init__(self, role, tokens, span):
        self.role = role
        self.tokens = tokens
        self.span = span
        
    def __str__(self, *args, **kwargs):
        return '%s=%s' %(self.role, ' '.join(self.tokens))

def _extract_labeled_spans(entries, offset=0):
    labeled_spans = {}
    tags = deque()
    for i, entry in enumerate(entries):
        j = 0
        while j < len(entry):
            if entry[j] == '(':
                tag = re.match(r'[\w-]+', entry[j+1:]).group()
                tags.append((i, tag))
                j += len(tag)
            elif entry[j] == ')':
                start, tag = tags.pop()
                labeled_spans[tag] = Span(offset+start, offset+i)
            else:
                assert entry[j] == '*'
            j += 1
    return labeled_spans

def __extract_from_column(rows, column):
    return [row[column] for row in rows]

def parse_frames(corpus):
    for doc in corpus:
        doc.frames = []
        doc.frame_elements = [[] for _ in range(len(doc.tokens))]
        for sent_id, sent_span in enumerate(doc.sentence_spans):
            rows = doc.document_table[sent_span.begin:sent_span.end+1]
            num_cols = len(rows[0])
            for col in range(11, num_cols-1):
                entries = __extract_from_column(rows, col)
                spans = _extract_labeled_spans(entries, sent_span.begin)
                if 'V' in spans:
                    predicate_offset = spans['V'].begin
                    predicate_row = doc.document_table[predicate_offset]
                    predicate_id = '%s.%s' %(predicate_row[6], predicate_row[7])
                    frame = Frame(sent_id, predicate_id, predicate_row[3], predicate_offset)
                    del spans['V']
                    for role, span in spans.items():
                        fe_rows = doc.document_table[span.begin:span.end+1]
                        tokens = __extract_from_column(fe_rows, 3)
                        fe = FrameElement(role, tokens, span)
                        frame.append(fe)
                        for i in range(span.begin, span.end+1):
                            doc.frame_elements[i].append(fe)
                    doc.frames.append(frame)
                else:
                    print('Frame without a verb: ' + str(spans))
                    