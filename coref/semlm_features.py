'''
Implementation of SemLM features. Please train the models with the right revision
of the code (see model paths) before using this class.

Example:

init_scorer()
<use the features>
close_scorer()
'''

from subprocess import Popen, PIPE
from peng_roth.semlm import extract_ec_chain
import os
from data import parse_frames
import sys

fn_model_path = 'output/lb-ec-fn-chain.bin.c362e64291d4e49eb2522917a02a91424e52ae51'
pb_model_path = 'output/lb-ec-pb-chain.bin.c362e64291d4e49eb2522917a02a91424e52ae51'
_fn_scorer_process = None
_pb_scorer_process = None

def init_scorers(name='all'):
    global _fn_scorer_process, _pb_scorer_process
    if name in ['fn', 'all']:
        assert os.path.exists(fn_model_path), 'Please train the model with the right revision first'
        _fn_scorer_process = Popen(['OxLM-2017-10-09/bin/score2', '-t', '3', '-m', fn_model_path],
                                   stdin=PIPE, stdout=PIPE)
    if name in ['pb', 'all']:
        assert os.path.exists(pb_model_path), 'Please train the model with the right revision first'
        _pb_scorer_process = Popen(['OxLM-2017-10-09/bin/score2', '-t', '3', '-m', pb_model_path],
                                   stdin=PIPE, stdout=PIPE)

def close_scorers():
    _fn_scorer_process.stdin.write('\n')
    _fn_scorer_process.wait()
    _pb_scorer_process.stdin.write('\n')
    _pb_scorer_process.wait()

def _compute_prob(anaphor, antecedent, process, map_to_framenet=None):
    if not hasattr(anaphor.document, 'frame_elements'):
        parse_frames([anaphor.document])
    seq = extract_ec_chain(antecedent.chain + [anaphor], map_to_framenet=map_to_framenet)
    if not seq: return -1
    seq = ' '.join(seq)
    process.stdin.write(('%s\n' %seq).encode())
    process.stdin.flush()
#     print('Sent a sequence (%s), waiting for reply...' %seq) # for debugging
    line = process.stdout.readline().decode().strip()
    prob = float(line[:line.index(' ')])
    echoed_input = line[line.index(' ')+1:]
    assert echoed_input == seq
#     print("SEMLM: %s" %line) # for debugging
    return prob

def semlm_pb(anaphor, antecedent):
    """ Compute the SemLM score for the chain formed by sticking the anaphor
    to the chain that contains the antecedent.
    """
    if _pb_scorer_process == None: init_scorers('pb')
    return "semlm_pb", _compute_prob(anaphor, antecedent, _pb_scorer_process, 
                                     map_to_framenet=False)

def semlm_fn(anaphor, antecedent):
    """ Compute the SemLM score for the chain formed by sticking the anaphor
    to the chain that contains the antecedent.
    """
    if _fn_scorer_process == None: init_scorers('fn')
    return "semlm_fn", _compute_prob(anaphor, antecedent, _fn_scorer_process, 
                                     map_to_framenet=True)
