'''
Created on 29 Oct 2017

@author: cumeo
'''
import logging

from _collections import defaultdict
from cort.core.mentions import Mention
from cort.core.corpora import Corpus
import codecs

class MentionWithHistory(object):
    
    def __init__(self, mention, chain):
        assert isinstance(mention, Mention)
        self.mention = mention
        self.chain = chain
        
    def __getattr__(self,attr):
        if attr in ('mention', 'chain'):
            return super(MentionWithHistory, self).__getattr__()
        else:
            orig_attr = self.mention.__getattribute__(attr)
            if callable(orig_attr):
                def hooked(*args, **kwargs):
                    result = orig_attr(*args, **kwargs)
                    # prevent mention from becoming unwrapped
                    if result == self.mention:
                        return self
                    return result
                return hooked
            else:
                return orig_attr
        
    def __eq__(self, other):
        if isinstance(other, MentionWithHistory):
            return self.mention == other.mention
        elif isinstance(other, Mention):
            return self.mention == other
        else:
            return False
    
    def __ne__(self, other):
        if isinstance(other, MentionWithHistory):
            return self.mention != other.mention
        elif isinstance(other, Mention):
            return self.mention != other
        else:
            return True
    
    def __lt__(self, other):
        if isinstance(other, MentionWithHistory):
            return self.mention < other.mention
        elif isinstance(other, Mention):
            return self.mention < other
        else:
            super(MentionWithHistory, self).__lt__(other)
            
    def __hash__(self):
        return hash(self.mention)
    
    def __str__(self):
        return '%s, linked to: %s' %(self.mention, self.chain)

def extract_substructures_ranking(doc, use_predicted_set_id=False, index=None):
    """Adapted from cort.coreference.approaches.mention_ranking.extract_substructures
    to take into account the history of mentions. 
    
    Extract the search space for the mention ranking model,
    The mention ranking model consists in computing the optimal antecedent
    for an anaphor, which corresponds to predicting an edge in graph. This
    functions extracts the search space for each such substructure (one
    substructure corresponds to one antecedent decision for an anaphor).
    The search space is represented as a nested list of mention pairs. The
    mention pairs are candidate arcs in the graph. The ith list contains all
    potential (mention, antecedent) pairs for the ith mention in the
    document. The antecedents are ordered by distance. For example,
    the third list contains the pairs (m_3, m_2), (m_3, m_1), (m_3, m_0),
    where m_j is the jth mention in the document.
    Args:
        doc (CoNLLDocument): The document to extract substructures from.
    Returns:
        (list(list((Mention, Mention)))): The nested list of mention pairs
        describing the search space for the substructures.
    """
    substructures = []
    
    # build history of mentions
    set_id2chain = defaultdict(list)
    mens_with_hist = []
    for men in doc.system_mentions:
        set_id = men.attributes.get("set_id" if use_predicted_set_id else "annotated_set_id")
        if set_id is not None:
            set_id2chain[set_id].append(men)
        mens_with_hist.append(MentionWithHistory(men, sorted(set_id2chain[set_id])))

    # iterate over mentions
    if index is None:
        enums = enumerate(doc.system_mentions)
    else:
        if index >= len(doc.system_mentions): raise StopIteration
        enums = [(index, doc.system_mentions[index])]
    for i, ana in enums:
        for_anaphor_arcs = []

        # iterate in reversed order over candidate antecedents
        for ante in sorted(mens_with_hist[:i], reverse=True):
            for_anaphor_arcs.append((ana, ante))

        substructures.append(for_anaphor_arcs)

    return substructures


def predict(testing_corpus,
            instance_extractor,
            perceptron,
            coref_extractor):
    """ According to a learned model, predict coreference information.

    Args:
        testing_corpus (Corpus): The corpus to predict coreference on.
        instance_extractor (InstanceExtracor): The instance extracor that
            defines the features and the structure of instances that are
            extracted during testing.
        perceptron (Perceptron): A perceptron learned from training data.
        argmax_function (function): A decoder that computes the best-scoring
            coreference structure over a set of structures.
        coref_extractor (function): An extractor for consolidating pairwise
            predictions into coreference clusters.

    Returns:
        A tuple containing two dicts. The components are

            - **mention_entity_mapping** (*dict(Mention, int)*): A mapping of
              mentions to entity identifiers.
            - **antecedent_mapping** (*dict(Mention, Mention)*): A mapping of
              mentions to their antecedent (as determined by the
              ``coref_extractor``).
    """
    logging.info("Predicting.")

    logging.info("\tRemoving coreference annotations from corpus.")
    for doc in testing_corpus:
        doc.antecedent_decisions = {}
        for mention in doc.system_mentions:
            mention.attributes["antecedent"] = None
            mention.attributes["set_id"] = None
            mention.attributes["annotated_set_id"] = None

    logging.info("\tExtracting feature and predicting one link at a time.")
    instance_extractor.use_predicted_set_id = True
    arcs, labels, scores = [], [], []
    try:
        while True:
            instance_extractor.increment()
            substructures, arc_information = instance_extractor.extract(testing_corpus) #, single_thread=True)
            arcs_i, labels_i, scores_i = perceptron.predict(substructures, arc_information)
            arcs.extend(arcs_i)
            labels.extend(labels_i)
            scores.extend(scores_i)
            logging.info("\t\tStep: %d" %instance_extractor.increment_step)
    except StopIteration:
        pass

    logging.info("\tClustering results.")
    return coref_extractor(arcs, labels, scores, perceptron.get_coref_labels())


if __name__ == '__main__':
    with codecs.open('preprocessed-data/sample.conll', 'r', 'utf-8') as f:
        sampleCorpus = Corpus.from_file('sample', f)
    for doc in sampleCorpus:
        doc.system_mentions = doc.annotated_mentions
    print(extract_substructures_ranking(sampleCorpus.documents[0]))
    