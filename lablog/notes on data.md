
There are only 2.9M lines in the file, how come they get 3.4M sequences (Table 2)???

    $ wc -l preprocessed-data/srlCorefChains.txt
     2934410 preprocessed-data/srlCorefChains.txt

A lot of *I*'s happen to be treated as prepositions (same as, and 
sometimes next to *back*, *down*, *off*, etc.)

    $ grep '\[I\]' preprocessed-data/srlCorefChains.txt | wc -l
        1094
    $ grep '\[I\]' preprocessed-data/srlCorefChains.txt | more
    PER     use.03[I]#hear.01!C-V,startle.01#hear.01!A1,
    PER     say.01!A0,give.01(not)[I]!A0,
    PER     experience.01!A0,use.03[I]#live.01!C-V,get.05[off]!A1,get.05[off]!A1,get.05[off]!A1,
    PER     say.01#suffer.01!A0,use.03[I]#go.06!C-V,taunt.01!A1,
    LOC     think.01!A0,use.03[I]#send.01!C-V,work.01!R-A0,develop.02!R-A0,
    PER     use.03[I]#come.01!C-V,miss.02!A0,take.01!A0,
    PER     accept.01!A0,go.01#spring.01[I]!A1,
    PER     start.01!R-A0,invite.01!A1,show.02[I][up]#look.02!C-V,

Same for other pronouns:

    $ grep '\[they\]' preprocessed-data/srlCorefChains.txt | wc -l
         136
    $ grep '\[he\]' preprocessed-data/srlCorefChains.txt | wc -l
         398
    $ grep '\[she\]' preprocessed-data/srlCorefChains.txt | wc -l
         113

More stats:

    Number of unique words in different frequencies ranges:
        [0, 5): 240146
        [5, 10): 17887
        [10, 20): 9702
        [20, 25): 2118
        [25, 30): 1437
        [30, 50): 3350
        [50, 100): 3073
        [100, 1000): 3948
        >=1000: 786
    High frequency frame compounds: ['say.01#add.01!A0', 'say.01#refer.01!A0', 'say.01#want.01!A0', 'say.01#expect.01!A0', 'want.01#see.01!A0', 'say.01#believe.01!A0', 'say.01#hope.01!A0', 'want.01#know.01!A0', 'say.01#think.01!A0', 'want.01#play.01!A0', 'like.02#see.01!A0', 'say.01#plan.01!A0', 'try.01#get.01!A0', 'say.01#feel.01!A0', 'want.01#make.04!A0', 'want.01#win.01!A0', 'say.01#tell.01!A0', 'want.01#take.01!A0', 'want.01#get.01!A0', 'want.01#go.02!A0', 'say.01#see.01!A0', 'say.01#know.01!A0', 'want.01#give.01!A0', 'try.01#make.02!A0', 'say.01#note.01!A0', 'want.01#stay.01!A0', 'say.01#need.01!A0', 'want.01#make.02!A0', 'think.01#know.01!A0', 'say.01#ask.01!A0', 'try.01#find.01!A0', 'need.01#know.01!A0', 'want.01#say.01!A0', 'want.01#get.04!A0', 'decline.02#say.01!A0', 'agree.01#pay.01!A0', 'go.02#work.01!A0', 'want.01#come.01!A0', 'want.01#go.01!A0', 'want.01#keep.02!A0', 'decline.02#comment.01!A0', 'say.01#smile.01!A0', 'try.01#get.04!A0', 'want.01#hear.01!A0', 'want.01#show.01!A0', 'think.01#need.01!A0', 'want.01#talk.01!A0', 'want.01#make.03!A0', 'agree.01#sell.01!A0', 'say.01#find.01!A0', 'want.01#help.01!A0', 'say.01#explain.01!A0', 'begin.01#work.01!A0', 'want.01#go.06!A0', 'say.01#take.01!A0', 'want.01(not)#see.01!A0', 'say.01#speak.01!A0', 'agree.01#buy.01!A0', 'say.01#give.01!A0', 'say.01#laugh.01!A0', 'want.01#buy.01!A0', 'get.03#hurt.01!A1', 'want.01#use.01!A0', 'want.01#put.01!A0', 'say.01#intend.01!A0', 'say.01#feel.02!A0', 'say.01#fear.01!A0', 'try.01#put.01!A0', 'plan.01#use.01!A0', 'want.01#work.01!A0', 'keep.02#say.01!A0', 'say.01#use.01!A0', 'try.01#make.03!A0', 'wait.01#see.01!A1', 'let.01#go.01!A0', 'try.01#take.01!A0', 'get.03#involve.01!A1', 'see.01#happen.01!A0', 'make.02#feel.01!A0', 'let.01#know.01!A0', 'say.01#include.01!A0', 'try.01#help.01!A0', 'continue.01#work.01!A0', 'think.01#get.01!A0', 'help.01#find.01!A0', 'say.01#make.02!A0', 'get.03#use.02!A1', 'know.01#want.01!A0', 'become.01#involve.01!A1', 'decline.02#discuss.01!A0', 'hear.01#say.01!A0', 'try.01#figure.05[out]!A0', 'think.01#want.01!A0', 'want.01#get.03!A0', 'say.01#look.01!A0', 'add.01#refer.01!A0', 'want.01(not)#get.03!A0', 'say.01#come.01!A0', 'get.03#hit.01!A1', 'quote.01#say.01!A1']
    Num unique frames: 12594
    Num unique frame compounds: 3217
    Num sequences: 1938596
    Num tokens: 6296005
    Num tokens per sequence: 3.2

