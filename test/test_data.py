from data import _extract_labeled_spans
from cort.core.spans import Span


def test_extract_labeled_spans():
    # empty
    assert _extract_labeled_spans(['*', '*', '*']) == {}
    # singletons
    assert _extract_labeled_spans(['*', '(ARG0*)', '(ARG1*)']) == {'ARG0': Span(1, 1), 'ARG1': Span(2, 2)}
    # roles with dash
    assert _extract_labeled_spans(['(R-ARG1(ARG0*)', '*)', '*']) == {'ARG0': Span(0, 0), 'R-ARG1': Span(0, 1)}
    assert _extract_labeled_spans(['(ARGM-MOD*)', '*', '*']) == {'ARGM-MOD': Span(0, 0)}
    # singleton + normal
    assert _extract_labeled_spans(['(ARG1*', '*)', '(ARG0*)']) == {'ARG0': Span(2, 2), 'ARG1': Span(0, 1)}
    # nested normal
    assert _extract_labeled_spans(['(ARG1(ARG0*', '*)', '*)']) == {'ARG0': Span(0, 1), 'ARG1': Span(0, 2)}
    # offset
    assert _extract_labeled_spans(['(ARG1(ARG0*', '*)', '*)'], 10) == {'ARG0': Span(10, 11), 'ARG1': Span(10, 12)}
    # overwriting
    assert _extract_labeled_spans(['*', '(ARG0*)', '(ARG0*)']) == {'ARG0': Span(2, 2)}

