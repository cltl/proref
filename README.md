
## Setting up

The code is tested on Python 3. It's likely to break on Python 2.

Install Python packages: `pip3 install tabulate cort`.

You will also need Java 7+, cmake and GCC.

Build OxLM by following instructions in `OxLM-2017-10-09/README.md`

Please refer to [setup-vm.log](setup-vm.log) for an example of setup 
instructions for an newly-instantiated Ubuntu 16.04 virtual machine.

## Obtain data

Put ACE-2005 into a folder called `data/ACE-2005-english`. It should look like:

    + data
    | + ACE-2005-english
    | |-- bc
    | |-- bn   
    | |-- nw   
    | |-- wl   

Make sure file `apf.v5.1.1.dtd` is in `nw`. You can find it online.

Put CoNLL-2012 into a folder called `data/conll-2012` (make sure *_conll files
are generated properly). 

conll-2012
conll-2012
conll-2012-test-key

## Replication steps

### Peng and Roth (2016)

1. Ask Peng Haoruo for file `srlCorefChains.txt` (PropBank) and `srlCorefChains_group.txt` (FrameNet). Put them into `preprocessed-data`
2. Run `python3 -m peng_roth.preprocess`
3. Run `python3 -m peng_roth.stats` to get some statistics if you like
4. Run `./oxlm.job` to train language models (can take some hours)

### cort

1. Look at `preprocess_conll.py` to find out the version of the code used to preprocess data, check out that version and run `python3 preprocess_conll.py`, switch back to HEAD
2. 



