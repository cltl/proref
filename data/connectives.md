This is how I created the file `connectives.lst`

1. Download [pdtb2 from Github](https://github.com/cgpotts/pdtb2/tree/ab806da562d559f8a118958d8647ca36085228cc) (contains both code and data)
2. Extract `pdtb2.csv.zip` into `pdtb2.csv`
3. Open ipython3 and execute this code:
4. Copy and paste into `connectives.lst`

    from collections import defaultdict
    from pdtb2 import CorpusReader, Datum
    
    def connective_distribution():
        """Counts of connectives by relation type."""
        pdtb = CorpusReader('pdtb2.csv')
        d = defaultdict(lambda : defaultdict(int))
        for datum in pdtb.iter_data():
            cs = datum.conn_str(distinguish_implicit=False)
            # Filter None values (should be just EntRel/NoRel data).
            if cs:
                # Downcase for further collapsing, and add 1.
                d[datum.Relation][cs.lower()] += 1
        return d
    
    d = connective_distribution()
    print('\n'.join(sorted(d['Explicit'].keys())))
